﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adbag_NavMicroservice.Models
{
    public class UPSDetails
    {
        public string UPSLICENSENUMBER { get; set; }
        public string UPSUSERID { get; set; }
        public string UPSPASSWORD { get; set; }
        public string UPSSHIPPERNUMBER { get; set; }
        public string CUSTOMERCLASSIFICATION { get; set; }
        public string UPSADDITIONALCOST { get; set; }
        public string FROMZIPCODE { get; set; }
        public string FROMSTATE { get; set; }
        public string FROMCITY { get; set; }
        public string FROMCOUNTRY { get; set; }
        public string NEGOTIATEDRATE { get; set; }
        public string OrderType { get; set; }
    }
}