﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adbag_NavMicroservice.Models
{
    public class RateQuoteResponse
    {
        #region RateQuoteResult

        public class PageHead
        {
            public string pageTitle { get; set; }
            public string pageSubTitle { get; set; }
        }

        public class BodyHead
        {
            public string bodyTitle { get; set; }
            public string message { get; set; }
        }

        public class RequestedServiceType
        {
            public string value { get; set; }
        }

        public class RequestedTime
        {
            public string type { get; set; }
        }

        public class RequestedDateTime
        {
            public List<RequestedTime> requestedTime { get; set; }
        }

        public class Delivery
        {
            public RequestedServiceType requestedServiceType { get; set; }
            public RequestedDateTime requestedDateTime { get; set; }
            public int standardDate { get; set; }
            public int standardDays { get; set; }
        }

        public class Location
        {
            public string role { get; set; }
            public string city { get; set; }
            public string statePostalCode { get; set; }
            public string zipCode { get; set; }
            public string nationCode { get; set; }
            public int terminal { get; set; }
            public string zone { get; set; }
            public string typeService { get; set; }
        }

        public class Customer
        {
            public string role { get; set; }
            public string name { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string statePostalCode { get; set; }
            public string zipCode { get; set; }
            public string nationCode { get; set; }
            public string terminal { get; set; }
            public string account { get; set; }
            public string busId { get; set; }
            public string locationCreditIndicator { get; set; }
            public string cmfnbr { get; set; }
        }

        public class HandlingUnits
        {
            public int count { get; set; }
            public string packageCode { get; set; }
            public int length { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class Pieces
        {
        }

        public class Nmfc
        {
            public string @class { get; set; }
        }

        public class LineItem
        {
            public string type { get; set; }
            public HandlingUnits handlingUnits { get; set; }
            public Pieces pieces { get; set; }
            public string hazardous { get; set; }
            public Nmfc nmfc { get; set; }
            public int weight { get; set; }
            public string density { get; set; }
            public string ratedType { get; set; }
            public string ratedClass { get; set; }
            public int ratedWeight { get; set; }
            public string rate { get; set; }
            public string rateUOM { get; set; }
            public string charges { get; set; }
            public string weightUOM { get; set; }
            public string ratePerUOM { get; set; }
            public string description { get; set; }
            public string code { get; set; }
            public string terms { get; set; }
            public string creditInd { get; set; }
            public string disposition { get; set; }
            public string sourceCode { get; set; }
            public string quantity { get; set; }
        }

        public class RatedCharges
        {
            public int freightCharges { get; set; }
            public int otherCharges { get; set; }
            public int totalCharges { get; set; }
        }

        public class Pricing
        {
            public string agent { get; set; }
            public string tariff { get; set; }
            public string item { get; set; }
            public string applyPayTerms { get; set; }
            public string usedFlag { get; set; }
            public string role { get; set; }
            public string pricingType { get; set; }
            public string scheduleType { get; set; }
            public string type { get; set; }
        }

        public class PromoCodeInfo
        {
        }

        public class Liability
        {
        }

        public class RateQuote
        {
            public string referenceId { get; set; }
            public string quoteId { get; set; }
            public string paymentTerms { get; set; }
            public string payerRole { get; set; }
            public int pickupDate { get; set; }
            public Delivery delivery { get; set; }
            public string currency { get; set; }
            public List<Location> location { get; set; }
            public List<Customer> customer { get; set; }
            public List<LineItem> lineItem { get; set; }
            public string vendorSCAC { get; set; }
            public string publishRateFlag { get; set; }
            public string minCharge { get; set; }
            public string guaranteeFlag { get; set; }
            public RatedCharges ratedCharges { get; set; }
            public string shipmentRateUnit { get; set; }
            public int shipmentPricePerUnit { get; set; }
            public List<Pricing> pricing { get; set; }
            public PromoCodeInfo promoCodeInfo { get; set; }
            public Liability liability { get; set; }
            public string minRateApplied { get; set; }
            public string dimFactor { get; set; }
            public string ratedPricingDays { get; set; }
            public string accSrvcCount { get; set; }
            public string commodityCount { get; set; }
            public string minChargeFloor { get; set; }
            public int weight { get; set; }
            public int ratedWeight { get; set; }
            public string quoteType { get; set; }
            public string linearFeet { get; set; }
            public string cubicFeet { get; set; }
            public string calcCubicFeet { get; set; }
            public string fullVisibleCapacity { get; set; }
            public string positionHours { get; set; }
        }

        public class BodyMain
        {
            public RateQuote rateQuote { get; set; }
        }

        public class PageRoot
        {
            public string context { get; set; }
            public string programId { get; set; }
            public string queryType { get; set; }
            public string dateTime { get; set; }
            public string returnCode { get; set; }
            public string returnText { get; set; }
            public string recordCount { get; set; }
            public string recordOffset { get; set; }
            public string maxRecords { get; set; }
            public PageHead pageHead { get; set; }
            public BodyHead bodyHead { get; set; }
            public BodyMain bodyMain { get; set; }
        }

        public class RootObjectMain
        {
            public bool isSuccess { get; set; }
            public List<object> errors { get; set; }
            public object warnings { get; set; }
            public PageRoot pageRoot { get; set; }
        }

        #endregion RateQuoteResult
    }
}