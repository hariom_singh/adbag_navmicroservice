﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adbag_NavMicroservice.Models
{
    public class FEDEXDetails
    {
        public string FEDEXACCOUNTNUMBER { get; set; }
        public string FEDEXMETERNUMBER { get; set; }
        public string FEDEXUSERKEY { get; set; }
        public string FEDEXUSERPASSWORD { get; set; }
        public string FEDEXRATETYPE { get; set; }
    }
}