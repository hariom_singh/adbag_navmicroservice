﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adbag_NAVMicroservice.Models
{
    public class ArtworkApprovalDetails
    {
        public string SalesOrder { get; set; }
        public string CustomerPO { get; set; }

        public string ItemNumber { get; set; }
        public string Quantity { get; set; }
        public string ActualQuantity { get; set; }
        public string ItemColor { get; set; }
        public string ImprintMethod { get; set; }
        public string ImprintLocation { get; set; }
        public string ImprintSize { get; set; }
        public string ImprintColors { get; set; }
        public string ProductColors { get; set; }

        public string ArtworkApprovalStatus { get; set; }
        public string Comments { get; set; }
        public string Signature { get; set; }

        //public string ImagePath1 { get; set; }
        //public string ImagePath2 { get; set; }
        public string HostUrl { get; set; }
        public string PrimaryKey { get; set; }
        public string NewPrimaryKey { get; set; }
        public string IsSO { get; set; }
        public string Status { get; set; }

        public string ArtFile { get; set; }
        public string SalesPerson { get; set; }
        public string ItemDescription { get; set; }
        public string NoOfRework { get; set; }
        public string ArtworkDate { get; set; }
        public string ArtworkTime { get; set; }
        public bool IsBlindPage { get; set; }
        public string PackagingOption { get; set; }
        public string CommentsForCustomer { get; set; }
        public string CommentsByCustomer { get; set; }
        public string CustomerPOnumber { get; set; }

        public string ProofImage { get; set; }
        public string RulerImage { get; set; }
        public string OrderNo { get; set; }
        public string ExceptionMessage { get; set; }
        public byte[] label { get; set; }
        public string pdfpath { get; set; }

    }
    public class ImageDetails
    {
        public string ImgPath1 { get; set; }
        public string ImgPath2 { get; set; }
        public string OrderNo { get; set; }
        public string pdfname { get; set; }
        public string pdfdownpath { get; set; }
    }
}
