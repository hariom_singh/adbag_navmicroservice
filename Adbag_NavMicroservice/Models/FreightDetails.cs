﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adbag_NAVMicroservice.Models
{
    public class FreightDetails
    {
        public string FreightRequestdata { get; set; }
        public string FreightType { get; set; }
        public string UserKey { get; set; }
        public string Password { get; set; }
        public string Meter_ShipperNo { get; set; }
        public string Account_LicenceNo { get; set; }
        public string[] ServiceType { get; set; }
        public bool AllowNegotiatedRates { get; set; }
        public string FEDEXRATETYPE { get; set; }

        public string DropoffType { get; set; }
        public string PackagingType { get; set; }

        public string[] Shipper_StreetLines { get; set; }
        public string Shipper_City { get; set; }
        public string Shipper_StateOrProvinceCode { get; set; }
        public string Shipper_PostalCode { get; set; }
        public string Shipper_CountryCode { get; set; }
        public bool Shipper_isResidential { get; set; }

        public string[] Recipient_StreetLines { get; set; }
        public string Recipient_City { get; set; }
        public string Recipient_StateOrProvinceCode { get; set; }
        public string Recipient_PostalCode { get; set; }
        public string Recipient_CountryCode { get; set; }
        public bool Recipient_isResidential { get; set; }

        public string Currency { get; set; }
        public string Amount { get; set; }
        public string Quantity { get; set; }
        public string Weight_Units { get; set; }
        public string Weight_Value { get; set; }
        public string Dim_Units { get; set; }
        public string Dim_Length { get; set; }
        public string Dim_Width { get; set; }
        public string Dim_Height { get; set; }
        public string OrderType { get; set; }
        public string UnitsPerCarton { get; set; }

        public string UPSValue { get; set; }
        public string UPSText { get; set; }
        public string DocumentNo { get; set; }
        public string LineNo { get; set; }
        public string ShipmentMethod { get; set; }
        public string ExceptionMessage { get; set; }
    }

    public class USPSFreightDetails
    {
        public string USERID { get; set; }
        public string PASSWORD { get; set; }
        public string Revision { get; set; }

        public string PackageID { get; set; }
        public string Service { get; set; }
        public string FirstClassMailType { get; set; }
        public string ZipOrigination { get; set; }
        public string ZipDestination { get; set; }
        public string Pounds { get; set; }
        public string Ounces { get; set; }
        public string Container { get; set; }
        public string Size { get; set; }
        public string Width { get; set; }
        public string Length { get; set; }
        public string Height { get; set; }
        public string Girth { get; set; }
        public string Value { get; set; }
        public bool Machinable { get; set; }
    }

    public class RnlCharges
    {
        public string Amount { get; set; }
        public string Rate { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Weight { get; set; }
    }

    public class FreightRate
    {
        public string freightCode { get; set; }
        public string code { get; set; }
        public string freightType { get; set; }
        public string description { get; set; }
        public string currencyCode { get; set; }
        public string monetaryValue { get; set; }
        public string negotiateValue { get; set; }
        public object surChargeValue { get; set; }
        public object daysToDelivery { get; set; }
        public object shippingType { get; set; }
        public object guaranteed { get; set; }
        public object estimatedArrival { get; set; }
        public object productCode { get; set; }
    }
    public class BOL
    {
        public byte[] BOLimag { get; set; }

        public string filepath { get; set; }
    }


    #region RateQuote

    public class Login
    {
        public string username { get; set; }
        public string password { get; set; }
        public string busId { get; set; }
        public string busRole { get; set; }
        public string paymentTerms { get; set; }
    }

    public class Details
    {
        public string serviceClass { get; set; }
        public string typeQuery { get; set; }
        public string pickupDate { get; set; }
    }

    public class OriginLocation
    {
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
    }

    public class DestinationLocation
    {
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
    }

    public class Commodity
    {
        public int nmfcClass { get; set; }
        public int handlingUnits { get; set; }
        public string packageCode { get; set; }
        public int packageLength { get; set; }
        public int packageWidth { get; set; }
        public int packageHeight { get; set; }
        public int weight { get; set; }
    }

    public class ListOfCommodities
    {
        public List<Commodity> commodity { get; set; }
    }

    public class ServiceOpts
    {
        public List<string> accOptions { get; set; }
    }

    public class RootObject
    {
        public Login login { get; set; }
        public Details details { get; set; }
        public OriginLocation originLocation { get; set; }
        public DestinationLocation destinationLocation { get; set; }
        public ListOfCommodities listOfCommodities { get; set; }
        public ServiceOpts serviceOpts { get; set; }
    }
    #endregion RateQuote


    

}
