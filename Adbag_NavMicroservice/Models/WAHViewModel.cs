﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adbag_NavMicroservice.Models
{
    public class WAHViewModel
    {
        public String Key { get; set; }
        public String Type { get; set; }
        public String TypeSpecified { get; set; }
        public String No { get; set; }
        public String Source_Document { get; set; }
        public String Source_DocumentSpecified { get; set; }
        public String Source_No { get; set; }
        public String Location_Code { get; set; }
        public String External_Document_No { get; set; }
        public String Sales_Order_No { get; set; }
        public string Scan_Worksheet_No { get; set; }
    }
}