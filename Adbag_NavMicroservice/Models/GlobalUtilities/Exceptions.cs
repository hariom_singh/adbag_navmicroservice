﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace Adbag_NAVMicroservice.Models.GlobalUtilities
{
    public class Exceptions
    {
        public static void WriteExceptionLog(Exception ex)
        {
            System.Threading.ThreadAbortException exception = ex as System.Threading.ThreadAbortException;
            if (exception == null)
            {
                string ExceptionLogFolderPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ExceptionLogFolder"]);
                try
                {
                    if (!Directory.Exists(ExceptionLogFolderPath))
                        Directory.CreateDirectory(ExceptionLogFolderPath);

                    if (Directory.Exists(ExceptionLogFolderPath))
                    {
                        //Create month wise exception log file.
                        string date = string.Format("{0:dd}", DateTime.Now);
                        string month = string.Format("{0:MMM}", DateTime.Now);
                        string year = string.Format("{0:yyyy}", DateTime.Now);

                        string folderName = month + year; //Feb2013
                        string monthFolder = ExceptionLogFolderPath + "\\" + folderName;
                        if (!Directory.Exists(monthFolder))
                            Directory.CreateDirectory(monthFolder);

                        string ExceptionLogFileName = monthFolder +
                            "\\ExceptionLog_" + date + month + ".txt"; //ExceptionLog_04Feb.txt

                        using (System.IO.StreamWriter strmWriter = new System.IO.StreamWriter(ExceptionLogFileName, true))
                        {
                            strmWriter.WriteLine("On " + DateTime.Now.ToString() +
                                ", following error occured in the application:");
                            strmWriter.WriteLine("Message: " + ex.Message);
                            //strmWriter.WriteLine("Inner Exception: " + ex.InnerException.Message);
                            //strmWriter.WriteLine("Inner Exception(2): " + ex.InnerException.InnerException.Message);
                            strmWriter.WriteLine("Source: " + ex.Source);
                            strmWriter.WriteLine("Stack Trace: " + ex.StackTrace);
                            strmWriter.WriteLine("HelpLink: " + ex.HelpLink);
                            strmWriter.WriteLine("-------------------------------------------------------------------------------");
                        }
                    }
                    else
                        throw new DirectoryNotFoundException("Exception log folder not found in the specified path");
                }
                catch
                {

                }
            }
        }
    }
}
