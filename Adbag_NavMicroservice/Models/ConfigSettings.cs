﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adbag_NavMicroservice.Models
{
    public class ConfigSettings
    {
        public string MongoConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string ServiceURL { get; set; }
        public string HostURL { get; set; }
    }
}