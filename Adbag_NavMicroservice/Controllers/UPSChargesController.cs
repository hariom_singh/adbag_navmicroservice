using Adbag_NavMicroservice.FreightParameterList;
using Adbag_NavMicroservice.UPSSalesLineCarton;
using Adbag_NavMicroservice.UPSShippingInfo;
using Adbag_NavMicroservice.UPSWebTransaction;
using Adbag_NAVMicroservice.Models;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("Freight")]
    public class UPSChargesController : Controller
    {
        private string _navUrl, _document_No, _line_No, _host;

        FreightParameter_List_PortClient FreightParameter_List_PortClient = null;
        FreightParameter_List_Filter FreightParamListFilter;
        FreightParameter_List_Filter FreightParamListFilter1;
        FreightParameter_List[] objFreightParameter_List;

        Sales_Line_Carton_PortClient objSalesLineCarton_PortClient = null;
        Sales_Line_Carton_Filter objSalesLineCarton_Filter = null;
        Sales_Line_Carton_Filter objSalesLineCarton_Filter1 = null;
        Sales_Line_Carton[] objSalesLineCarton;

        Web_Transaction_PortClient objWeb_Transaction_PortClient = null;

        Shipping_Info_PortClient objShipping_Info_PortClient;
        Shipping_Info objShipping_Info;
        Shipping_Info_Filter objShipping_Info_Filter;
        Shipping_Info[] objShipping_InfoArray;

        CustomerDetails objCustomerDetails = null;
        FreightDetails objFreightDetails = null;
        int ordertype = 0;


        protected void BindSetting()
        {
            try
            {
                objFreightDetails = new FreightDetails();

                if (objShipping_InfoArray != null && objShipping_InfoArray.Count() > 0 &&
                    objFreightParameter_List != null && objFreightParameter_List.Count() > 0 &&
                   objSalesLineCarton != null && objSalesLineCarton.Count() > 0)
                {
                    if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                        ViewBag.OrderType = "2";
                    else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                        ViewBag.OrderType = "1";

                    string ProductDetails = string.Empty;
                    string RecipientCity = string.Empty, RecipientState = string.Empty, RecipientZipCode = string.Empty, RecipientCountry = string.Empty;
                    string ShipperCity = string.Empty, ShipperState = string.Empty, ShipperZipCode = string.Empty, ShipperCountry = string.Empty;
                    bool Residential = false;
                    for (int j = 0; j < objFreightParameter_List.Count(); j++)
                    {
                        if (objFreightParameter_List[j].Shipment_Sequence == 0 && objFreightParameter_List[j].IsShipment.ToString().ToLower() == "true")
                        {
                            RecipientCity = Convert.ToString(objFreightParameter_List[j].RecipientCity);
                            RecipientState = Convert.ToString(objFreightParameter_List[j].RecipientState);
                            RecipientZipCode = Convert.ToString(objFreightParameter_List[j].RecipientZipCode);
                            RecipientCountry = Convert.ToString(objFreightParameter_List[j].RecipientCountry);
                            Residential = objFreightParameter_List[j].Residential;

                            ShipperCity = Convert.ToString(objFreightParameter_List[j].ShipperCity);
                            ShipperState = Convert.ToString(objFreightParameter_List[j].ShipperState);
                            ShipperZipCode = Convert.ToString(objFreightParameter_List[j].ShipperZipCode);
                            ShipperCountry = Convert.ToString(objFreightParameter_List[j].ShipperCountry);
                            break;
                        }
                    }

                    for (int i = 0; i < objSalesLineCarton.Count(); i++)
                    {
                        if (objSalesLineCarton[i].Shipment_Sequence == 0)
                        {
                            decimal weight = objSalesLineCarton[i].Total_Weight / objSalesLineCarton[i].Carton_Quantity;
                            decimal declaredValue = objSalesLineCarton[i].Item_Value / objSalesLineCarton[i].Carton_Quantity;
                            decimal unitsPerCarton = objSalesLineCarton[i].Item_Quantity / objSalesLineCarton[i].Carton_Quantity;
                            ProductDetails = ProductDetails + "," + "{'Currency': 'USD','Amount': '0','Quantity': '" + Convert.ToString(objSalesLineCarton[i].Item_Quantity) + "','Declared_Value': '" + Convert.ToString(declaredValue) + "','Weight_Units': '" + objSalesLineCarton[i].Weight_Unit_of_Measurement + "','Weight_Value': '" + Convert.ToString(weight) + "','Dim_Units': 'IN','Dim_Length': '" + Convert.ToString(objSalesLineCarton[i].Length) + "','Dim_Width':'" + Convert.ToString(objSalesLineCarton[i].Width) + "','Dim_Height': '" + Convert.ToString(objSalesLineCarton[i].Height) + "','UnitsPerCarton': '" + Convert.ToString(unitsPerCarton) + "'}";
                            //ProductDetails = ProductDetails + "," + "{'Currency': 'USD','Amount': '0','Quantity': '" + Convert.ToString(objSalesLineCarton[i].Carton_Quantity) + "','Declared_Value': '" + Convert.ToString(objSalesLineCarton[i].Item_Value) + "','Weight_Units': '" + objSalesLineCarton[i].Weight_Unit_of_Measurement + "','Weight_Value': '" + Convert.ToString(objSalesLineCarton[i].Total_Weight) + "','Dim_Units': 'IN','Dim_Length': '" + Convert.ToString(objSalesLineCarton[i].Length) + "','Dim_Width':'" + Convert.ToString(objSalesLineCarton[i].Width) + "','Dim_Height': '" + Convert.ToString(objSalesLineCarton[i].Height) + "','UnitsPerCarton': '" + Convert.ToString(objSalesLineCarton[i].Item_Quantity) + "'}";
                        }
                    }
                    ProductDetails = ProductDetails.TrimStart(',');
                    string FreightRequestdata = "{'RateRequest': {'UserCredential': [{'FreightType': '','UserKey': '','Password': '','Meter_ShipperNo': '','Account_LicenceNo': ''," +
                        "'ServiceType': [],'AllowNegotiatedRates': false},{'FreightType':'UPS','UserKey':'" + Convert.ToString(objShipping_InfoArray[0].UPS_User_Id) + "'," +
                        "'Password':'" + Convert.ToString(objShipping_InfoArray[0].UPS_Password) + "','Meter_ShipperNo':'" + Convert.ToString(objShipping_InfoArray[0].UPS_Shipper_Number) + "','Account_LicenceNo':'" + Convert.ToString(objShipping_InfoArray[0].UPS_License_Number) + "','ServiceType':[],'AllowNegotiatedRates':'False'}]," +
                        "'RequestedShipment': {'DropoffType': 'DROP_BOX','PackagingType': '_02Package'}," +
                        "'ShipperAddress': {'StreetLines': ['Input Your Information','Input Your Information2'],'City': '" + ShipperCity + "','StateOrProvinceCode': '" + ShipperState + "','PostalCode': '" + ShipperZipCode + "'," +
                        "'CountryCode': '" + ShipperCountry + "','isResidential': '" + Residential + "'},'RecipientAddress': {'StreetLines': ['Input Your Information','Input Your Information2'],'City': '" + RecipientCity + "'," +
                        "'StateOrProvinceCode': '" + RecipientState + "','PostalCode': '" + RecipientZipCode + "','CountryCode': '" + RecipientCountry + "','isResidential': '" + Residential + "'},'ProductDetails': [" + ProductDetails + "]} }";
                    FreightRequestdata = FreightRequestdata.Replace("'", "\"");
                    objFreightDetails.FreightRequestdata = FreightRequestdata;
                }
                else
                {
                    string strServiceName = string.Empty;
                    if (objShipping_InfoArray == null || objShipping_InfoArray.Count() == 0)
                    {
                        strServiceName = "Shipping Info";
                    }
                    if (objFreightParameter_List == null || objFreightParameter_List.Count() == 0)
                    {
                        strServiceName = strServiceName + ",FreightParameter List";
                    }
                    if (objSalesLineCarton == null || objSalesLineCarton.Count() == 0)
                    {
                        strServiceName = strServiceName + ",SalesLine Carton";
                    }
                    strServiceName = strServiceName.TrimStart(',');
                    objFreightDetails.ExceptionMessage = "Nav Service " + strServiceName + " found with Null Values.";
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
        }

        [Route("Get")]
        [HttpGet()]
        public string Get()
        {
            return "Success";
        }

        [Route("Getfreight/{Document_No}/{Line_No}")]
        [HttpGet()]
        public ActionResult Getfreight(string Document_No, string Line_No)
        {
            try
            {
                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                //objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                //objShipping_Info_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUserName"].ToString(), ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString());
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;
                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                {
                    ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                }
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                #endregion

                BindSetting();
                ViewBag.FreightDetails = objFreightDetails;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("Index", objFreightDetails);
        }

        [HttpPost]
        [Route("SubmitUPS")]
        public ActionResult SubmitUPS(string UPSValue, string UPSText, string DocumentNo, string LineNo, string OrderType)
        {
            try
            {
                //To get IP Address for Remote User              
                //string ipString = HttpContext.Connection.RemoteIpAddress.ToString();
                //End
                //string UPSValue = objfreightDetails.UPSValue, UPSText = objfreightDetails.UPSText, DocumentNo = objfreightDetails.DocumentNo, LineNo = objfreightDetails.LineNo, OrderType = objfreightDetails.OrderType;
                string navUrl = ConfigurationManager.AppSettings["NavUrl"];
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = navUrl + "/Codeunit/Web_Transaction";

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                objWeb_Transaction_PortClient = new UPSWebTransaction.Web_Transaction_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objWeb_Transaction_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objWeb_Transaction_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                string rbtext = UPSText;     //UPSText.Split(' ')[2];
                string rbvalue = UPSValue;
                objWeb_Transaction_PortClient.UpdateUPSChargesAsync(Convert.ToInt32(OrderType), DocumentNo, Convert.ToInt32(LineNo), rbtext, rbvalue);

                return Json("success");
            }
            catch (Exception ex)
            {
               
                Exceptions.WriteExceptionLog(ex);
                return Json("failure");
            }
        }


        public async Task<string> CalculateFreight(FreightDetails objFreightDetails, string Filter, string Document_No, string Line_No, int OrderType)
        {
            string Result = string.Empty;
            string resultJSON = string.Empty;
            string token = string.Empty;
            string username = ConfigurationManager.AppSettings["UPSUsername"];
            string password = ConfigurationManager.AppSettings["UPSPassword"];
            try
            {
                using (var client = new HttpClient())
                {

                    var url = "https://freight.ewizpower.com/api/token";
                    Dictionary<string, string> objdata = new Dictionary<string, string>();
                    objdata.Add("username", username);
                    objdata.Add("password", password);

                    using (var content = new FormUrlEncodedContent(objdata))
                    {
                        content.Headers.Clear();
                        content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                        HttpResponseMessage response = await client.PostAsync(url, content);
                        if (response.IsSuccessStatusCode)
                        {
                            resultJSON = response.Content.ReadAsStringAsync().Result;
                            Dictionary<string, string> dResponse = new Dictionary<string, string>();
                            dResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(resultJSON);
                            if (dResponse["access_token"] != null)
                                token = dResponse["access_token"];

                        }
                    }

                }
                using (var client = new HttpClient())
                {
                    if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                        ViewBag.OrderType = "2";

                    else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                        ViewBag.OrderType = "1";

                    var data = objFreightDetails.FreightRequestdata;
                    var url = "https://freight.ewizpower.com/api/FreightRates";
                    string UPSValue = string.Empty;
                    string UPSText = string.Empty;

                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                    HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync(url, content);
                    if (response.IsSuccessStatusCode)
                    {
                        resultJSON = response.Content.ReadAsStringAsync().Result;
                        //Dictionary<string, string> dResponse = new Dictionary<string, string>();
                        var dResponse = JsonConvert.DeserializeObject<List<FreightRate>>(resultJSON);

                        if (dResponse != null && dResponse.Count > 0)
                        {
                            FreightRate freightRate = dResponse.Where(x => x.freightType.ToLower() == Filter.ToLower()).FirstOrDefault();
                            string monatoryVal = freightRate != null ? freightRate.monetaryValue : "";


                            if (monatoryVal != null && Filter != null && Document_No != null && ViewBag.OrderType != null)
                                Result = Submit(monatoryVal, Filter, Document_No, Line_No, ViewBag.OrderType);

                        }

                    }


                    return Result;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return Result;
            }

        }
        [Route("Calculate/{Document_No}/{Line_No}/{Filter}")]
        [HttpGet()]
        public async Task<ActionResult> Calculate(string Document_No, string Line_No, string Filter)
        {
            var IsScuccess = string.Empty;
            try
            {
                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                ViewBag.Filter = Filter;

                _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                //objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                //objShipping_Info_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUserName"].ToString(), ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString());
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;
                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                {
                    ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                }
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                #endregion

                BindSetting();
                ViewBag.FreightDetails = objFreightDetails;

                IsScuccess = await CalculateFreight(objFreightDetails, Filter, Document_No, Line_No, ordertype);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            //return View("Index", objFreightDetails);


            return Json(IsScuccess, JsonRequestBehavior.AllowGet);
        }


        public string Submit(string UPSValue, string UPSText, string DocumentNo, string LineNo, string OrderType)
        {
            string isSuccess = string.Empty;
            try
            {

                //To get IP Address for Remote User              
                //string ipString = HttpContext.Connection.RemoteIpAddress.ToString();
                //End
                //string UPSValue = objfreightDetails.UPSValue, UPSText = objfreightDetails.UPSText, DocumentNo = objfreightDetails.DocumentNo, LineNo = objfreightDetails.LineNo, OrderType = objfreightDetails.OrderType;
                string navUrl = ConfigurationManager.AppSettings["NavUrl"];
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = navUrl + "/Codeunit/Web_Transaction";
                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                objWeb_Transaction_PortClient = new Web_Transaction_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objWeb_Transaction_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objWeb_Transaction_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objWeb_Transaction_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                string rbtext = UPSText;     //UPSText.Split(' ')[2];
                string rbvalue = UPSValue;
                objWeb_Transaction_PortClient.UpdateUPSChargesAsync(Convert.ToInt32(OrderType), DocumentNo, Convert.ToInt32(LineNo), rbtext, rbvalue);

                isSuccess = UPSValue;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return isSuccess;
        }

    }
}