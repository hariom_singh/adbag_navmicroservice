﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Adbag_NAVMicroservice.Models;
using Adbag_NavMicroservice.Models;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using System.ServiceModel;
using System.Net;
using Adbag_NavMicroservice.FreightParameterList;
using Adbag_NavMicroservice.UPSSalesLineCarton;
using System.Configuration;
using Adbag_NavMicroservice.UPSShippingInfo;
//using Adbag_NavMicroservice.RLCarrier;
using Adbag_NavMicroservice.RLCarriersService;
using System.Xml;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using static Adbag_NavMicroservice.Models.RateQuoteResponse;

namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("RL")]
    public class RLCarrierController : Controller

    {
        // GET: RLCarrier
        public ActionResult Index()
        {
            return View();
        }

        IConfiguration _iconfiguration;
        private readonly ConfigSettings _ConfigSettings;
        readonly ILogger<FEDEXChargesController> _log;
        private string _navUrl, _document_No, _line_No;

        FreightParameterList.FreightParameter_List_PortClient FreightParameter_List_PortClient = null;
        FreightParameterList.FreightParameter_List_Filter FreightParamListFilter;
        FreightParameterList.FreightParameter_List_Filter FreightParamListFilter1;
        FreightParameterList.FreightParameter_List[] objFreightParameter_List;

        UPSSalesLineCarton.Sales_Line_Carton_PortClient objSalesLineCarton_PortClient = null;
        UPSSalesLineCarton.Sales_Line_Carton_Filter objSalesLineCarton_Filter = null;
        UPSSalesLineCarton.Sales_Line_Carton_Filter objSalesLineCarton_Filter1 = null;
        UPSSalesLineCarton.Sales_Line_Carton[] objSalesLineCarton;

        UPSWebTransaction.Web_Transaction_PortClient objWeb_Transaction_PortClient = null;

        UPSShippingInfo.Shipping_Info_PortClient objShipping_Info_PortClient;
        UPSShippingInfo.Shipping_Info objShipping_Info;
        UPSShippingInfo.Shipping_Info_Filter objShipping_Info_Filter;
        UPSShippingInfo.Shipping_Info[] objShipping_InfoArray;

        UPSDetails objUPSDetails = null;
        FEDEXDetails objFEDEXDetails = null;
        FreightDetails objFreightDetails = null;
        CustomerDetails objCustomerDetails = null;
        List<UPSDetails> lstUPSDetails = null;
        //List<RnlCharges> lstRnlCharges = null;
        IEnumerable<ServiceLevel> RnlCharges = null;
        RateQuoteResponse RateQuoteResponse = new RateQuoteResponse();
        RnlCharges[]  objrnl = null;
        private string _host;

        RLCarriersService.RateQuoteServiceSoapClient objRLCarrier = null;

        [Route("GetRL/{Document_No}/{Line_No}")]
        [HttpGet()]
        public ActionResult GetRL(string Document_No, string Line_No)
       {
            try
            {
                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;


                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;
                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                {
                    ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                }
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                    ViewBag.OrderType = "2";
                else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                    ViewBag.OrderType = "1";
                #endregion


                string strOrderLinePageURL = ConfigurationManager.AppSettings["RLserviceURL"];


                /////OrderHeader
                objRLCarrier = new RLCarriersService.RateQuoteServiceSoapClient(objNavWSBinding, new EndpointAddress(strOrderLinePageURL));
                objRLCarrier.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["RLUsername"];
                objRLCarrier.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["RLPassword"];

                RLCarriersService.RateQuoteRequest objrequest = new RLCarriersService.RateQuoteRequest();
                RLCarriersService.RateQuoteReply objresponse = new RLCarriersService.RateQuoteReply();


                List<RLCarriersService.Item> items = new List<RLCarriersService.Item>();
                List<RLCarriersService.RQAccessorial> accessorials = new List<RLCarriersService.RQAccessorial>();


                objrequest.QuoteType = RLCarriersService.QuoteType.Domestic;
              

                objrequest.Origin = new RLCarriersService.ServicePoint();
                // objrequest.Origin.City = Convert.ToString(objShipping_InfoArray[0].From_City); //"WILMINGTON";
                // objrequest.Origin.StateOrProvince = Convert.ToString(objShipping_InfoArray[0].From_State);// "OH";
                // objrequest.Origin.ZipOrPostalCode = Convert.ToString(objShipping_InfoArray[0].From_Zip_Code); //"45177";
                //  objrequest.Origin.CountryCode = Convert.ToString(objShipping_InfoArray[0].From_Country);//"USA";
                objrequest.Origin.City = Convert.ToString(objFreightParameter_List[0].ShipperCity); //"WILMINGTON";
                objrequest.Origin.StateOrProvince = Convert.ToString(objFreightParameter_List[0].ShipperState);// "OH";
                objrequest.Origin.ZipOrPostalCode = Convert.ToString(objFreightParameter_List[0].ShipperZipCode); //"45177";
                objrequest.Origin.CountryCode = Convert.ToString(objFreightParameter_List[0].ShipperCountry);//"USA";

                //Add the Destination 
                objrequest.Destination = new RLCarriersService.ServicePoint();
                objrequest.Destination.City = Convert.ToString(objFreightParameter_List[0].RecipientCity);//"ALPHARETTA";
                objrequest.Destination.StateOrProvince = Convert.ToString(objFreightParameter_List[0].RecipientState); //"GA";
                objrequest.Destination.ZipOrPostalCode = Convert.ToString(objFreightParameter_List[0].RecipientZipCode); //"30005";
                objrequest.Destination.CountryCode = Convert.ToString(objFreightParameter_List[0].RecipientCountry); //"USA";
                decimal weight = Convert.ToDecimal(objFreightParameter_List[0].PackageWeight);
                //Add the Handling Units
                items.Add(new RLCarriersService.Item()
                {
                    Class = "60",
                    Weight = (float)weight //weight in lbs
                    
                    

                });
               
          
                objrequest.Items = items.ToArray();
               // objrequest.Pallets

                //Add any accessorials to be added in the quote.
                //accessorials.Add(RLCarriersService.RQAccessorial.Freezable);
                //accessorials.Add(RLCarriersService.RQAccessorial.DestinationLiftgate);
                //objrequest.Accessorials = accessorials.ToArray();

                //If any of the freight is over dimesion, set the number of pieces that are over dimension:
               // objrequest.OverDimensionList = new List<RLCarriersService.OverDimension>

                //If there is a declared value for the freight include it here
                //objrequest.DeclaredValue = 1500;

                objresponse = objRLCarrier.GetRateQuote(ConfigurationManager.AppSettings["APIKey"], objrequest);

                if (objresponse.WasSuccess == true)
                {
                    RnlCharges = objresponse.Result.ServiceLevels;
                }
                else
                {
                    RnlCharges = null;
                }



                return View("GetRL", RnlCharges);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);

            }
            return View("GetRL", RnlCharges);
        }

       

        [HttpPost]
        [Route("SubmitRL")]
        public ActionResult SubmitRL(string FEDEXValue, string FEDEXText, string DocumentNo, string LineNo, string OrderType)
        {
            try
            {

                string navUrl = ConfigurationManager.AppSettings["NavUrl"];
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = navUrl + "/Codeunit/Web_Transaction";

                

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                objWeb_Transaction_PortClient = new UPSWebTransaction.Web_Transaction_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objWeb_Transaction_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objWeb_Transaction_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                string rbtext = FEDEXText;     //UPSText.Split(' ')[2];
                string rbvalue = FEDEXValue;
                objWeb_Transaction_PortClient.UpdateUPSChargesAsync(Convert.ToInt32(OrderType), DocumentNo, Convert.ToInt32(LineNo), rbtext, rbvalue);

                return Json("success");
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
                return Json("failure");
            }
        }


        [HttpPost]
        [Route("SubmitRQ")]
        public ActionResult SubmitRQ(string FEDEXValue, string FEDEXText, string DocumentNo, string LineNo, string OrderType)
        {
            try
            {

                string navUrl = ConfigurationManager.AppSettings["NavUrl"];
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = navUrl + "/Codeunit/Web_Transaction";



                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                objWeb_Transaction_PortClient = new UPSWebTransaction.Web_Transaction_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objWeb_Transaction_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objWeb_Transaction_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                double ratequote = Convert.ToDouble(FEDEXValue);
                double rate = (ratequote / 100);
                string rbtext = FEDEXText;     //UPSText.Split(' ')[2];
                string rbvalue = rate.ToString();
                objWeb_Transaction_PortClient.UpdateUPSChargesAsync(Convert.ToInt32(OrderType), DocumentNo, Convert.ToInt32(LineNo), rbtext, rbvalue);

                return Json("success");
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
                return Json("failure");
            }
        }


        [HttpGet()]
        [Route("RateQuote/{Document_No}/{Line_No}")]
      
        public async Task<ActionResult> RateQuote(string Document_No, string Line_No)
        {
             var result = string.Empty;
            string access_token = string.Empty;
            RootObjectMain objresponse = null;

                string Result = string.Empty;
                string resultJSON = string.Empty;
                string token = string.Empty;
                string stringData;
                string username = ConfigurationManager.AppSettings["Rqyrcuser"];
                string password = ConfigurationManager.AppSettings["Rqyrcpass"];
                RootObject rootObject = new RootObject();
                HttpClientHandler handler = new HttpClientHandler();
                var busId = ConfigurationManager.AppSettings["BusID"];

            ViewBag.URL = _host;
            ViewBag.DocumentNo = Document_No;
            ViewBag.LineNo = Line_No;
            _navUrl = ConfigurationManager.AppSettings["NavUrl"];

           
            try
            {
                using (var client = new HttpClient())
                {

                    var url = ConfigurationManager.AppSettings["yrcRatequoteURl"];

                    client.BaseAddress = new Uri(url);
                    

                    Login objlogin = new Login();
                    objlogin.username = username;
                    objlogin.password = password;
                    objlogin.busId = busId;
                    objlogin.busRole = "Shipper";
                    objlogin.paymentTerms = "Prepaid";

                    rootObject.login = objlogin;

                    ViewBag.URL = _host;
                    ViewBag.DocumentNo = Document_No;
                    ViewBag.LineNo = Line_No;
                    _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                    BasicHttpBinding objNavWSBinding = null;
                    objNavWSBinding = new BasicHttpBinding();
                    objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                    objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                    objNavWSBinding.MaxReceivedMessageSize = 2147483647;


                    #region Shipping_Info
                    objCustomerDetails = new CustomerDetails();
                    objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                    objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                    objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                    objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;
                    //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                    #endregion

                    #region FreightParameter_List
                    objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                    FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                    FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                    //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                    FreightParamListFilter = new FreightParameter_List_Filter();
                    FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                    FreightParamListFilter.Criteria = "=" + Document_No;

                    FreightParamListFilter1 = new FreightParameter_List_Filter();
                    FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                    FreightParamListFilter1.Criteria = "=" + Line_No;

                    FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                    objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                    if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                    {
                        ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                    }
                    #endregion

                    #region SalesLineCarton
                    objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                    objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                    objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                    //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                    objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                    objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                    objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                    objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                    objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                    objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                    Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                    objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                    if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                        ViewBag.OrderType = "2";
                    else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                        ViewBag.OrderType = "1";
                    #endregion


                    OriginLocation originLocation = new OriginLocation();
                    originLocation.state =  Convert.ToString(objFreightParameter_List[0].ShipperState);// "AZ";
                    originLocation.city = Convert.ToString(objFreightParameter_List[0].ShipperCity);//"Glendale";
                    originLocation.postalCode = Convert.ToString(objFreightParameter_List[0].ShipperZipCode); //"85301";
                    originLocation.country = "USA"; //Convert.ToString(objFreightParameter_List[0].ShipperCountry);

                    rootObject.originLocation = originLocation;

                    DestinationLocation destinationLocation = new DestinationLocation();
                    destinationLocation.state = Convert.ToString(objFreightParameter_List[0].RecipientState);//"MD";
                    destinationLocation.city = Convert.ToString(objFreightParameter_List[0].RecipientCity);//"Columbia";
                    destinationLocation.postalCode = Convert.ToString(objFreightParameter_List[0].RecipientZipCode);//"21045";

                    destinationLocation.country = "USA"; //Convert.ToString(objFreightParameter_List[0].RecipientCountry);
                    rootObject.destinationLocation = destinationLocation;

                    Details objdetails = new Details();
                    objdetails.pickupDate = "20200220";
                    objdetails.serviceClass = "STD";
                    objdetails.typeQuery = "QUOTE";
                    rootObject.details = objdetails;

                    //Commodity commodity = new Commodity();


                    ListOfCommodities listOfCommodities = new ListOfCommodities();

                    List<Commodity> commodity = new List<Commodity>();

                    commodity.Add(new Commodity()
                    {
                        nmfcClass = 60,
                        handlingUnits = 1,
                        packageCode = "SKD",
                        packageLength = 11,
                        packageWidth = 12,
                        packageHeight = 10,
                        weight = 17
                    });

                    listOfCommodities.commodity = commodity;
                    rootObject.listOfCommodities = listOfCommodities;


                    ServiceOpts serviceOpts = new ServiceOpts();
                    List<string> accOptions = new List<string>();
                    accOptions.Add("NTFY");
                    serviceOpts.accOptions = accOptions;
                    rootObject.serviceOpts = serviceOpts;

                    stringData = JsonConvert.SerializeObject(rootObject);



                    var response = await client.PostAsync(url, new StringContent(stringData, Encoding.UTF8, "application/json"));
                    result = response.Content.ReadAsStringAsync().Result;

                    if (result != null)
                    {
                        objresponse = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<RootObjectMain>(result);
                        if (objresponse.isSuccess==true)
                        {
                            return View("RateQuote", objresponse);
                        }

                        //dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RootObjectMain>>(result);
                    }
                    
                    //response.EnsureSuccessStatusCode();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                //return Json(resultJSON, JsonRequestBehavior.AllowGet);
            }

            return View("RateQuote", objresponse);

        }

    }
}