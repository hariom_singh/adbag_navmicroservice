using Adbag_NavMicroservice.ArtworkApproval;
using Adbag_NavMicroservice.SalesLineWeb;
using Adbag_NAVMicroservice.Models;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Diagnostics;
using System.IO.Compression;
using System.Net.Security;
using System.Text;
using System.Threading;
using FluentFTP;
using System.ComponentModel;
using Adbag_NavMicroservice.CustomerComment;
using PdfiumViewer;
using Bytescout.PDFRenderer;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using EnterpriseDT.Net.Ftp;


namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("Artwork")]
    public class ArtworkApprovalController : Controller
    {

        private string _host, _navUrl;
        WebClient webClient;               // Our WebClient that will be doing the downloading for us
        Stopwatch sw = new Stopwatch();  
        BasicHttpBinding objNavWSBinding = null;
        //SystemService_PortClient systemService = null;
        WS_Artwork_Approval_Listing_PortClient artworkService = null;
        WS_Artwork_Approval_Listing objArtworkListingRef = null;
        WS_Artwork_Approval_Listing_Filter[] objArtworkFilter;

        Sales_Line_Web_PortClient objSalesLineWeb_PortClient = null;
        Sales_Line_Web objSalesLineWeb = null;
        Sales_Line_Web_Filter[] objSalesLineWeb_Filter = null;
        ArtworkApprovalDetails objArtworkApprovalDetails = null;

        SelectedCommonFactoryNotLog_PortClient customerCommentservice = null;
        SelectedCommonFactoryNotLog objCustcomlistRef = null;
        SelectedCommonFactoryNotLog_Filter[] objCustcomFilter = null;
        SelectedCommonFactoryNotLog objcustomercomment = null;

        private object MessageBox;
        string strimgname = String.Empty;

        
        [Route("GetArtworkDetails/{PrimaryKey}/{IsSO}")]
        [HttpGet()]
        public async Task<ActionResult> GetArtworkDetails(string PrimaryKey, string IsSO)
        {
            try
                 
            {
                 string OrderNo = string.Empty;
                _navUrl = ConfigurationManager.AppSettings["NavUrl"].ToString();
                _host = ConfigurationManager.AppSettings["HostURL"].ToString();
                //Added By Swapnil for Assorted
                string NewPrimaryKey = string.Empty;
                string Notes = string.Empty;



                string SO = IsSO;
                string documentno = string.Empty;
                if (SO == "Yes")
                {
                    
                    if (PrimaryKey.Contains("{") && PrimaryKey.Contains("}"))
                    {
                        OrderNo = ConvertGUID(PrimaryKey, IsSO);
                        string[] order = OrderNo.Split('|');
                        OrderNo = order[0];
                        documentno= order[1];
                    }
                    else
                    {
                        OrderNo = PrimaryKey;
                    }

                    BasicHttpBinding objNavWSBindingNew = null;
                    WS_Artwork_Approval_Listing_PortClient artworkServiceNew = null;
                    WS_Artwork_Approval_Listing objArtworkListingRefNew = null;
                    WS_Artwork_Approval_Listing_Filter[] objArtworkFilterNew;

                    objNavWSBindingNew = new BasicHttpBinding();
                    objNavWSBindingNew.MaxReceivedMessageSize = Int32.MaxValue;
                    objNavWSBindingNew.MaxBufferSize = Int32.MaxValue;
                    objNavWSBindingNew.Security.Mode = BasicHttpSecurityMode.Transport;
                    objNavWSBindingNew.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;



                    string strCustomerPageURLNew = _navUrl + "/Page/WS_Artwork_Approval_Listing";

                    artworkServiceNew = new WS_Artwork_Approval_Listing_PortClient(objNavWSBindingNew, new EndpointAddress(strCustomerPageURLNew));
                    //artworkServiceNew.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                    artworkServiceNew.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    artworkServiceNew.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                    objArtworkListingRefNew = new WS_Artwork_Approval_Listing();

                    objArtworkFilterNew = new WS_Artwork_Approval_Listing_Filter[2];
                    objArtworkFilterNew[0] = new WS_Artwork_Approval_Listing_Filter();
                    objArtworkFilterNew[0].Field = WS_Artwork_Approval_Listing_Fields.Order_No;
                    objArtworkFilterNew[0].Criteria = "=" + OrderNo;

                    objArtworkFilterNew[1] = new WS_Artwork_Approval_Listing_Filter();
                    objArtworkFilterNew[1].Field = WS_Artwork_Approval_Listing_Fields.Line_No;
                    objArtworkFilterNew[1].Criteria = "=" + documentno;

                    WS_Artwork_Approval_Listing[] obj2_New = artworkServiceNew.ReadMultipleAsync(objArtworkFilterNew, null, 0).Result.ReadMultiple_Result1;


                    string Comments = _navUrl + "/Page/SelectedCommonFactoryNotLog";

                    customerCommentservice = new SelectedCommonFactoryNotLog_PortClient(objNavWSBindingNew, new EndpointAddress(Comments));
                    //artworkServiceNew.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                    customerCommentservice.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    customerCommentservice.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                    objCustcomlistRef = new SelectedCommonFactoryNotLog();

                    objCustcomFilter = new SelectedCommonFactoryNotLog_Filter[1];
                    objCustcomFilter[0] = new SelectedCommonFactoryNotLog_Filter();
                    objCustcomFilter[0].Field = SelectedCommonFactoryNotLog_Fields.Document_No;
                    objCustcomFilter[0].Criteria = "=" + OrderNo;


                    SelectedCommonFactoryNotLog[] obj3 = customerCommentservice.ReadMultipleAsync(objCustcomFilter, null, 0).Result.ReadMultiple_Result1;
                    if (obj3 != null)
                    {
                        for (int i = 0; i < obj3.Count(); i++)
                        {
                            Notes = Notes + "," + Convert.ToString(obj3[i].Notes);
                        }
                    }

                        if (obj2_New != null)
                    {
                        if (obj2_New.Length > 0)
                        {
                            for (int i = 0; i < obj2_New.Count(); i++)
                            {
                                NewPrimaryKey = NewPrimaryKey + "|" + obj2_New[i].Primary_Key.ToString();
                            }
                            NewPrimaryKey = NewPrimaryKey.TrimStart('|');
                        }
                    }
                }
                else
                {
                    if (PrimaryKey.Contains("{") && PrimaryKey.Contains("}"))
                    {
                        NewPrimaryKey = ConvertGUID(PrimaryKey, IsSO);
                    }
                    else
                    {
                        NewPrimaryKey = PrimaryKey;
                    }

                   
                }
                //End By Swapnil

                ViewBag.URL = _host;
                ViewBag.PrimaryKey = PrimaryKey;
                ViewBag.NewPrimaryKey = NewPrimaryKey;
                ViewBag.IsSO = IsSO;

                objArtworkApprovalDetails = new ArtworkApprovalDetails();
                // Create a NAV compatible binding
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.MaxReceivedMessageSize = Int32.MaxValue;
                objNavWSBinding.MaxBufferSize = Int32.MaxValue;
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                string strCustomerPageURL = _navUrl + "/Page/WS_Artwork_Approval_Listing";
                artworkService = new WS_Artwork_Approval_Listing_PortClient(objNavWSBinding, new EndpointAddress(strCustomerPageURL));
                //artworkService.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                artworkService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                artworkService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objArtworkListingRef = new WS_Artwork_Approval_Listing();

                objArtworkFilter = new WS_Artwork_Approval_Listing_Filter[NewPrimaryKey.Split('|').Length];
                for (int i = 0; i < NewPrimaryKey.Split('|').Length; i++)
                {
                    objArtworkFilter[i] = new WS_Artwork_Approval_Listing_Filter();
                    objArtworkFilter[i].Field = WS_Artwork_Approval_Listing_Fields.Primary_Key;
                    objArtworkFilter[i].Criteria = "=" + NewPrimaryKey;
                }

                WS_Artwork_Approval_Listing[] obj2 = artworkService.ReadMultipleAsync(objArtworkFilter, null, 0).Result.ReadMultiple_Result1;

                
                //objArtworkApprovalDetails.SalesOrder = objArtworkListingRef.Order_No.Trim();
                if (obj2 != null)
                {
                    if (obj2.Length > 0)
                    {
                        string lsColors = string.Empty;
                        string ItemNo = string.Empty;
                        int PrevItemNo = 0;
                        bool isPreProduction = false;

                        //Added By Swapnil
                        string strItemNumber = string.Empty;
                        string strQuantity = string.Empty;
                        string strItemColor = string.Empty;
                        string strlocation = string.Empty;
                        string strcustomerPO = string.Empty;

                        //End

                        string strcolour = string.Empty;
                        for (int i = 0; i < obj2.Length; i++)
                        {
                            if (obj2[i].Is_Pre_Production_Item)
                                isPreProduction = true;
                            objArtworkApprovalDetails.CustomerPO = Convert.ToString(obj2[i].External_Document_No);
                        }
                        if (isPreProduction)
                        {
                            for (int i = 0; i < obj2.Length; i++)
                            {
                                
                                if (obj2[i].Is_Pre_Production_Item)
                                {
                                    lsColors = string.Empty;
                                    if (obj2[i].Line_No == PrevItemNo)
                                    {
                                        objArtworkApprovalDetails.ItemDescription = string.Empty;
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(obj2[i].Item_No))
                                        {
                                            strItemNumber = strItemNumber + "," + Convert.ToString(obj2[i].Item_No);
                                        }

                                        objArtworkApprovalDetails.ItemDescription = Convert.ToString(obj2[i].recItem_Description);

                                        var ObjQtyVal = await artworkService.GetOrderQuantityAsync(obj2[i].Key);
                                        if (obj2[i].Full_Back_Order)
                                        {
                                            strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value);
                                            //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value);
                                        }
                                        else
                                        {
                                            strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].Back_Order);
                                            //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].BackOrder);
                                        }

                                        if (obj2[i].Is_Pre_Production_Item)
                                        {
                                            var ObjActualQty = await artworkService.GetActualQuantityAsync(obj2[i].Key);
                                            objArtworkApprovalDetails.ActualQuantity = Convert.ToString(ObjActualQty.return_value);
                                        }

                                        if (!string.IsNullOrEmpty(obj2[i].Web_Color))
                                        {
                                            strItemColor = strItemColor + "," + Convert.ToString(obj2[i].Web_Color);
                                        }
                                    }
                                    if (obj2[i].Method != null)
                                    {
                                        var ObjQtyImprint = await artworkService.GetImprintMethodNameAsync(obj2[i].Key, obj2[i].Method.Trim());
                                        objArtworkApprovalDetails.ImprintMethod = Convert.ToString(ObjQtyImprint.return_value);

                                    }
                                    else
                                        objArtworkApprovalDetails.ImprintMethod = string.Empty;

                                    objArtworkApprovalDetails.ImprintLocation = Convert.ToString(obj2[i].Location);

                                    if (obj2[i].Imprint_Location_Size != null)
                                    {
                                        objArtworkApprovalDetails.ImprintSize = Convert.ToString(obj2[i].Imprint_Location_Size) + " Inches";
                                    }

                                    GetImprintColors Objcolor = new GetImprintColors()
                                    {
                                        imprint = obj2[i].Key,
                                        imprintColors = "",
                                    };
                                    var objimpcolor = await artworkService.GetImprintColorsAsync(Objcolor);
                                    lsColors = objimpcolor.imprintColors.Replace("|", ", ").Trim().TrimEnd(',');
                                    if (lsColors.Contains(','))
                                    {
                                        string strImprintColors = lsColors.Replace(",", "\r\n");
                                        objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strImprintColors.ToLower());
                                    }
                                    else
                                    {
                                        objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(lsColors.ToLower());
                                    }

                                    GetProductColors objProdcol = new GetProductColors()
                                    {
                                        imprint = obj2[i].Key,
                                        productColors = "",
                                        product = "",
                                    };

                                    var objimprintcolor = await artworkService.GetProductColorsAsync(objProdcol);
                                }
                            }
                        }
                        else
                        {
                            Notes = string.Empty;
                            for (int i = 0; i < obj2.Length; i++)
                            {
                                BasicHttpBinding objNavWSBindingNew = null;
                                objNavWSBindingNew = new BasicHttpBinding();
                                objNavWSBindingNew.MaxReceivedMessageSize = Int32.MaxValue;
                                objNavWSBindingNew.MaxBufferSize = Int32.MaxValue;
                                objNavWSBindingNew.Security.Mode = BasicHttpSecurityMode.Transport;
                                objNavWSBindingNew.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;


                                string Comments = _navUrl + "/Page/SelectedCommonFactoryNotLog";

                                customerCommentservice = new SelectedCommonFactoryNotLog_PortClient(objNavWSBindingNew, new EndpointAddress(Comments));
                                //artworkServiceNew.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                                customerCommentservice.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                                customerCommentservice.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                                objCustcomlistRef = new SelectedCommonFactoryNotLog();

                                objCustcomFilter = new SelectedCommonFactoryNotLog_Filter[1];
                                objCustcomFilter[0] = new SelectedCommonFactoryNotLog_Filter();
                                objCustcomFilter[0].Field = SelectedCommonFactoryNotLog_Fields.Document_No;
                                objCustcomFilter[0].Criteria = "=" + obj2[i].Order_No;


                                SelectedCommonFactoryNotLog[] obj3 = customerCommentservice.ReadMultipleAsync(objCustcomFilter, null, 0).Result.ReadMultiple_Result1;
                                if (obj3 != null)
                                {
                                    for (int j = 0; j < obj3.Count(); j++)
                                    {
                                        Notes = Notes + "," + Convert.ToString(obj3[j].Notes);
                                    }
                                }
                            
                            lsColors = string.Empty;
                                if (obj2[i].Line_No == PrevItemNo)
                                {
                                    //objArtworkApprovalDetails.ItemNumber = string.Empty;
                                    objArtworkApprovalDetails.ItemDescription = string.Empty;
                                    //objArtworkApprovalDetails.Quantity = string.Empty;
                                    //objArtworkApprovalDetails.ItemColor = string.Empty;
                                }
                                else
                                {
                                    //if (!string.IsNullOrEmpty(obj2[i].Item_No))
                                    //{
                                    //    strItemNumber = strItemNumber + "," + Convert.ToString(obj2[i].Item_No);
                                    //}
                                    var objParent = await artworkService.GetParentProductAsync(obj2[i].Key);

                                    strItemNumber = objParent.return_value;


                                    objArtworkApprovalDetails.ItemDescription = Convert.ToString(obj2[i].recItem_Description);

                                    var ObjQtyVal = await artworkService.GetOrderQuantityAsync(obj2[i].Key);
                                    if (obj2[i].Full_Back_Order)
                                    {
                                        strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value);
                                        //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value);
                                    }
                                    else
                                    {
                                        strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].Back_Order);
                                        //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].BackOrder);
                                    }

                                    if (obj2[i].Is_Pre_Production_Item)
                                    {
                                        var ObjActualQty = await artworkService.GetActualQuantityAsync(obj2[i].Key);
                                        objArtworkApprovalDetails.ActualQuantity = Convert.ToString(ObjActualQty.return_value);
                                    }

                                    if (!string.IsNullOrEmpty(obj2[i].Web_Color))
                                    {
                                        strItemColor = strItemColor + "," + Convert.ToString(obj2[i].Web_Color);
                                    }
                                }
                                if (obj2[i].Method != null)
                                {
                                    var ObjQtyImprint = await artworkService.GetImprintMethodNameAsync(obj2[i].Key, obj2[i].Method.Trim());
                                    objArtworkApprovalDetails.ImprintMethod = Convert.ToString(ObjQtyImprint.return_value);
                                    objArtworkApprovalDetails.ImprintMethod = (Convert.ToString(obj2[i].Method));

                                }
                                else
                                    objArtworkApprovalDetails.ImprintMethod = string.Empty;
                                //here

                                if (obj2[i].Location != null)
                                {
                                    strlocation = strlocation + "," + Convert.ToString(obj2[i].Location);
                                    //  objArtworkApprovalDetails.ImprintLocation = Convert.ToString(obj2[i].Location);
                                }

                                if (obj2[i].Imprint_Location_Size != null)
                                {
                                    objArtworkApprovalDetails.ImprintSize = Convert.ToString(obj2[i].Imprint_Location_Size) + " Inches";
                                }

                                GetImprintColors Objcolor = new GetImprintColors()
                                {
                                    imprint = obj2[i].Key,
                                    imprintColors = "",
                                };
                                var objimpcolor = await artworkService.GetImprintColorsAsync(Objcolor);
                                lsColors = objimpcolor.imprintColors.Replace("|", ", ").Trim().TrimEnd(','); 

                               

                                if(obj2.Length==1)
                                {
                                    if (lsColors.Contains(','))
                                    {
                                        string strImprintColors = lsColors.Replace(",", "\r\n");
                                        objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strImprintColors.ToLower());
                                        strcolour = strcolour + "," + Convert.ToString(objArtworkApprovalDetails.ImprintColors);
                                    }
                                    else
                                    {
                                        objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(lsColors.ToLower());
                                        strcolour = strcolour + "," + Convert.ToString(objArtworkApprovalDetails.ImprintColors);
                                    }
                                }

                               
                                else
                                {
                                    string strImprintColors = lsColors.Replace(",", "\r\n");
                                    objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strImprintColors.ToLower());
                                    strcolour = strcolour + "," + Convert.ToString(objArtworkApprovalDetails.ImprintColors);
                                }

                                GetProductColors objProdcol = new GetProductColors()
                                {
                                    imprint = obj2[i].Key,
                                    productColors = "",
                                    product = "",
                                };

                              
                                var objProdcolor = await artworkService.GetProductColorsAsync(objProdcol);
                                objArtworkApprovalDetails.ProductColors = objProdcolor.productColors.Replace("|", ",");

                              
                         

                                if (obj2[0].Proof_Image==null)
                                {
                                    for (int j=0; j<obj2.Length; j++)
                                    {
                                        if(obj2[i].Proof_Image!=null)
                                        {
                                            strimgname = obj2[i].Proof_Image;
                                        }
                                    }
                                   
                                }

                                if(obj2[i].External_Document_No != null)
                                {
                                    strcustomerPO = strcustomerPO + "," + Convert.ToString(obj2[i].External_Document_No);
                                }
                               
                            }

                        }

                        //Added By Swapnil
                        strItemNumber = strItemNumber.TrimStart(',');
                        strQuantity = strQuantity.TrimStart(',');
                        strItemColor = strItemColor.TrimStart(',');
                        strlocation = strlocation.TrimStart(',');
                        strcolour= strcolour.TrimStart(',');
                        Notes= Notes.TrimStart(',');
                        strcustomerPO = strcustomerPO.TrimStart(',');

                        //objArtworkApprovalDetails.ItemNumber = strItemNumber;
                        objArtworkApprovalDetails.Quantity = strQuantity;
                        objArtworkApprovalDetails.ItemColor = strItemColor;
                        objArtworkApprovalDetails.ImprintLocation = strlocation;
                        objArtworkApprovalDetails.ImprintColors = strcolour;
                        objArtworkApprovalDetails.CommentsByCustomer = Notes;
                        objArtworkApprovalDetails.CustomerPOnumber = strcustomerPO;
                        //End

                        objArtworkListingRef = obj2[0];
                        objArtworkApprovalDetails.ItemNumber = strItemNumber;
                        objArtworkApprovalDetails.SalesOrder = Convert.ToString(objArtworkListingRef.Order_No);
                        objArtworkApprovalDetails.ArtFile = Convert.ToString(objArtworkListingRef.Art_ID);
                        objArtworkApprovalDetails.SalesPerson = Convert.ToString(objArtworkListingRef.recsh_Salesperson_Code);
                        objArtworkApprovalDetails.NoOfRework = Convert.ToString(objArtworkListingRef.No_of_Rework);
                        objArtworkApprovalDetails.CommentsForCustomer= Convert.ToString(objArtworkListingRef.Customer_Comment);
                        if (!string.IsNullOrEmpty(objArtworkListingRef.Artwork_DateTime.ToString()))
                        {
                            string[] _splitDate = Convert.ToString(objArtworkListingRef.Artwork_DateTime).Split(' ');
                            if (_splitDate.Count() > 0)
                            {
                                objArtworkApprovalDetails.ArtworkDate = !string.IsNullOrEmpty(_splitDate[0].ToString()) ? _splitDate[0].ToString() : string.Empty;
                                objArtworkApprovalDetails.ArtworkTime = (!string.IsNullOrEmpty(_splitDate[1].ToString()) ? _splitDate[1].ToString() : string.Empty) + (!string.IsNullOrEmpty(_splitDate[2].ToString()) ? _splitDate[2].ToString() : string.Empty);
                            }
                        }
                        objArtworkApprovalDetails.IsBlindPage = objArtworkListingRef.Blind_Page;
                       

                        if (!string.IsNullOrEmpty(objArtworkListingRef.Order_No.ToString()) || objArtworkListingRef.Order_No != null)
                        {
                            try
                            {
                                #region SalesLine Web
                                string strCustomerPageURLSales = _navUrl + "/Page/Sales_Line_Web";
                                objSalesLineWeb_PortClient = new Sales_Line_Web_PortClient(objNavWSBinding, new EndpointAddress(strCustomerPageURLSales));
                                //objSalesLineWeb_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                                objSalesLineWeb_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                                objSalesLineWeb_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                                objSalesLineWeb = new Sales_Line_Web();

                                objSalesLineWeb_Filter = new Sales_Line_Web_Filter[1];
                                objSalesLineWeb_Filter[0] = new Sales_Line_Web_Filter();
                                objSalesLineWeb_Filter[0].Field = Sales_Line_Web_Fields.Document_No;
                                objSalesLineWeb_Filter[0].Criteria = "=" + Convert.ToString(objArtworkListingRef.Order_No);

                                Sales_Line_Web[] objSLB = objSalesLineWeb_PortClient.ReadMultipleAsync(objSalesLineWeb_Filter, null, 0).Result.ReadMultiple_Result1;
                                if (objSLB != null)
                                {
                                    if (objSLB.Length > 0)
                                    {
                                        if (SO == "Yes")
                                        {
                                            objArtworkApprovalDetails.PackagingOption = Convert.ToString(objSLB[0].Packaging_Option);
                                        }
                                        else
                                        {
                                            string strPackagingOption = string.Empty;
                                            for (int i = 0; i < objSLB.Count(); i++)
                                            {
                                                strPackagingOption = strPackagingOption + "," + Convert.ToString(objSLB[i].Packaging_Option);
                                            }
                                            strPackagingOption = strPackagingOption.TrimStart(',');
                                            objArtworkApprovalDetails.PackagingOption = strPackagingOption;
                                        }
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                            }
                        }
                    }
                }

           
                //string webRootPath = _hostingEnvironment.WebRootPath;
                Array.ForEach(Directory.GetFiles(Server.MapPath("~/FTPImages/ArtworkApproval/")), System.IO.File.Delete);
                //string ftpFileSourcePath = "ftp://pwpdc.powerweave.com/clothpromotions-Images/Artwork/Proof/" + Convert.ToString(objArtworkListingRef.Order_No) + "/ArtWork/";

                //if (!string.IsNullOrEmpty(objArtworkListingRef.Proof_Image))
                //{
                //    DownloadFile("clothpromotions", "Cl0ThProM0T!on$%543", ftpFileSourcePath + objArtworkListingRef.Proof_Image.Trim(), webRootPath + "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Proof_Image.Trim());
                //    objArtworkApprovalDetails.ImagePath1 = "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Proof_Image.Trim();
                //}
                //if (!string.IsNullOrEmpty(objArtworkListingRef.Ruler_Image))
                //{
                //    DownloadFile("clothpromotions", "Cl0ThProM0T!on$%543", ftpFileSourcePath + objArtworkListingRef.Ruler_Image.Trim(), webRootPath + "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Ruler_Image.Trim());
                //    objArtworkApprovalDetails.ImagePath2 = "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Ruler_Image.Trim();
                //}
                if (!string.IsNullOrEmpty(objArtworkListingRef.Proof_Image))
                {
                    ViewBag.Image1 = objArtworkListingRef.Proof_Image;
                }
                if(objArtworkListingRef.Proof_Image==null)
                {
                    ViewBag.Image1 = strimgname;
                }

                if (!string.IsNullOrEmpty(objArtworkListingRef.Ruler_Image))
                {
                    ViewBag.Image2 = objArtworkListingRef.Ruler_Image;
                }
                ViewBag.OrderNo = Convert.ToString(objArtworkListingRef.Order_No);
                ViewBag.Pdf = Convert.ToString(objArtworkListingRef.Proof_Pdf);
                ViewBag.pdfpath = "~/FTPImages/ArtworkApproval/" + objArtworkListingRef.Proof_Pdf.Trim();
                // Check artwork has been evaluated by customer or not
                if (objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Rejected_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof_with_Suggestions)
                {
                    if (objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof_with_Suggestions)
                    {
                        objArtworkApprovalDetails.Status = "This proof is already approved.";
                        if (objArtworkListingRef.Proof_Approved_DateSpecified)
                        {
                            if (objArtworkListingRef.Proof_Approved_Date != null)
                            {
                                if (objArtworkListingRef.Proof_Approved_Date.ToShortDateString().Trim() != "1/1/0001")
                                {
                                    objArtworkApprovalDetails.Status = "This proof is already approved on " + objArtworkListingRef.Proof_Approved_Date.ToShortDateString();
                                }
                            }
                        }
                    }
                    else if (objArtworkListingRef.Status == Status.Customer_Rejected_Paper_Proof)
                    {
                        objArtworkApprovalDetails.Status = "This proof is already rejected.";
                        if (objArtworkListingRef.Proof_Approved_DateSpecified)
                        {
                            if (objArtworkListingRef.Proof_Approved_Date != null)
                            {
                                if (objArtworkListingRef.Proof_Approved_Date.ToShortDateString().Trim() != "1/1/0001")
                                    objArtworkApprovalDetails.Status = "This proof is already rejected on " + objArtworkListingRef.Proof_Approved_Date.ToShortDateString();
                            }
                        }
                    }
                }
                else
                {
                    objArtworkApprovalDetails.Status = "Ok";
                }
                //End

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("GetArtworkDetails", objArtworkApprovalDetails);
        }
        private string ConvertGUID(string GUIDKey, string IsSO)
        {
            string key = string.Empty;
            string Documentno = string.Empty;
            try
            {
                BasicHttpBinding objNavWSBindingGUID = null;
                WS_Artwork_Approval_Listing_PortClient artworkServiceGUID = null;
                WS_Artwork_Approval_Listing objArtworkListingRefGUID = null;
                WS_Artwork_Approval_Listing_Filter[] objArtworkFilterGUID;

                objNavWSBindingGUID = new BasicHttpBinding();
                objNavWSBindingGUID.MaxReceivedMessageSize = Int32.MaxValue;
                objNavWSBindingGUID.MaxBufferSize = Int32.MaxValue;
                objNavWSBindingGUID.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBindingGUID.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                string strCustomerPageURLNew = _navUrl + "/Page/WS_Artwork_Approval_Listing";

                artworkServiceGUID = new WS_Artwork_Approval_Listing_PortClient(objNavWSBindingGUID, new EndpointAddress(strCustomerPageURLNew));
                //artworkServiceGUID.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                artworkServiceGUID.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                artworkServiceGUID.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objArtworkListingRefGUID = new WS_Artwork_Approval_Listing();

                objArtworkFilterGUID = new WS_Artwork_Approval_Listing_Filter[1];
                objArtworkFilterGUID[0] = new WS_Artwork_Approval_Listing_Filter();
                objArtworkFilterGUID[0].Field = WS_Artwork_Approval_Listing_Fields.GUID;
                objArtworkFilterGUID[0].Criteria = "=" + GUIDKey;

                WS_Artwork_Approval_Listing[] obj2_GUID = artworkServiceGUID.ReadMultipleAsync(objArtworkFilterGUID, null, 0).Result.ReadMultiple_Result1;

                if (IsSO == "Yes")
                {
                    if (obj2_GUID != null)
                    {
                        if (obj2_GUID.Length > 0)
                        {
                            key = obj2_GUID[0].Order_No;
                            Documentno = "|"+obj2_GUID[0].Line_No.ToString();
                        }
                    }
                }
                else
                {
                    if (obj2_GUID != null)
                    {
                        if (obj2_GUID.Length > 0)
                        {
                            key = Convert.ToString(obj2_GUID[0].Primary_Key);
                            //Documentno = obj2_GUID[0].Line_No.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return key+ Documentno;
        }

        public void download(string remoteFile, string localFile, string user, string pass)
        {
            try


            {    FtpWebRequest ftpRequest = null;
                 FtpWebResponse ftpResponse = null;
                 Stream ftpStream = null;
                 int bufferSize = 2048;
                 bool UsePassive = false;

        /* Create an FTP Request */
        ftpRequest = (FtpWebRequest)FtpWebRequest.Create(remoteFile);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(user, pass);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = UsePassive;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Get the FTP Server's Response Stream */
                ftpStream = ftpResponse.GetResponseStream();
                /* Open a File Stream to Write the Downloaded File */
                FileStream localFileStream = new FileStream(localFile, FileMode.Create);
                /* Buffer for the Downloaded Data */
                byte[] byteBuffer = new byte[bufferSize];
                int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                /* Download the File by Writing the Buffered Data Until the Transfer is Complete */
                try
                {
                    while (bytesRead > 0)
                    {
                        localFileStream.Write(byteBuffer, 0, bytesRead);
                        bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                    }
                }
                catch (Exception ex) { throw new Exception(ex.Message); }
                /* Resource Cleanup */
                localFileStream.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return;
        }

        // public async Task<string> GetReadFileFromFTP([FromBody] FTPFileUpload objFTPFileUpload)
        //public async Task<string> FileAtFTP(string userName, string password, string host,string file,string filepath,string ftppath)
        //{
        //    try
        //    {
        //        var ftpStream= (dynamic)null; 
        //        string SuccessMsg = string.Empty;
        //        ManualResetEvent m_reset = new ManualResetEvent(false);
        //        using (FtpClient FtpClientConn = new FtpClient())
        //        {

        //            m_reset.Reset();
        //            ServicePointManager.Expect100Continue = true;
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            FtpClientConn.Host = host;
        //            FtpClientConn.Credentials = new NetworkCredential(userName, password);
        //            FtpClientConn.DataConnectionType = FtpDataConnectionType.PASV;


        //            if (FtpClientConn.DirectoryExists(file))
        //            {
        //                var Isfileavial= FtpClientConn.FileExists(filepath);
        //                //
        //                if (Isfileavial)
        //                {

        //                    ftpStream = FtpClientConn.OpenRead(filepath);
        //                    //var FileContentStream = await FtpClientConn.OpenReadAsync(filepath);
        //                    StreamReader StrReader = new StreamReader(ftpStream);
        //                    string FileContent = StrReader.ReadToEnd();
        //                    FtpClientConn.Disconnect();


        //                    return FileContent;
        //                }
        //            }
        //        }
        //    }


        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //    return (dynamic)null;

        //}

        public Stream FileAtFTP(string userName, string password, string host, string file, string filepath)
        {
            try
            {
                //var ftpStream = (dynamic)null;
                Stream ftpStream = new MemoryStream();
                string SuccessMsg = string.Empty;
                ManualResetEvent m_reset = new ManualResetEvent(false);
               
                using (FtpClient FtpClientConn = new FtpClient())
                {
                    m_reset.Reset();
                    FtpClientConn.Host = host;
                    FtpClientConn.Credentials = new NetworkCredential(userName, password);
                    FtpClientConn.DataConnectionType = FtpDataConnectionType.PASV;
                    if (FtpClientConn.DirectoryExists(file))
                                                                   {
                        var Isfileavial = FtpClientConn.FileExists(filepath);
                        //
                         if (Isfileavial)
                        {
                            FtpClientConn.Download(ftpStream, filepath);

                            return ftpStream;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return (dynamic)null;
        }


        //public void Download(string fileName, string ftpServerIP, string ftpUserID, string ftpPassword, string ServerFilePath, string LocalFilePath,string filepATH)
        //{
        //    try
        //    {
        //        FTPConnection ftpAWS = new FTPConnection();
        //        ftpAWS.ServerAddress = ftpServerIP;
        //        ftpAWS.UserName = ftpUserID;
        //        ftpAWS.Password = ftpPassword;
        //        ftpAWS.ServerDirectory = ServerFilePath;
        //        ftpAWS.Connect();

        //        ftpAWS.TransferType = FTPTransferType.BINARY;
        //        ftpAWS.Timeout = 600000;



        //        ftpAWS.DownloadFile(LocalFilePath  , filepATH);
        //        //  ftpAWS.DownloadFile(LocalPath, startpath);

        //        ftpAWS.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //    //return (dynamic)null;
        //}



        private void DownloadFile(string userName, string password, string ftpSourceFilePath, string localDestinationFilePath)
        {
            try
            {
                WebClient client = new WebClient();
                client.Credentials = new NetworkCredential(userName, password);
                client.DownloadFile(ftpSourceFilePath, localDestinationFilePath);
            }
            catch (Exception ex)
            {
                //_log.LogInformation(ex.ToString());
                Exceptions.WriteExceptionLog(ex);
            }
        }

        public void downloadpdf(string userName, string password, string ftpSourceFilePath, string localDestinationFilePath)
        {
       //     FtpWebRequest request =
       //(FtpWebRequest)WebRequest.Create(ftpSourceFilePath);
       //     request.Credentials = new NetworkCredential(userName, password);
       //     request.Method = WebRequestMethods.Ftp.DownloadFile;

       //     using (Stream ftpStream = request.GetResponse().GetResponseStream())
       //     using (Stream fileStream = System.IO.File.Create(localDestinationFilePath))
       //     {
       //         ftpStream.CopyTo(fileStream);
       //     }

            NetworkCredential credentials = new NetworkCredential(userName, password);
            string url = ftpSourceFilePath;
            DownloadFtpDirectory(url, credentials, localDestinationFilePath);
        }

        void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath)
        {
            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials;

            List<string> lines = new List<string>();

            using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }

            foreach (string line in lines)
            {
                string[] tokens =
                    line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                string name = tokens[8];
                string permissions = tokens[0];

                string localFilePath = Path.Combine(localPath, name);
                string fileUrl = url + name;

                if (permissions[0] == 'd')
                {
                    if (!Directory.Exists(localFilePath))
                    {
                        Directory.CreateDirectory(localFilePath);
                    }

                    DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath);
                }
                else
                {
                    FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                    downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                    downloadRequest.Credentials = credentials;

                    using (FtpWebResponse downloadResponse =
                              (FtpWebResponse)downloadRequest.GetResponse())
                    using (Stream sourceStream = downloadResponse.GetResponseStream())
                    using (Stream targetStream = System.IO.File.Create(localFilePath))
                    {
                        byte[] buffer = new byte[10240];
                        int read;
                        while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            targetStream.Write(buffer, 0, read);
                        }
                    }
                }
            }
        }

        public static string Download(string ftpSourceFilePath,
                   string userName, string password, string localDestinationFilePath)
        {
            string ResponseDescription = "";
            string PureFileName = new FileInfo(localDestinationFilePath).Name;
            string DownloadedFilePath = localDestinationFilePath;
            string downloadUrl = ftpSourceFilePath;
            FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(downloadUrl);
            req.Method = WebRequestMethods.Ftp.DownloadFile;
            req.Credentials = new NetworkCredential(userName, password);
            req.UseBinary = true;
            req.Proxy = null;
            try
            {
                FtpWebResponse response = (FtpWebResponse)req.GetResponse();
                Stream stream = response.GetResponseStream();
                byte[] buffer = new byte[2048];
                FileStream fs = new FileStream(DownloadedFilePath, FileMode.Create);
                int ReadCount = stream.Read(buffer, 0, buffer.Length);
                while (ReadCount > 0)
                {
                    fs.Write(buffer, 0, ReadCount);
                    ReadCount = stream.Read(buffer, 0, buffer.Length);
                }
                ResponseDescription = response.StatusDescription;
                fs.Close();
                stream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ResponseDescription;
        }


        private void SavePDFFile(string cReportName, Stream pdfStream)
        {
            using (FileStream file = new FileStream(cReportName, FileMode.Create, FileAccess.Write))
            {
                byte[] bytes = new byte[pdfStream.Length];
                pdfStream.Read(bytes, 0, (int)pdfStream.Length);
                file.Write(bytes, 0, bytes.Length);
                pdfStream.Close();
            }

           

        }

        [Route("DownloadFile/{filename}")]
        [HttpGet()]
        public FileResult DownloadFile(string filename)
        {
            MemoryStream ms = null;
            string fullPath = string.Empty;
            try
            {
                //string webRootPath = _hostingEnvironment.WebRootPath;
                fullPath = Path.Combine(Server.MapPath("~/FTPImages/ArtworkApproval/"), filename);

                WebClient myWebClient = new WebClient();
                byte[] imageBytes = myWebClient.DownloadData(fullPath);
                ms = new MemoryStream(imageBytes);
                //var image = System.Drawing.Image.FromStream(ms);
                //image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return File(ms.ToArray(), "image/jpeg", filename);
            //new File(myDataBuffer, "image/jpeg", "ImageName");
        }



        [Route("LoadImages")]
        [HttpPost]
        public string LoadImages(ImageDetails objImageDetails)
        {
            ImageDetails imgDetails = new ImageDetails();
            ArtworkApprovalDetails obj = new ArtworkApprovalDetails();

            try
            {
                Array.ForEach(Directory.GetFiles(Server.MapPath("~/FTPImages/ArtworkApproval/")), System.IO.File.Delete);
                string ftpFileSourcePath = "/" + objImageDetails.OrderNo.Trim() + "/Artwork/" + objImageDetails.pdfname;

                using (var document = PdfiumViewer.PdfDocument.Load(@"input.pdf"))
                {
                    var image = document.Render(0, 300, 300, true);
                    image.Save(@"output.png", ImageFormat.Png);
                }

                if (!string.IsNullOrEmpty(objImageDetails.ImgPath1))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //var ftpStream = (dynamic)null;
                    string host = "cs.artworkservicesusa.com";
                    string path = "/Adbag_ERP/" + objImageDetails.OrderNo.Trim() + "/Artwork/" + objImageDetails.pdfname;
                    string filepath = string.Empty;
                    string ServerFilePath = path + objImageDetails.pdfname;
                    string FileSourcePath = "ftp://in1.hostedftp.com/Adbag_ERP/" + objImageDetails.OrderNo.Trim() + "/Artwork/";

                    if (host != null && path != null && filepath != null && objImageDetails.ImgPath1 != null)
                    {
                        filepath = "/Adbag_ERP/" + objImageDetails.OrderNo.Trim() + "/Artwork/" + objImageDetails.ImgPath1.Trim();
                        Download(objImageDetails.pdfname, "52.66.83.164", "Adbag", "@DbAg&%7891", FileSourcePath, Server.MapPath("~/FTPImages/ArtworkApproval/" + objImageDetails.pdfname.Trim()), path);
                        //downloadpdf("Adbag", "@DbAg&%7891", ftpFileSourcePath, Server.MapPath("~/FTPImages/ArtworkApproval/" + objImageDetails.pdfname.Trim()));
                        //var ftpStream = FileAtFTP("Adbag", "@DbAg&%7891", host, path, filepath);
                        //SavePDFFile(Server.MapPath("~/FTPImages/ArtworkApproval/" + objImageDetails.pdfname.Trim()), ftpStream);
                        //Image img = System.Drawing.Image.FromStream(ftpStream);
                        //img.Save(Server.MapPath("~/FTPImages/ArtworkApproval/" + objImageDetails.ImgPath1.Trim()), ImageFormat.Jpeg);
                       imgDetails.ImgPath1 = "/FTPImages/ArtworkApproval/" + objImageDetails.ImgPath1.Trim();
                        imgDetails.pdfdownpath = "/FTPImages/ArtworkApproval/" + objImageDetails.pdfname.Trim();
                        ViewBag.pdfpath = "/FTPImages/ArtworkApproval/" + objImageDetails.pdfname.Trim();
                    }
                }
               
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return JsonConvert.SerializeObject(imgDetails);
        }





        [HttpPost]
        [Route("SubmitArtwork")]
        public ActionResult SubmitArtwork(ArtworkApprovalDetails objArtworkApprovalDetails)
        {
            try
            {
                //To get IP Address for Remote User  
                string ipaddress = string.Empty;
                //ipString = HttpContext.Connection.RemoteIpAddress.ToString();
                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                //End
                Boolean lbIsStatusUpdated = false;
                string ArtworkStatus = objArtworkApprovalDetails.ArtworkApprovalStatus;
                string Comments = objArtworkApprovalDetails.Comments;
                string Signature = objArtworkApprovalDetails.Signature;
                string PrimaryKey = objArtworkApprovalDetails.NewPrimaryKey;
                //if (!string.IsNullOrEmpty(Signature))
                //{
                if (!string.IsNullOrEmpty(PrimaryKey))
                {
                    BasicHttpBinding objNavWSBinding = null;
                    //SystemService_PortClient systemService = null;
                    WS_Artwork_Approval_Listing_PortClient artworkService = null;
                    WS_Artwork_Approval_Listing objArtworkListingRef = null;
                    WS_Artwork_Approval_Listing_Filter objArtworkFilter = null;

                    try
                    {
                        _navUrl = ConfigurationManager.AppSettings["NavUrl"].ToString();

                        objNavWSBinding = new BasicHttpBinding();
                        objNavWSBinding.MaxReceivedMessageSize = Int32.MaxValue;
                        objNavWSBinding.MaxBufferSize = Int32.MaxValue;
                        objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                        objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                        string strCustomerPageURL = _navUrl + "/Page/WS_Artwork_Approval_Listing";
                        artworkService = new WS_Artwork_Approval_Listing_PortClient(objNavWSBinding, new EndpointAddress(strCustomerPageURL));
                        //artworkService.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                        artworkService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                        artworkService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                        objArtworkListingRef = new WS_Artwork_Approval_Listing();
                        objArtworkFilter = new WS_Artwork_Approval_Listing_Filter();
                        objArtworkFilter.Field = WS_Artwork_Approval_Listing_Fields.Primary_Key;
                        objArtworkFilter.Criteria = "=" + PrimaryKey;

                        WS_Artwork_Approval_Listing_Filter[] FilterArr = { objArtworkFilter };
                        WS_Artwork_Approval_Listing[] obj2 = artworkService.ReadMultipleAsync(FilterArr, null, 0).Result.ReadMultiple_Result1;

                        if (obj2 != null)
                        {
                            if (obj2.Length > 0)
                            {
                                objArtworkListingRef = obj2[0];

                                if (objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Rejected_Paper_Proof)
                                {
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "AlreadyApprovedOrRejected", "javascript:alert('The artwork has already been approved or rejected.');window.location='ArtworkApproval.aspx?pid=" + Convert.ToString(Request.QueryString["pid"]).Trim() + "'", true);
                                    //return;
                                }
                                objArtworkListingRef.Customer_Approved = Customer_Approved.Option1;
                                //|| ArtworkStatus == "1"
                                if (ArtworkStatus == "0")
                                {
                                    //objArtworkListingRef.Customer_ApprovedSpecified = true;
                                    //if (ArtworkStatus == "1")
                                    //{
                                    //    objArtworkListingRef.Status = ArtworkApproval.Status.Customer_Approved_Paper_Proof_with_Suggestions;
                                    //    objArtworkListingRef.Customer_Comment = Comments.Trim();
                                    //}
                                    //if (ArtworkStatus == "0")
                                    //{
                                    objArtworkListingRef.Status = Status.Customer_Approved_Paper_Proof;
                                    objArtworkListingRef.Customer_Comment = Comments.Trim();
                                    //}
                                    objArtworkListingRef.StatusSpecified = true;
                                    objArtworkListingRef.Proof_Approved_Date = System.DateTime.Today.Date;
                                    objArtworkListingRef.Proof_Approved_DateSpecified = true;
                                }
                                else if (ArtworkStatus == "1")
                                {
                                    objArtworkListingRef.Status = Status.Customer_Rejected_Paper_Proof;
                                    objArtworkListingRef.StatusSpecified = true;
                                    objArtworkListingRef.Customer_Comment = Comments.Trim();
                                    objArtworkListingRef.Proof_Approved_Date = System.DateTime.Today.Date;
                                    objArtworkListingRef.Proof_Approved_DateSpecified = true;
                                }
                                objArtworkListingRef.Approved_by_Signature = Signature.ToString().Trim() == "" ? "" : Signature.ToString().Trim();
                                objArtworkListingRef.IP_Address = ipaddress;
                                //artworkService.UpdateAsync(ref objArtworkListingRef);
                                Adbag_NavMicroservice.ArtworkApproval.Update update = new Adbag_NavMicroservice.ArtworkApproval.Update();
                                update.WS_Artwork_Approval_Listing = objArtworkListingRef;
                                artworkService.UpdateAsync(update);
                                lbIsStatusUpdated = true;
                            }
                        }
                    }
                    catch (Exception foException)
                    {
                        lbIsStatusUpdated = false;
                    }
                    finally
                    {
                        objNavWSBinding = null;
                        artworkService = null;
                        objArtworkListingRef = null;
                        objArtworkFilter = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return Json("success");
        }

        
        [HttpGet()]
        [Route("UploadFile/{orderno}/{filename}")]
        public async Task<ActionResult> UploadFile(string orderno, string filename)
        {
            //string filename = ""; string orderno = "";
            var IsScuccess = string.Empty;
            string FileSourcePath = ConfigurationManager.AppSettings["FTPUrl"].ToString() + orderno + "/Artwork/";
            string path = "/Adbag_ERP/" + orderno.Trim() + "/Artwork/" + filename + ".pdf";

            string filepath = Server.MapPath("~/FTPImages/ArtworkApproval/");
            string LocalPath = Path.Combine(filepath, filename.Trim() + ".pdf");


            IsScuccess = await Download(filename, ConfigurationManager.AppSettings["Ftpdomain"].ToString(), ConfigurationManager.AppSettings["Ftpusername"].ToString(), ConfigurationManager.AppSettings["Ftppassword"].ToString(), FileSourcePath, LocalPath, path);

            // IsScuccess = await Download(filename, "52.66.83.164", "Adbag", "@DbAg&%7891", FileSourcePath, Server.MapPath("~/FTPImages/ArtworkApproval/" + filename.Trim() + ".pdf"), path);
            return Json(IsScuccess, JsonRequestBehavior.AllowGet);
        }

        public async Task<string> Download(string fileName, string ftpServerIP, string ftpUserID, string ftpPassword, string ServerFilePath, string LocalFilePath, string filepATH)
        //public string Download(string fileName, string ftpServerIP, string ftpUserID, string ftpPassword, string ServerFilePath, string LocalFilePath, string filepATH)
        {
            try
            {
                Exception ex = new Exception("Download Method called");
                Exceptions.WriteExceptionLog(ex);
                Exception ex1 = new Exception("ServerFilePath : " + ServerFilePath);
                Exceptions.WriteExceptionLog(ex1);
                Exception ex2 = new Exception("LocalFilePath : " + LocalFilePath);
                Exceptions.WriteExceptionLog(ex2);
                Exception ex3 = new Exception("filepATH : " + filepATH);
                Exceptions.WriteExceptionLog(ex3);

                FTPConnection ftpAWS = new FTPConnection();
                ftpAWS.ServerAddress = ftpServerIP;
                ftpAWS.UserName = ftpUserID;
                ftpAWS.Password = ftpPassword;
                ftpAWS.ServerDirectory = ServerFilePath;
                ftpAWS.Connect();

                ftpAWS.TransferType = FTPTransferType.BINARY;
                ftpAWS.Timeout = 600000;

                ftpAWS.DownloadFile(LocalFilePath, filepATH);
                //  ftpAWS.DownloadFile(LocalPath, startpath);

                ftpAWS.Close();
                Exception ex4 = new Exception("Download Method Ended");
                Exceptions.WriteExceptionLog(ex4);

                return ("Downloaded successfully");
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return ("Downloaded failed");
            }
            //return (dynamic)null;
        }

    }
}
