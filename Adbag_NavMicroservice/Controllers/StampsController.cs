﻿using Adbag_NavMicroservice.USPSLabel;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("Stamps")]
    
        public class StampsController : Controller
    {
        [Route("USPSLabel")]
        [HttpGet()]
        public ActionResult StampsUSPSLabel()
        {
          
            Guid objguid = new Guid("{56eb8235-5aad-4ed5-9f5d-b081fedb04ef}");
            try
            {

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;
                USPSLabel.SwsimV84SoapClient objuspslabel = null;

                string strOrderLinePageUSPSURL = ConfigurationManager.AppSettings["USPSserviceURLstaging"];
                objuspslabel = new USPSLabel.SwsimV84SoapClient(objNavWSBinding, new EndpointAddress(strOrderLinePageUSPSURL));
                objuspslabel.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["USPSUsername"];
                objuspslabel.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["USPSPassword"];

                Adbag_NavMicroservice.USPSLabel.CreateIndiciumRequest objrequest = new USPSLabel.CreateIndiciumRequest();
                Adbag_NavMicroservice.USPSLabel.CreateIndiciumResponse objresponse = new USPSLabel.CreateIndiciumResponse();

                Adbag_NavMicroservice.USPSLabel.Credentials objcrendential = new Adbag_NavMicroservice.USPSLabel.Credentials();
                objcrendential.Username = ConfigurationManager.AppSettings["USPSUsername"];
                objcrendential.Password = ConfigurationManager.AppSettings["USPSPassword"];
                objcrendential.IntegrationID = Guid.Parse(ConfigurationManager.AppSettings["USPSIntegrationID"]);
                objrequest.Item = objcrendential;

                Adbag_NavMicroservice.USPSLabel.Address From = new Adbag_NavMicroservice.USPSLabel.Address();

                From.FullName = "AAB-GLENDALE";
                From.NamePrefix = "AZ";
                From.FirstName = "test";
                From.MiddleName = "test";
                From.LastName = "test";
                From.NamePrefix = "TS";
                From.Title = "MR";
                From.Department = "Dispactch";
                From.Company = "American Adbag Co";
                From.Address1 = "4953 W Missouri Ave";
                From.Address2 = "Glendale";
                From.Address3 = "AZ 85301";
                From.City = "United States";
                From.State = "Arizona";
                From.ZIPCode = "90245";
                From.ZIPCodeAddOn = "";
                From.DPB = "";
                From.CheckDigit = "";
                From.Province = "";
                From.PostalCode = "";
                From.Country = "";
                From.Urbanization = "";
                From.PhoneNumber = "";
                From.Extension = "";
                From.CleanseHash = "";
                From.OverrideHash = "";
                From.EmailAddress = "kalpana.gaonkar@powerweave.com";
                From.FullAddress = "";

                Adbag_NavMicroservice.USPSLabel.Address To = new Adbag_NavMicroservice.USPSLabel.Address();
                To.FullName = "Powerweave LLC";
                To.NamePrefix = "AZ";
                To.FirstName = "test";
                To.MiddleName = "test";
                To.LastName = "test";
                To.NamePrefix = "TS";
                To.Title = "MR";
                To.Department = "Dispactch";
                To.Company = "Powerweave LLC";
                To.Address1 = "Suite 410";
                To.Address2 = "8815 Center Park Drive";
                To.Address3 = "";
                To.City = "Columbia";
                To.State = "US";
                To.ZIPCode = "21045";
                To.ZIPCodeAddOn = "";
                To.DPB = "";
                To.CheckDigit = "";
                To.Province = "";
                To.PostalCode = "";
                To.Country = "";
                To.Urbanization = "";
                To.PhoneNumber = "";
                To.Extension = "";
                To.CleanseHash = "";
                To.OverrideHash = "";
                To.EmailAddress = "";
                To.FullAddress = "";

                string EncodedTrackingNumber = string.Empty;
                string BannerText = string.Empty;
                string TrailingSuperScript = string.Empty;
                string ReturnTrackingNumber = string.Empty;

                Guid StampsTxID = Guid.Empty;
                string Mac = string.Empty;
                string URL = string.Empty;
                string PostageHash = string.Empty;
                byte[][] ImageData = null;
                string FormURL = string.Empty;
                string labelCategory = string.Empty;
                byte[] IndiciumData = null;
                string IntegratorTxID = ConfigurationManager.AppSettings["IntegratorTxID"];
                string TrackingNumber = null;
                Adbag_NavMicroservice.USPSLabel.RateV31 Rate = new RateV31();
                Rate.FromZIPCode = "90245";
                Rate.ToZIPCode = "21045";
                Rate.ToCountry = "US";
                Rate.Amount = 10;
                Rate.MaxAmount = 100;
                Rate.ServiceType = ServiceType.USLM;
                Rate.ServiceDescription = "";
                Rate.PrintLayout = "";
                Rate.DeliverDays = "";
                Rate.WeightLb = 12;
                Rate.WeightOz = 0;
                //Rate.PackageType = PackageTypeV9.Package;
                ////Rate.RequiresAllOf = AddOnTypeV15.SCAINS;
                //Rate.ShipDate = DateTime.Today.AddDays(10);
                //Rate.ContentType = ContentTypeV2.Merchandise;







                string Reference1 = string.Empty;
                string Reference2 = string.Empty;
                string Reference3 = string.Empty;
                string Reference4 = string.Empty;

                Adbag_NavMicroservice.USPSLabel.PostageMode PostageMode = new Adbag_NavMicroservice.USPSLabel.PostageMode();

                Adbag_NavMicroservice.USPSLabel.PostageBalance PostageBalance = null;

                PostageBalance postageBalance = new PostageBalance();

                Adbag_NavMicroservice.USPSLabel.HoldForPickUpFacility HoldForPickUpFacility = null;
                //postageBalance.AvailablePostage = 0;
                //postageBalance.ControlTotal = 0;

                var Response = objuspslabel.CreateIndicium(
                    objrequest.Item,
                    ref IntegratorTxID,
                    ref TrackingNumber,
                    ref Rate,
                    From,
                    To,
                    null,
                    null,
                    false,
                    PostageMode.Normal,
                    ImageType.Jpg,
                    EltronPrinterDPIType.Default,
                    "",
                    0,
                    false,
                    null,
                    0,
                    0,
                    0,
                    0,
                    true,
                    false,
                    false,
                    NonDeliveryOption.Return,
                    null,
                    "",
                    "",
                    true,
                    "1234",
                    PaperSizeV1.LabelSize,
                    null,
                    false,
                    0,
                    ImageDpi.ImageDpi150,
                    "",
                    "",
                    false,
                    0,
                    ref Reference1,
                    ref Reference2,
                    ref Reference3,
                    ref Reference4,
                    true,
                    null,
                    EnclosedServiceType.ParcelSelect,
                    EnclosedPackageType.Flats,
                    out EncodedTrackingNumber,
                    out BannerText,
                    out TrailingSuperScript,
                    out ReturnTrackingNumber,
                    out StampsTxID,
                    out URL,
                    out PostageBalance,
                    out Mac,
                   out PostageHash,
                   out ImageData,
                   out HoldForPickUpFacility,
                   out FormURL,
                   out labelCategory,
                   out IndiciumData);

                if (Response != null)
                {
                    Guid newIntegrationID = StampsTxID;
                }
                return null;


            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);

            }
            return null;
        }
    }
}