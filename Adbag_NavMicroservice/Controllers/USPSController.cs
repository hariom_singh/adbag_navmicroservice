﻿using Adbag_NavMicroservice.FreightParameterList;
using Adbag_NavMicroservice.Models;
using Adbag_NavMicroservice.UPSSalesLineCarton;
using Adbag_NavMicroservice.UPSShippingInfo;
using Adbag_NavMicroservice.USPSLabel;
using Adbag_NAVMicroservice.Models;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("USPS")]
    public class USPSController : Controller
    {
        public USPSController()
        {
        }
        IConfiguration _iconfiguration;
        private readonly ConfigSettings _ConfigSettings;
        readonly ILogger<UPSChargesController> _log;
        private string _navUrl, _document_No, _line_No;

        FreightParameterList.FreightParameter_List_PortClient FreightParameter_List_PortClient = null;
        FreightParameterList.FreightParameter_List_Filter FreightParamListFilter;
        FreightParameterList.FreightParameter_List_Filter FreightParamListFilter1;
        FreightParameterList.FreightParameter_List[] objFreightParameter_List;

        UPSSalesLineCarton.Sales_Line_Carton_PortClient objSalesLineCarton_PortClient = null;
        UPSSalesLineCarton.Sales_Line_Carton_Filter objSalesLineCarton_Filter = null;
        UPSSalesLineCarton.Sales_Line_Carton_Filter objSalesLineCarton_Filter1 = null;
        UPSSalesLineCarton.Sales_Line_Carton[] objSalesLineCarton;

        SalesOrderList.Sales_Order_List_PortClient objSalesOrderList_PortClient = null;
        SalesOrderList.Sales_Order_List_Filter objSalesOrderList_Filter = null;

        SalesOrderList.Sales_Order_List[] objSalesOrderList = null;

        ShippingAgentServices.Shipping_Agent_Services_PortClient objShippingAgentServices_PortClient = null;
        ShippingAgentServices.Shipping_Agent_Services_Filter objShippingAgentServices_Filter = null;
        ShippingAgentServices.Shipping_Agent_Services[] objShippingAgentServices = null;

        UPSWebTransaction.Web_Transaction_PortClient objWeb_Transaction_PortClient = null;

        UPSShippingInfo.Shipping_Info_PortClient objShipping_Info_PortClient;
        UPSShippingInfo.Shipping_Info objShipping_Info;
        UPSShippingInfo.Shipping_Info_Filter objShipping_Info_Filter;
        UPSShippingInfo.Shipping_Info[] objShipping_InfoArray;

        USPSFreightDetails objUSPSFreightDetails = null;
        CustomerDetails objCustomerDetails = null;
        private string _host;
        FreightDetails objFreightDetails = null;
        int ordertype = 0;
        RLCarriersService.RateQuoteServiceSoapClient objRLCarrier = null;
        USPSLabel.SwsimV84SoapClient objuspslabel = null;

        public USPSController(IOptions<ConfigSettings> settings, IConfiguration iconfiguration, ILogger<UPSChargesController> log)
        {
            try
            {
                _iconfiguration = iconfiguration;
                _ConfigSettings = settings.Value;
                _host = settings.Value.HostURL;
                _log = log;
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
        }

        protected void BindSetting()
        {
            try
            {
                objUSPSFreightDetails = new USPSFreightDetails();
                 objUSPSFreightDetails.USERID = Convert.ToString(objShipping_InfoArray[0].USPS_User_Key);        //"810STORM0590";  
                objUSPSFreightDetails.PASSWORD = Convert.ToString(objShipping_InfoArray[0].USPS_User_Password);         //"322WU12IT456";  

                //objUSPSFreightDetails.USERID = "YJdC0T5mdpk1Wiqs";
                //objUSPSFreightDetails.PASSWORD = "QU0y1pWBzidf3Evk";
                objUSPSFreightDetails.Revision = "2";

                objUSPSFreightDetails.PackageID = "0";
                objUSPSFreightDetails.Service = Convert.ToString(objShippingAgentServices[0].Description); //Convert.ToString(objSalesOrderList[0].Shipping_Agent_Service_Code);     //"All";
                objUSPSFreightDetails.FirstClassMailType = "";
                objUSPSFreightDetails.ZipOrigination = Convert.ToString(objShipping_InfoArray[0].From_Zip_Code);        //"33487";
                objUSPSFreightDetails.ZipDestination = Convert.ToString(objFreightParameter_List[0].RecipientZipCode);      //"28054";
                objUSPSFreightDetails.Pounds = Convert.ToString(objSalesLineCarton[0].Total_Weight);        //"0";
                objUSPSFreightDetails.Ounces = "1";
                objUSPSFreightDetails.Container = "";
                objUSPSFreightDetails.Size = Convert.ToString(objSalesLineCarton[0].Size);      //"REGULAR";
                objUSPSFreightDetails.Width = Convert.ToString(objSalesLineCarton[0].Width);
                objUSPSFreightDetails.Length = Convert.ToString(objSalesLineCarton[0].Length);
                objUSPSFreightDetails.Height = Convert.ToString(objSalesLineCarton[0].Height);
                objUSPSFreightDetails.Girth = "";
                objUSPSFreightDetails.Value = "";
                objUSPSFreightDetails.Machinable = true;

                if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                    ViewBag.OrderType = "2";
                else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                    ViewBag.OrderType = "1";
            }
            catch (Exception ex)
            {
                _log.LogInformation(ex.ToString());
                Exceptions.WriteExceptionLog(ex);
            }
        }
        [Route("Get")]
        [HttpGet()]
        public string Get()
        {
            return "Success";
        }

        [Route("GetUSPS/{Document_No}/{Line_No}")]
        [HttpGet()]
        public ActionResult GetUSPS(string Document_No, string Line_No)
        {
            try
            {
                // Document_No = "SALES-15-02282";
                //Line_No = "10000";
                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                //objShipping_Info_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUserName"].ToString(), ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString());
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;
                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameterList.FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));

                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new UPSSalesLineCarton.Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));

                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                UPSSalesLineCarton.Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                #endregion

                #region SalesOrderList
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Order_List";
                objSalesOrderList_PortClient = new SalesOrderList.Sales_Order_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesOrderList_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesOrderList_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                objSalesOrderList_Filter = new SalesOrderList.Sales_Order_List_Filter();
                objSalesOrderList_Filter.Field = SalesOrderList.Sales_Order_List_Fields.No;
                objSalesOrderList_Filter.Criteria = "=" + Document_No;

                //objSalesOrderList_Filter1 = new SalesOrderList.Sales_Order_List_Filter();
                //objSalesOrderList_Filter1.Field = SalesOrderList.Sales_Order_List_Fields.Line_No;
                //objSalesOrderList_Filter1.Criteria = "=" + Line_No;

                SalesOrderList.Sales_Order_List_Filter[] objSales_Order_List_Filter = { objSalesOrderList_Filter };
                objSalesOrderList = objSalesOrderList_PortClient.ReadMultipleAsync(objSales_Order_List_Filter, null, 0).Result.ReadMultiple_Result1;
                #endregion

                #region ShippingAgentServices
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Agent_Services";
                objShippingAgentServices_PortClient = new ShippingAgentServices.Shipping_Agent_Services_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objShippingAgentServices_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShippingAgentServices_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                objShippingAgentServices_Filter = new ShippingAgentServices.Shipping_Agent_Services_Filter();
                objShippingAgentServices_Filter.Field = ShippingAgentServices.Shipping_Agent_Services_Fields.Code;
                objShippingAgentServices_Filter.Criteria = "=" + Convert.ToString(objSalesOrderList[0].Shipping_Agent_Service_Code);

                if (Convert.ToString(objSalesOrderList[0].Shipping_Agent_Service_Code) != null)
                {
                    objShippingAgentServices_Filter.Field = ShippingAgentServices.Shipping_Agent_Services_Fields.Code;
                    objShippingAgentServices_Filter.Criteria = "=" + Convert.ToString(objSalesOrderList[0].Shipping_Agent_Service_Code);
                }
                else
                {
                    objShippingAgentServices_Filter.Field = ShippingAgentServices.Shipping_Agent_Services_Fields.Code;
                    objShippingAgentServices_Filter.Criteria = "=ALL";
                }
                ShippingAgentServices.Shipping_Agent_Services_Filter[] objShipping_Agent_Services_Filter = { objShippingAgentServices_Filter };
                objShippingAgentServices = objShippingAgentServices_PortClient.ReadMultipleAsync(objShipping_Agent_Services_Filter, null, 0).Result.ReadMultiple_Result1;
                #endregion

                BindSetting();

            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);

            }
            return View("GetUSPSCharges", objUSPSFreightDetails);
        }


        [HttpPost]
        [Route("SubmitUSPS")]
        public ActionResult SubmitUSPS(string USPSValue, string USPSText, string DocumentNo, string LineNo, string OrderType)
        {
            try
            {

                string navUrl = ConfigurationManager.AppSettings["NavUrl"];
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = navUrl + "/Codeunit/Web_Transaction";
                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                objWeb_Transaction_PortClient = new UPSWebTransaction.Web_Transaction_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objWeb_Transaction_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objWeb_Transaction_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                string rbtext = USPSText;     //UPSText.Split(' ')[2];
                string rbvalue = USPSValue;
                objWeb_Transaction_PortClient.UpdateUPSChargesAsync(Convert.ToInt32(OrderType), DocumentNo, Convert.ToInt32(LineNo), rbtext, rbvalue);

                return Json("success");
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
                return Json("failure");
            }
        }

        public async Task<string> CalculateUSPS(FreightDetails objFreightDetails, string Filter, string Document_No, string Line_No, int OrderType)
        {
            string Result = string.Empty;
            string resultJSON = string.Empty;
            string token = string.Empty;
            string username = ConfigurationManager.AppSettings["UPSUsername"];
            string password = ConfigurationManager.AppSettings["UPSPassword"];
            try
            {
                using (var client = new HttpClient())
                {

                    var url = "https://freight.ewizpower.com/api/token";
                    Dictionary<string, string> objdata = new Dictionary<string, string>();
                    objdata.Add("username", username);
                    objdata.Add("password", password);

                    using (var content = new FormUrlEncodedContent(objdata))
                    {
                        content.Headers.Clear();
                        content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                        HttpResponseMessage response = await client.PostAsync(url, content);
                        if (response.IsSuccessStatusCode)
                        {
                            resultJSON = response.Content.ReadAsStringAsync().Result;
                            Dictionary<string, string> dResponse = new Dictionary<string, string>();
                            dResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(resultJSON);
                            if (dResponse["access_token"] != null)
                                token = dResponse["access_token"];

                        }
                    }

                }
                using (var client = new HttpClient())
                {
                    if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                        ViewBag.OrderType = "2";

                    else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                        ViewBag.OrderType = "1";

                    var data = objFreightDetails.FreightRequestdata;
                    var url = "https://freight.ewizpower.com/api/FreightRates";
                    string UPSValue = string.Empty;
                    string UPSText = string.Empty;

                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                    HttpContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync(url, content);
                    if (response.IsSuccessStatusCode)
                    {
                        resultJSON = response.Content.ReadAsStringAsync().Result;
                        //Dictionary<string, string> dResponse = new Dictionary<string, string>();
                        var dResponse = JsonConvert.DeserializeObject<List<FreightRate>>(resultJSON);

                        if (dResponse != null && dResponse.Count > 0)
                        {
                            FreightRate freightRate = dResponse.Where(x => x.freightType.ToLower() == Filter.ToLower()).FirstOrDefault();
                            string monatoryVal = freightRate != null ? freightRate.monetaryValue : "";


                            if (monatoryVal != null && Filter != null && Document_No != null && ViewBag.OrderType != null)
                                Result = Submit(monatoryVal, Filter, Document_No, Line_No, ViewBag.OrderType);

                        }

                    }


                    return Result;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return Result;
            }

        }
        [Route("Calculate/{Document_No}/{Line_No}/{Filter}")]
        [HttpGet()]
        public async Task<ActionResult> Calculate(string Document_No, string Line_No, string Filter)
        {
            var IsScuccess = string.Empty;
            try
            {
                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                ViewBag.Filter = Filter;

                _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                //objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                //objShipping_Info_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(ConfigurationManager.AppSettings["WebServiceUserName"].ToString(), ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString());
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;
                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                {
                    ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                }
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                #endregion

                BindSetting();
                //ViewBag.FreightDetails = objFreightDetails;

                IsScuccess = await CalculateUSPS(objFreightDetails, Filter, Document_No, Line_No, ordertype);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            //return View("Index", objFreightDetails);


            return Json(IsScuccess, JsonRequestBehavior.AllowGet);
        }


        public string Submit(string UPSValue, string UPSText, string DocumentNo, string LineNo, string OrderType)
        {
            string isSuccess = string.Empty;
            try
            {

                //To get IP Address for Remote User              
                //string ipString = HttpContext.Connection.RemoteIpAddress.ToString();
                //End
                //string UPSValue = objfreightDetails.UPSValue, UPSText = objfreightDetails.UPSText, DocumentNo = objfreightDetails.DocumentNo, LineNo = objfreightDetails.LineNo, OrderType = objfreightDetails.OrderType;
                string navUrl = ConfigurationManager.AppSettings["NavUrl"];
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = navUrl + "/Codeunit/Web_Transaction";
                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                objWeb_Transaction_PortClient = new UPSWebTransaction.Web_Transaction_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objWeb_Transaction_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objWeb_Transaction_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objWeb_Transaction_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                string rbtext = UPSText;     //UPSText.Split(' ')[2];
                string rbvalue = UPSValue;
                objWeb_Transaction_PortClient.UpdateUPSChargesAsync(Convert.ToInt32(OrderType), DocumentNo, Convert.ToInt32(LineNo), rbtext, rbvalue);

                isSuccess = UPSValue;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return isSuccess;
        }

        [Route("StampsUSPSLabel")]
        [HttpGet()]
        public ActionResult StampsUSPSLabel()
        {
            Execute();
            Guid objguid = new Guid("{56eb8235-5aad-4ed5-9f5d-b081fedb04ef}");
            try
            {

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                string strOrderLinePageUSPSURL = ConfigurationManager.AppSettings["USPSserviceURLstaging"];
                objuspslabel = new USPSLabel.SwsimV84SoapClient(objNavWSBinding, new EndpointAddress(strOrderLinePageUSPSURL));
                objuspslabel.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["USPSUsername"];
                objuspslabel.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["USPSPassword"];

                Adbag_NavMicroservice.USPSLabel.CreateIndiciumRequest objrequest = new USPSLabel.CreateIndiciumRequest();
                Adbag_NavMicroservice.USPSLabel.CreateIndiciumResponse objresponse = new USPSLabel.CreateIndiciumResponse();

                Adbag_NavMicroservice.USPSLabel.Credentials objcrendential = new Adbag_NavMicroservice.USPSLabel.Credentials();
                objcrendential.Username = ConfigurationManager.AppSettings["USPSUsername"];
                objcrendential.Password = ConfigurationManager.AppSettings["USPSPassword"];
                objcrendential.IntegrationID = Guid.Parse(ConfigurationManager.AppSettings["USPSIntegrationID"]);
                objrequest.Item = objcrendential;

                Adbag_NavMicroservice.USPSLabel.Address From = new Adbag_NavMicroservice.USPSLabel.Address();
                
                From.FullName = "AAB-GLENDALE";
                From.NamePrefix = "AZ";
                From.FirstName = "test";
                From.MiddleName = "test";
                From.LastName = "test";
                From.NamePrefix = "TS";
                From.Title = "MR";
                From.Department = "Dispactch";
                From.Company = "American Adbag Co";
                From.Address1 = "4953 W Missouri Ave";
                From.Address2 = "Glendale";
                From.Address3 = "AZ 85301";
                From.City = "United States";
                From.State = "Arizona";
                From.ZIPCode = "90245";
                From.ZIPCodeAddOn = "";
                From.DPB = "";
                From.CheckDigit = "";
                From.Province = "";
                From.PostalCode = "";
                From.Country = "";
                From.Urbanization = "";
                From.PhoneNumber = "";
                From.Extension = "";
                From.CleanseHash = "";
                From.OverrideHash = "";
                From.EmailAddress = "kalpana.gaonkar@powerweave.com";
                From.FullAddress = "";

                Adbag_NavMicroservice.USPSLabel.Address To = new Adbag_NavMicroservice.USPSLabel.Address();
                To.FullName = "Powerweave LLC";
                To.NamePrefix = "AZ";
                To.FirstName = "test";
                To.MiddleName = "test";
                To.LastName = "test";
                To.NamePrefix = "TS";
                To.Title = "MR";
                To.Department = "Dispactch";
                To.Company = "Powerweave LLC";
                To.Address1 = "Suite 410";
                To.Address2 = "8815 Center Park Drive";
                To.Address3 = "";
                To.City = "Columbia";
                To.State = "US";
                To.ZIPCode = "21045";
                To.ZIPCodeAddOn = "";
                To.DPB = "";
                To.CheckDigit = "";
                To.Province = "";
                To.PostalCode = "";
                To.Country = "";
                To.Urbanization = "";
                To.PhoneNumber = "";
                To.Extension = "";
                To.CleanseHash = "";
                To.OverrideHash = "";
                To.EmailAddress = "";
                To.FullAddress = "";

                string EncodedTrackingNumber =string.Empty;
                string BannerText =string.Empty;
                string TrailingSuperScript=string.Empty;
                string ReturnTrackingNumber=string.Empty;

                Guid StampsTxID = Guid.Empty;
                string Mac = string.Empty;
                string URL = string.Empty;
                string PostageHash = string.Empty;
                byte[][] ImageData = null;
                string FormURL = string.Empty;
                string labelCategory = string.Empty;
                byte[] IndiciumData = null;
                string IntegratorTxID = ConfigurationManager.AppSettings["IntegratorTxID"];
                string TrackingNumber = null;
                Adbag_NavMicroservice.USPSLabel.RateV31 Rate = new RateV31();
                Rate.FromZIPCode = "90245";
                Rate.ToZIPCode = "11742";
                Rate.ToCountry = "US";
                Rate.Amount = 10;
                Rate.MaxAmount = 100;
                Rate.ServiceType = ServiceType.USLM;
                Rate.ServiceDescription = "";
                Rate.PrintLayout = "";
                Rate.DeliverDays = "";
                Rate.WeightLb = 12;
                Rate.WeightOz = 0;
                //Rate.PackageType = PackageTypeV9.Package;
                ////Rate.RequiresAllOf = AddOnTypeV15.SCAINS;
                //Rate.ShipDate = DateTime.Today.AddDays(10);
                //Rate.ContentType = ContentTypeV2.Merchandise;







                string Reference1 = string.Empty;
                 string Reference2 = string.Empty;
                 string Reference3 = string.Empty;
                 string Reference4 = string.Empty;

                Adbag_NavMicroservice.USPSLabel.PostageMode PostageMode = new Adbag_NavMicroservice.USPSLabel.PostageMode();

                Adbag_NavMicroservice.USPSLabel.PostageBalance PostageBalance = null;

                PostageBalance postageBalance = new PostageBalance();
                Adbag_NavMicroservice.USPSLabel.HoldForPickUpFacility HoldForPickUpFacility = null;
                //postageBalance.AvailablePostage = 0;
                //postageBalance.ControlTotal = 0;

                var Response = objuspslabel.CreateIndicium(
                    objrequest.Item,                        
                    ref IntegratorTxID,
                    ref TrackingNumber,
                    ref Rate,
                    From,
                    To,
                    null,
                    null,
                    false,
                    PostageMode.Normal,
                    ImageType.Jpg,
                    EltronPrinterDPIType.Default,
                    "",
                    0,
                    false,
                    null,
                    0,
                    0,
                    0,
                    0,
                    true,
                    false,
                    false,
                    NonDeliveryOption.Return,
                    null,
                    "",
                    "",
                    true,
                    "1234",
                    PaperSizeV1.LabelSize,
                    null,
                    false,
                    0,
                    ImageDpi.ImageDpi150,
                    "",
                    "",
                    false,
                    0,
                    ref Reference1,
                    ref Reference2,
                    ref Reference3,
                    ref Reference4,
                    true,
                    null,
                    EnclosedServiceType.ParcelSelect,
                    EnclosedPackageType.Flats,
                    out EncodedTrackingNumber,
                    out BannerText,
                    out TrailingSuperScript,
                    out ReturnTrackingNumber,
                    out StampsTxID,
                    out URL,
                    out PostageBalance,
                    out Mac,
                   out PostageHash,
                   out ImageData,
                   out HoldForPickUpFacility,
                   out FormURL,
                   out labelCategory,
                   out IndiciumData);

                if(Response!=null)
                {
                    Guid newIntegrationID = StampsTxID;
                }
                return null; 

               
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);

            }
            return null;
        }

        public  void Execute()
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sws=""http://stamps.com/xml/namespace/2019/09/swsim/SwsimV84"">
   < soapenv:Header />
<soapenv:Body>
      <sws:CreateIndicium>
         <sws:Credentials>
            <sws:IntegrationID>865ca4a4-26fc-401d-b939-46c5fa52d55f</sws:IntegrationID>
            <sws:Username>PowerWeave-001</sws:Username>
            <sws:Password>December2019!</sws:Password>
         </sws:Credentials>
         <sws:IntegratorTxID>2921eb3d-4878-4904-95d9-9445b6ba8244</sws:IntegratorTxID>        
         <sws:TrackingNumber/>
         <sws:Rate>
            
            <sws:FromZIPCode>90245</sws:FromZIPCode>
           
            <sws:ToZIPCode>11742</sws:ToZIPCode>
            <sws:ServiceType>US-PM</sws:ServiceType>
            <sws:WeightLb>2</sws:WeightLb>
            <sws:PackageType>Package</sws:PackageType>
            <sws:Length>15</sws:Length>
            
            <sws:Width>11</sws:Width>
           
            <sws:Height>11</sws:Height>
            
            <sws:ShipDate>2020-02-21</sws:ShipDate>
         </sws:Rate>
         <sws:From>
           
            <sws:FullName>test test</sws:FullName>
            <sws:Address1>1990 e grand ave</sws:Address1>
            <sws:City>el segundo</sws:City>
            
            <sws:State>ca</sws:State>
           
            <sws:ZIPCode>90245</sws:ZIPCode>
         </sws:From>
         <sws:To>
           
            <sws:FullName>test test</sws:FullName>
            <sws:Address1>REMOVED</sws:Address1>
            <sws:City>REMOVED</sws:City>
           
            <sws:State>NY</sws:State>
          
            <sws:ZIPCode>11742</sws:ZIPCode>
         </sws:To>
      </sws:CreateIndicium>
   </soapenv:Body>
</soap:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    string soapResult = rd.ReadToEnd();
                    Console.WriteLine(soapResult);
                }
            }
        }

        public HttpWebRequest CreateWebRequest()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://swsim.testing.stamps.com/swsim/swsimv84.asmx");
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
    }

    }
