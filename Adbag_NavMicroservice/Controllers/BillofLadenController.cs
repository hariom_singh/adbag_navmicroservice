﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Adbag_NAVMicroservice.Models;
using Adbag_NavMicroservice.Models;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using System.ServiceModel;
using System.Net;
using Adbag_NavMicroservice.FreightParameterList;
using Adbag_NavMicroservice.UPSSalesLineCarton;
using System.Configuration;
using Adbag_NavMicroservice.UPSShippingInfo;
//using Adbag_NavMicroservice.RLCarrier;
using Adbag_NavMicroservice.RLCarriersService;
using Adbag_NavMicroservice.BillofLadenSandbox;
using System.Xml;
using System.IO;
using System.Globalization;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Printing;
using System.Diagnostics;
using IronPdf;
using System.Threading;
using IPrint;
using System.Web.Script.Serialization;
using System.Threading.Tasks;

namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("BOL")]
    public class BillofLadenController : Controller
    {
        // GET: BillofLaden
        public ActionResult Index()
        {
            return View("BillofLaden");
        }


        IConfiguration _iconfiguration;
        private readonly ConfigSettings _ConfigSettings;
        readonly ILogger<FEDEXChargesController> _log;
        private string _navUrl, _document_No, _line_No;

        FreightParameterList.FreightParameter_List_PortClient FreightParameter_List_PortClient = null;
        FreightParameterList.FreightParameter_List_Filter FreightParamListFilter;
        FreightParameterList.FreightParameter_List_Filter FreightParamListFilter1;
        FreightParameterList.FreightParameter_List[] objFreightParameter_List;

        UPSSalesLineCarton.Sales_Line_Carton_PortClient objSalesLineCarton_PortClient = null;
        UPSSalesLineCarton.Sales_Line_Carton_Filter objSalesLineCarton_Filter = null;
        UPSSalesLineCarton.Sales_Line_Carton_Filter objSalesLineCarton_Filter1 = null;
        UPSSalesLineCarton.Sales_Line_Carton[] objSalesLineCarton;

        UPSWebTransaction.Web_Transaction_PortClient objWeb_Transaction_PortClient = null;

        UPSShippingInfo.Shipping_Info_PortClient objShipping_Info_PortClient;
        UPSShippingInfo.Shipping_Info objShipping_Info;
        UPSShippingInfo.Shipping_Info_Filter objShipping_Info_Filter;
        UPSShippingInfo.Shipping_Info[] objShipping_InfoArray;

        UPSDetails objUPSDetails = null;
        FEDEXDetails objFEDEXDetails = null;
        FreightDetails objFreightDetails = null;
        CustomerDetails objCustomerDetails = null;
        List<UPSDetails> lstUPSDetails = null;
        //List<RnlCharges> lstRnlCharges = null;
        //IEnumerable<ServiceLevel> RnlCharges = null;
        BOL objbyteimage = null;

        RnlCharges[] objrnl = null;
        private string _host;
        ArtworkApprovalDetails objArtworkApprovalDetails = null;
        string filepath = string.Empty;
        string shipfilepath = string.Empty;
        BillofLadenSandbox.BillOfLadingServiceSoapClient objBillladen = null;
        YRCBillofladen.YRCSecureBOLClient objyrc = null;

        IEnumerable<BillOfLadingPDFReply> objimg = null;



        [Route("GetBillOfLaden/{Document_No}/{Line_No}")]
        [HttpGet()]
        public ActionResult GetBillOfLaden(string Document_No, string Line_No)
        {
            try
            {
                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;


                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;

                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                {
                    ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                }
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                    ViewBag.OrderType = "2";
                else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                    ViewBag.OrderType = "1";
                #endregion


                string strOrderLinePageURL = ConfigurationManager.AppSettings["RLbookLadenURLsandbox"];


                /////OrderHeader

                objBillladen = new BillofLadenSandbox.BillOfLadingServiceSoapClient(objNavWSBinding, new EndpointAddress(strOrderLinePageURL));
                objBillladen.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["RLUsername"];
                objBillladen.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["RLPassword"];



                BillofLadenSandbox.BillOfLadingRequest request = new BillofLadenSandbox.BillOfLadingRequest();
                BillofLadenSandbox.BillOfLadingReply reply = new BillofLadenSandbox.BillOfLadingReply();

                // BillofLadenSandbox. request = new BillofLadenSandbox.BillOfLadingRequest();
                BillofLadenSandbox.BillOfLadingPDFReply pdfreply = new BillofLadenSandbox.BillOfLadingPDFReply();


                List<BillofLadenSandbox.Item> items = new List<BillofLadenSandbox.Item>();
                // BillOfLadingRequest objBill = new BillOfLadingRequest();

                request.BillOfLading = new BillofLadenSandbox.BillOfLading();
                request.BillOfLading.BillTo = null;
                //  request.BillOfLading.BOLDate = null;

                request.BillOfLading.Shipper = new Party();
                request.BillOfLading.Shipper.Name = objFreightParameter_List[0].Shipper_Name;
                request.BillOfLading.Shipper.AddressLine1 = objFreightParameter_List[0].Shipper_Address_1; //"4953 W Massouri AVE";
                request.BillOfLading.Shipper.AddressLine2 = null;
                request.BillOfLading.Shipper.City = Convert.ToString(objShipping_InfoArray[0].From_City);
                request.BillOfLading.Shipper.EmailAddress = null;
                request.BillOfLading.Shipper.ISO3Country = Convert.ToString(objShipping_InfoArray[0].From_Country);
                request.BillOfLading.Shipper.PhoneExtension = null;
                request.BillOfLading.Shipper.PhoneNumber = objFreightParameter_List[0].Shipper_Phone_No; //"963258741";
                request.BillOfLading.Shipper.State = Convert.ToString(objShipping_InfoArray[0].From_State);
                request.BillOfLading.Shipper.ZipCode = Convert.ToString(objShipping_InfoArray[0].From_Zip_Code); ;


                request.BillOfLading.Consignee = new Consignee();
                request.BillOfLading.Consignee.Name = objFreightParameter_List[0].Recipient_Name; //"test";
                request.BillOfLading.Consignee.AddressLine1 = objFreightParameter_List[0].Recipient_Address_1; // "Test";
                request.BillOfLading.Consignee.AddressLine2 = objFreightParameter_List[0].Recipient_Address_2;//null;
                request.BillOfLading.Consignee.City = Convert.ToString(objFreightParameter_List[0].RecipientCity);
                request.BillOfLading.Consignee.EmailAddress = objFreightParameter_List[0].Recipient_Email;
                request.BillOfLading.Consignee.ISO3Country = Convert.ToString(objFreightParameter_List[0].RecipientCountry);
                request.BillOfLading.Consignee.PhoneExtension = null;
                request.BillOfLading.Consignee.PhoneNumber = Convert.ToString(objFreightParameter_List[0].Recipient_Phone_No);
                request.BillOfLading.Consignee.State = Convert.ToString(objFreightParameter_List[0].RecipientState);
                request.BillOfLading.Consignee.ZipCode = Convert.ToString(objFreightParameter_List[0].RecipientZipCode);


                //request.BillOfLading.BillTo = new Party();
                //request.BillOfLading.BillTo.Name = null;
                //request.BillOfLading.BillTo.AddressLine1 = null;
                //request.BillOfLading.BillTo.AddressLine2 = null;
                //request.BillOfLading.BillTo.City = null;
                //request.BillOfLading.BillTo.EmailAddress = null;
                //request.BillOfLading.BillTo.ISO3Country = null;
                //request.BillOfLading.BillTo.PhoneExtension = null;
                //request.BillOfLading.BillTo.PhoneNumber = null;
                //request.BillOfLading.BillTo.State = null;
                //request.BillOfLading.BillTo.ZipCode = null;

                //request.BillOfLading.Broker = new Party();
                //request.BillOfLading.Broker.Name = null;
                //request.BillOfLading.Broker.AddressLine1 = null;
                //request.BillOfLading.Broker.AddressLine2 = null;
                //request.BillOfLading.Broker.City = null;
                //request.BillOfLading.Broker.EmailAddress = null;
                //request.BillOfLading.Broker.ISO3Country = null;
                //request.BillOfLading.Broker.PhoneExtension = null;
                //request.BillOfLading.Broker.PhoneNumber = null;
                //request.BillOfLading.Broker.State = null;
                //request.BillOfLading.Broker.ZipCode = null;


                //request.BillOfLading.RemitCOD.CODAmount = null;
                //request.BillOfLading.RemitCOD.CheckType = CODCheckType.Company;
                //request.BillOfLading.RemitCOD.FeeType = CODFeeType.Prepaid;
                //request.BillOfLading.AdditionalServices[0] = BOLAccessorial.DeliveryNotification;
                //request.BillOfLading.ServiceLevel= BillofLadenSandbox.ServiceLevel.ExpeditedService;
                //request.BillOfLading.HourlyWindow = new BillofLadenSandbox.HourlyWindow();
                //request.BillOfLading.HourlyWindow.Start = null;
                //request.BillOfLading.HourlyWindow.End = null;

                //request.BillOfLading.ExpeditedQuoteNumber = null;



                //request.BillOfLading.DeclaredValue = null;



                //request.BillOfLading.FreightChargePaymentMethod = BOLPaymentMethod.Prepaid;
                //request.BillOfLading.HazmatInformation = null;



                // request.BillOfLading.ReferenceNumbers = null;

                //request.BillOfLading.RemitCOD = null;
                //request.BillOfLading.ServiceLevel = BillofLadenSandbox.ServiceLevel.ExpeditedService;
                //request.BillOfLading.Shipper = null;
                //request.BillOfLading.SpecialInstructions = null;



                request.BillOfLading.BOLDate = DateTime.Now.ToString("MM/dd/yyyy");

                request.CustomerData = null;
                request.PickupRequestInfo = null;
                request.AddPickupRequest = false;

                items.Add(new BillofLadenSandbox.Item()
                {
                    Class = "50.0",
                    Weight = Convert.ToString(objFreightParameter_List[0].PackageWeight),
                    Pieces = "1",
                    PackageType = PackageType.BAG,
                    Description = "test"



                });

                request.BillOfLading.Items = items.ToArray();

                reply = objBillladen.CreateBillOfLading(ConfigurationManager.AppSettings["APIKey"], request);
                string[] message = reply.Messages;

                if (reply.WasSuccess == true)
                {
                    int boid = reply.BolID;
                    string wb = reply.WebProNumber;

                    int billOfLadingId = boid; string style = "STYLE_1"; int startPosition = 1; int numLabels = 1;

                    pdfreply = objBillladen.GetBillOfLadingLabelPDF(ConfigurationManager.AppSettings["APIKey"], billOfLadingId, style, startPosition, numLabels);

                    byte[] bytes = pdfreply.PdfDocument;
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

                    byte[] pdf = Convert.FromBase64String(base64String);
                    filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + boid + ".pdf"));
                    System.IO.File.WriteAllBytes(filepath, pdf);
                    GetReport(filepath);
                    getPdf(filepath);
                    objbyteimage = new BOL();
                    objbyteimage.BOLimag = bytes;
                    empdf(bytes);
                    objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + boid + ".pdf"));
                    printpdf(objbyteimage.filepath);


                    return View("BillofLaden", objbyteimage);

                }
                else
                {

                }



                return View("BillofLaden", null);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);

            }
            return View();
        }


        [Route("GetBOl/{Document_No}/{Line_No}")]
        [HttpGet()]
        public async Task<ActionResult> GetBOl(string Document_No, string Line_No, string Filter)
        {
            try
            {
                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                _navUrl = ConfigurationManager.AppSettings["NavUrl"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;


                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;

                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                {
                    ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                }
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                    ViewBag.OrderType = "2";
                else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                    ViewBag.OrderType = "1";
                #endregion


                string strOrderLinePageURL = ConfigurationManager.AppSettings["RLbookLadenURLsandbox"];


                /////OrderHeader

                objBillladen = new BillofLadenSandbox.BillOfLadingServiceSoapClient(objNavWSBinding, new EndpointAddress(strOrderLinePageURL));
                objBillladen.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["RLUsername"];
                objBillladen.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["RLPassword"];



                BillofLadenSandbox.BillOfLadingRequest request = new BillofLadenSandbox.BillOfLadingRequest();
                BillofLadenSandbox.BillOfLadingReply reply = new BillofLadenSandbox.BillOfLadingReply();

                // BillofLadenSandbox. request = new BillofLadenSandbox.BillOfLadingRequest();
                BillofLadenSandbox.BillOfLadingPDFReply pdfreply = new BillofLadenSandbox.BillOfLadingPDFReply();


                List<BillofLadenSandbox.Item> items = new List<BillofLadenSandbox.Item>();
                // BillOfLadingRequest objBill = new BillOfLadingRequest();

                request.BillOfLading = new BillofLadenSandbox.BillOfLading();
                request.BillOfLading.BillTo = null;
                //  request.BillOfLading.BOLDate = null;

                request.BillOfLading.Shipper = new Party();
                request.BillOfLading.Shipper.Name = objFreightParameter_List[0].Shipper_Name;
                request.BillOfLading.Shipper.AddressLine1 = objFreightParameter_List[0].Shipper_Address_1; //"4953 W Massouri AVE";
                request.BillOfLading.Shipper.AddressLine2 = null;
                request.BillOfLading.Shipper.City = Convert.ToString(objShipping_InfoArray[0].From_City);
                request.BillOfLading.Shipper.EmailAddress = null;
                request.BillOfLading.Shipper.ISO3Country = Convert.ToString(objShipping_InfoArray[0].From_Country);
                request.BillOfLading.Shipper.PhoneExtension = null;
                request.BillOfLading.Shipper.PhoneNumber = objFreightParameter_List[0].Shipper_Phone_No; //"963258741";
                request.BillOfLading.Shipper.State = Convert.ToString(objShipping_InfoArray[0].From_State);
                request.BillOfLading.Shipper.ZipCode = Convert.ToString(objShipping_InfoArray[0].From_Zip_Code); ;


                request.BillOfLading.Consignee = new Consignee();
                request.BillOfLading.Consignee.Name = objFreightParameter_List[0].Recipient_Name; //"test";
                request.BillOfLading.Consignee.AddressLine1 = objFreightParameter_List[0].Recipient_Address_1; // "Test";
                request.BillOfLading.Consignee.AddressLine2 = objFreightParameter_List[0].Recipient_Address_2;//null;
                request.BillOfLading.Consignee.City = Convert.ToString(objFreightParameter_List[0].RecipientCity);
                request.BillOfLading.Consignee.EmailAddress = objFreightParameter_List[0].Recipient_Email;
                request.BillOfLading.Consignee.ISO3Country = Convert.ToString(objFreightParameter_List[0].RecipientCountry);
                request.BillOfLading.Consignee.PhoneExtension = null;
                request.BillOfLading.Consignee.PhoneNumber = Convert.ToString(objFreightParameter_List[0].Recipient_Phone_No);
                request.BillOfLading.Consignee.State = Convert.ToString(objFreightParameter_List[0].RecipientState);
                request.BillOfLading.Consignee.ZipCode = Convert.ToString(objFreightParameter_List[0].RecipientZipCode);


                //request.BillOfLading.BillTo = new Party();
                //request.BillOfLading.BillTo.Name = null;
                //request.BillOfLading.BillTo.AddressLine1 = null;
                //request.BillOfLading.BillTo.AddressLine2 = null;
                //request.BillOfLading.BillTo.City = null;
                //request.BillOfLading.BillTo.EmailAddress = null;
                //request.BillOfLading.BillTo.ISO3Country = null;
                //request.BillOfLading.BillTo.PhoneExtension = null;
                //request.BillOfLading.BillTo.PhoneNumber = null;
                //request.BillOfLading.BillTo.State = null;
                //request.BillOfLading.BillTo.ZipCode = null;

                //request.BillOfLading.Broker = new Party();
                //request.BillOfLading.Broker.Name = null;
                //request.BillOfLading.Broker.AddressLine1 = null;
                //request.BillOfLading.Broker.AddressLine2 = null;
                //request.BillOfLading.Broker.City = null;
                //request.BillOfLading.Broker.EmailAddress = null;
                //request.BillOfLading.Broker.ISO3Country = null;
                //request.BillOfLading.Broker.PhoneExtension = null;
                //request.BillOfLading.Broker.PhoneNumber = null;
                //request.BillOfLading.Broker.State = null;
                //request.BillOfLading.Broker.ZipCode = null;


                //request.BillOfLading.RemitCOD.CODAmount = null;
                //request.BillOfLading.RemitCOD.CheckType = CODCheckType.Company;
                //request.BillOfLading.RemitCOD.FeeType = CODFeeType.Prepaid;
                //request.BillOfLading.AdditionalServices[0] = BOLAccessorial.DeliveryNotification;
                //request.BillOfLading.ServiceLevel= BillofLadenSandbox.ServiceLevel.ExpeditedService;
                //request.BillOfLading.HourlyWindow = new BillofLadenSandbox.HourlyWindow();
                //request.BillOfLading.HourlyWindow.Start = null;
                //request.BillOfLading.HourlyWindow.End = null;

                //request.BillOfLading.ExpeditedQuoteNumber = null;



                //request.BillOfLading.DeclaredValue = null;



                //request.BillOfLading.FreightChargePaymentMethod = BOLPaymentMethod.Prepaid;
                //request.BillOfLading.HazmatInformation = null;



                // request.BillOfLading.ReferenceNumbers = null;

                //request.BillOfLading.RemitCOD = null;
                //request.BillOfLading.ServiceLevel = BillofLadenSandbox.ServiceLevel.ExpeditedService;
                //request.BillOfLading.Shipper = null;
                //request.BillOfLading.SpecialInstructions = null;



                request.BillOfLading.BOLDate = DateTime.Now.ToString("MM/dd/yyyy");

                request.CustomerData = null;
                request.PickupRequestInfo = null;
                request.AddPickupRequest = false;

                items.Add(new BillofLadenSandbox.Item()
                {
                    Class = "60.0",
                    Weight = Convert.ToString(objFreightParameter_List[0].PackageWeight),
                    Pieces = "1",
                    PackageType = PackageType.BAG,
                    Description = "test"



                });

                request.BillOfLading.Items = items.ToArray();

                reply = objBillladen.CreateBillOfLading(ConfigurationManager.AppSettings["APIKey"], request);
                string[] message = reply.Messages;

                if (reply.WasSuccess == true)
                {
                    int boid = reply.BolID;
                    string wb = reply.WebProNumber;
                    //coment
                    //int billOfLadingId = boid; string style = "STYLE_1"; int startPosition = 1; int numLabels = 1;

                    //pdfreply = objBillladen.GetBillOfLadingLabelPDF(ConfigurationManager.AppSettings["APIKey"], billOfLadingId, style, startPosition, numLabels);

                    //byte[] bytes = pdfreply.PdfDocument;
                    //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

                    //byte[] pdf = Convert.FromBase64String(base64String);
                    //filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + boid + ".pdf"));
                    //System.IO.File.WriteAllBytes(filepath, pdf);
                    //GetReport(filepath);
                    //getPdf(filepath);
                    //objbyteimage = new BOL();
                    //objbyteimage.BOLimag = bytes;
                    //empdf(bytes);
                    //objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + boid + ".pdf"));
                    //printpdf(objbyteimage.filepath);


                    //return View("BillofLaden", objbyteimage);
                    //coment

                    Bolreply obj = new Bolreply();
                    obj.BolID = boid.ToString();
                    obj.WebProNumber = wb;

                    var json = new JavaScriptSerializer().Serialize(obj);

                    return Json(json, JsonRequestBehavior.AllowGet);

                }
                else
                {

                }



                return View("BillofLaden", null);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);

            }
            return View();
        }
        public FileResult GetReport(string filepath)
        {
            string ReportURL = filepath;
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/pdf");
        }

        public void getPdf(string filepath)
        {
            string FilePath = Server.MapPath("javascript1-sample.pdf");
            WebClient User = new WebClient();
            Byte[] FileBuffer = User.DownloadData(filepath);
            if (FileBuffer != null)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", FileBuffer.Length.ToString());
                Response.BinaryWrite(FileBuffer);
            }
        }

        public ActionResult empdf(byte[] filedata)
        {

            byte[] reportData = filedata;
            return new FileContentResult(reportData, "application/pdf");


        }

        public void printpdf(string filepath)
        {
            string pathPdf = filepath;
            ProcessStartInfo infoPrintPdf = new ProcessStartInfo();
            infoPrintPdf.FileName = pathPdf;
            // The printer name is hardcoded here, but normally I get this from a combobox with all printers
            string printerName = "7th floor printer";
            string driverName = "Print queues";
            string portName = "10.0.0.161";
            infoPrintPdf.FileName = @"C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe";
            infoPrintPdf.Arguments = string.Format("/t {0} \"{1}\" \"{2}\" \"{3}\"",
                pathPdf, printerName, driverName, portName);
            infoPrintPdf.CreateNoWindow = true;
            infoPrintPdf.UseShellExecute = false;
            infoPrintPdf.WindowStyle = ProcessWindowStyle.Hidden;
            Process printPdf = new Process();
            printPdf.StartInfo = infoPrintPdf;
            printPdf.Start();

            // This time depends on the printer, but has to be long enough to
            // let the printer start printing
            System.Threading.Thread.Sleep(10000);

            if (!printPdf.CloseMainWindow())              // CloseMainWindow never seems to succeed
                printPdf.Kill(); printPdf.WaitForExit();  // Kill AcroRd32.exe

            printPdf.Close();  // Close the process and release resources
        }

    //    [Route("GetBOLYRC")]
    //    [HttpGet()]
    //    public ActionResult GetBolYRC()
    //    {

    //        try
    //        {
    //            string strOrderLinePageURL = ConfigurationManager.AppSettings["BOLstaging"];


    //            BasicHttpBinding objNavWSBinding = null;
    //            objNavWSBinding = new BasicHttpBinding();
    //            objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
    //            objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
    //            objNavWSBinding.MaxReceivedMessageSize = 2147483647;



    //            objyrc = new YRCBillofladen.YRCSecureBOLClient(objNavWSBinding, new EndpointAddress(strOrderLinePageURL));
    //            objyrc.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["yrcuser"];
    //            objyrc.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["yrcpass"];



    //            Adbag_NavMicroservice.YRCBillofladen.submitBOLResponse response = new YRCBillofladen.submitBOLResponse();
    //            Adbag_NavMicroservice.YRCBillofladen.submitBOLRequest request = new YRCBillofladen.submitBOLRequest();

    //            Adbag_NavMicroservice.YRCBillofladen.BoLDetail boLDetail = new YRCBillofladen.BoLDetail();

    //            boLDetail.pickupDate = DateTime.Today.AddDays(5).ToString();
    //            boLDetail.role = Adbag_NavMicroservice.YRCBillofladen.Roles.SH;
    //            boLDetail.autoSchedulePickup = false;
    //            boLDetail.autoEmailBOL = false;
    //            boLDetail.paymentTerms = Adbag_NavMicroservice.YRCBillofladen.Terms.P;
    //            boLDetail.originAddressSameAsShipper = true;

    //            request.bolDetail = boLDetail;

    //            Adbag_NavMicroservice.YRCBillofladen.Customer Shipper = new YRCBillofladen.Customer();//From
    //            Shipper.companyName = "American AD Bag";
    //            Shipper.address = "4953 W Missouri Ave";
    //            Shipper.city = "Glendale";
    //            Shipper.state = "AZ";
    //            Shipper.zip = "85301";
    //            Shipper.country = Adbag_NavMicroservice.YRCBillofladen.countryCodeList.USA;

    //            request.shipper = Shipper;


    //            Adbag_NavMicroservice.YRCBillofladen.Customer consignee = new YRCBillofladen.Customer();


    //            consignee.companyName = "Powerweave llc";
    //            consignee.address = "Suite 410, 8815 Center Park Drive";
    //            consignee.city = "Columbia";
    //            consignee.state = "MD";
    //            consignee.zip = "21045";
    //            consignee.country = Adbag_NavMicroservice.YRCBillofladen.countryCodeList.USA;



    //            request.consignee = consignee;

    //            Adbag_NavMicroservice.YRCBillofladen.CommodityInformation commodityInformation = new YRCBillofladen.CommodityInformation();
    //            commodityInformation.weightTypeIdentifier = Adbag_NavMicroservice.YRCBillofladen.WeightTypes.LB;
    //            //commodityInformation.palletWeight = "";
    //            //commodityInformation.shipmentCube = "";

    //            request.commodityInformation = commodityInformation;

    //            // Adbag_NavMicroservice.YRCBillofladen.CommodityItem commodityItem = new YRCBillofladen.CommodityItem();

    //            List<Adbag_NavMicroservice.YRCBillofladen.CommodityItem> commodityItem = new List<Adbag_NavMicroservice.YRCBillofladen.CommodityItem>();
    //            Adbag_NavMicroservice.YRCBillofladen.DeliveryServiceOptions objDeliveryOptions = new Adbag_NavMicroservice.YRCBillofladen.DeliveryServiceOptions();
    //            Adbag_NavMicroservice.YRCBillofladen.PickupRequest objPickupRequest = new Adbag_NavMicroservice.YRCBillofladen.PickupRequest();
    //            Adbag_NavMicroservice.YRCBillofladen.BolLabelPDF objBolLabelPDF = new Adbag_NavMicroservice.YRCBillofladen.BolLabelPDF();


    //            commodityItem.Add(new Adbag_NavMicroservice.YRCBillofladen.CommodityItem()
    //            {

    //                totalWeight = "12",
    //                handlingUnitQuantity = "1",
    //                handlingUnitType = Adbag_NavMicroservice.YRCBillofladen.HandlingUnitTypes.BAG,
    //                //packageQuantity="1",
    //                //packageUnitType= Adbag_NavMicroservice.YRCBillofladen.HandlingUnitTypes.BOX,
    //                productDesc = "Books",
    //                //nmfc= "486001",
    //                //freightClass = Adbag_NavMicroservice.YRCBillofladen.FreightClassTypes.Item60,
    //                //isHazardous = false,
    //                handlingUnitTypeSpecified = true,
    //                //packageUnitTypeSpecified=false,
    //                // length="11",
    //                // width="12",
    //                // height="20",
    //                //density="12",
    //                //cube="1",
    //                //freightClassSpecified=true,

    //            });
    //            //YRCBillofladen
    //            request.commodityItem = commodityItem.ToArray();



    //            objDeliveryOptions.deliveryServiceOption = Adbag_NavMicroservice.YRCBillofladen.DeliveryServiceTypes.ACEL;

    //            request.deliveryServiceOptions = objDeliveryOptions;

    //            //objPickupRequest.pickupLocationContactName = "Ginny Semrow";
    //            //objPickupRequest.pickupLocationPhone = "+1 623-931-1386";
    //            //objPickupRequest.shipmentReadyTime = "10:00";
    //            //objPickupRequest.dockCloseTime = "16:00";
    //            //objPickupRequest.isCertifiedPickup = true;
    //            //objPickupRequest.certifiedPickupEmailAddress = "kalpana.gaonkar@powerweave.com";
    //            //objPickupRequest.sendConfirmationEmail = true;
    //            //objPickupRequest.confirmationEmailAddress = "kalpana.gaonkar@powerweave.com";

    //            //request.pickupRequest = objPickupRequest;

    //            objBolLabelPDF.generateBolPDF = true;
    //            objBolLabelPDF.bolDocumentType = Adbag_NavMicroservice.YRCBillofladen.BolDocumentType.STD;
    //            objBolLabelPDF.generateShippingLabelsPDF = true;
    //            objBolLabelPDF.numberOfLabelsPerShipment = "1";
    //            objBolLabelPDF.labelStartingPosition = "1";
    //            objBolLabelPDF.labelFormat = Adbag_NavMicroservice.YRCBillofladen.LabelFormat.AVERY_5164;
    //            objBolLabelPDF.generateProLabelsPDF = true;
    //            objBolLabelPDF.proLabelBorders = false;
    //            objBolLabelPDF.proLabelsPerPage = "5";

    //            request.bolLabelPDF = objBolLabelPDF;

    //            Adbag_NavMicroservice.YRCBillofladen.Credentials UsernameToken = new YRCBillofladen.Credentials();
    //            UsernameToken.Username = ConfigurationManager.AppSettings["yrcuser"];
    //            UsernameToken.Password = ConfigurationManager.AppSettings["yrcpass"];

    //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
    //            var response1 = objyrc.submitBOL(UsernameToken, request);
    //            if (response1.statusCode == "0")
    //            {
    //                Bolreply obj = new Bolreply();

    //                //Bill of laden

    //                //if(response1.encodedBolPdf!=null)
    //                //{
    //                //    string Pronumber = response1.proNumber;

    //                //    obj.bollabel = response1.encodedBolPdf;
    //                //    string base64String = Convert.ToBase64String(obj.bollabel, 0, obj.bollabel.Length);

    //                //    byte[] pdf = Convert.FromBase64String(base64String);
    //                //    filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + Pronumber + ".pdf"));
    //                //    System.IO.File.WriteAllBytes(filepath, pdf);
    //                //    GetReport(filepath);
    //                //    getPdf(filepath);
    //                //    objbyteimage = new BOL();
    //                //    objbyteimage.BOLimag = obj.bollabel;
    //                //    empdf(obj.bollabel);
    //                //    objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + Pronumber + ".pdf"));



    //                //    return View("BillofLaden", objbyteimage);
    //                //}

    //                //shipping label

    //                //if (response1.encodedShippingLabelsPdf != null)
    //                //{
    //                //    string Pronumber = response1.proNumber;

    //                //    obj.bollabel = response1.encodedShippingLabelsPdf;
    //                //    string base64String = Convert.ToBase64String(obj.bollabel, 0, obj.bollabel.Length);

    //                //    byte[] pdf = Convert.FromBase64String(base64String);
    //                //    filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/SH_"+ Pronumber + ".pdf"));
    //                //    System.IO.File.WriteAllBytes(filepath, pdf);
    //                //    GetReport(filepath);
    //                //    getPdf(filepath);
    //                //    objbyteimage = new BOL();
    //                //    objbyteimage.BOLimag = obj.bollabel;
    //                //    empdf(obj.bollabel);
    //                //    objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/SH_" + Pronumber + ".pdf"));



    //                //    return View("BillofLaden", objbyteimage);
    //                //}

    //                //Prolabelpdf

    //                if (response1.encodedProLabelsPdf != null)
    //                {
    //                    string Pronumber = response1.proNumber;

    //                    obj.bollabel = response1.encodedProLabelsPdf;
    //                    string base64String = Convert.ToBase64String(obj.bollabel, 0, obj.bollabel.Length);

    //                    byte[] pdf = Convert.FromBase64String(base64String);
    //                    filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/PRO_" + Pronumber + ".pdf"));
    //                    System.IO.File.WriteAllBytes(filepath, pdf);
    //                    GetReport(filepath);
    //                    getPdf(filepath);
    //                    objbyteimage = new BOL();
    //                    objbyteimage.BOLimag = obj.bollabel;
    //                    empdf(obj.bollabel);
    //                    objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/PRO_" + Pronumber + ".pdf"));



    //                    return View("BillofLaden", objbyteimage);
    //                }

    //                //printpdf(objbyteimage.filepath);
    //            }
    //            return null;
    //        }

    //        catch (Exception ex)
    //        {

    //            Exceptions.WriteExceptionLog(ex);

    //        }
    //        return null;
    //    }


    //}



    [Route("GetBOLYRC/{Document_No}/{Line_No}")]
        [HttpGet()]
        
        public ActionResult GetBolYRC(string Document_No, string Line_No)
        {

        try
        {

                string strOrderLinePageURL = ConfigurationManager.AppSettings["BOLstaging"];


                BasicHttpBinding objservice = null;
                objservice = new BasicHttpBinding();
                objservice.Security.Mode = BasicHttpSecurityMode.Transport;
                objservice.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                objservice.MaxReceivedMessageSize = 2147483647;


                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                ViewBag.URL = _host;
                ViewBag.DocumentNo = Document_No;
                ViewBag.LineNo = Line_No;
                _navUrl = ConfigurationManager.AppSettings["NavUrl"];




                #region Shipping_Info
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Shipping_Info";
                objShipping_Info_PortClient = new Shipping_Info_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objShipping_Info_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objShipping_Info_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objShipping_InfoArray = objShipping_Info_PortClient.ReadMultipleAsync(null, null, 0).Result.ReadMultiple_Result1;

                //objShipping_Info = (Shipping_Info)objShipping_InfoArray.GetValue(0);
                #endregion

                #region FreightParameter_List
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/FreightParameter_List";

                FreightParameter_List_PortClient = new FreightParameter_List_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                FreightParameter_List_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                FreightParameter_List_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //FreightParameter_List_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                FreightParamListFilter = new FreightParameter_List_Filter();
                FreightParamListFilter.Field = FreightParameter_List_Fields.Document_No;
                FreightParamListFilter.Criteria = "=" + Document_No;

                FreightParamListFilter1 = new FreightParameter_List_Filter();
                FreightParamListFilter1.Field = FreightParameter_List_Fields.Line_No;
                FreightParamListFilter1.Criteria = "=" + Line_No;

                FreightParameter_List_Filter[] objFilter = { FreightParamListFilter, FreightParamListFilter1 };
                objFreightParameter_List = FreightParameter_List_PortClient.ReadMultipleAsync(objFilter, null, 0).Result.ReadMultiple_Result1;

                if (objFreightParameter_List != null && objFreightParameter_List.Count() > 0)
                {
                    ViewBag.ResShipment_Method = objFreightParameter_List[0].Shipment_Method;
                }
                #endregion

                #region SalesLineCarton
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/Sales_Line_Carton";
                objSalesLineCarton_PortClient = new Sales_Line_Carton_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objSalesLineCarton_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objSalesLineCarton_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();
                //objSalesLineCarton_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);

                objSalesLineCarton_Filter = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter.Field = Sales_Line_Carton_Fields.Document_No;
                objSalesLineCarton_Filter.Criteria = "=" + Document_No;

                objSalesLineCarton_Filter1 = new Sales_Line_Carton_Filter();
                objSalesLineCarton_Filter1.Field = Sales_Line_Carton_Fields.Line_No;
                objSalesLineCarton_Filter1.Criteria = "=" + Line_No;

                Sales_Line_Carton_Filter[] objSales_Line_Carton_Filter = { objSalesLineCarton_Filter, objSalesLineCarton_Filter1 };
                objSalesLineCarton = objSalesLineCarton_PortClient.ReadMultipleAsync(objSales_Line_Carton_Filter, null, 0).Result.ReadMultiple_Result1;

                if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "order")
                    ViewBag.OrderType = "2";
                else if (objSalesLineCarton[0].Document_Type.ToString().ToLower() == "quote")
                    ViewBag.OrderType = "1";
                #endregion


            objyrc = new YRCBillofladen.YRCSecureBOLClient(objservice, new EndpointAddress(strOrderLinePageURL));
            objyrc.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["yrcuser"];
            objyrc.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["yrcpass"];



            Adbag_NavMicroservice.YRCBillofladen.submitBOLResponse response = new YRCBillofladen.submitBOLResponse();
            Adbag_NavMicroservice.YRCBillofladen.submitBOLRequest request = new YRCBillofladen.submitBOLRequest();

            Adbag_NavMicroservice.YRCBillofladen.BoLDetail boLDetail = new YRCBillofladen.BoLDetail();

            boLDetail.pickupDate = DateTime.Today.AddDays(5).ToString();
            boLDetail.role = Adbag_NavMicroservice.YRCBillofladen.Roles.SH;
            boLDetail.autoSchedulePickup = false;
            boLDetail.autoEmailBOL = false;
            boLDetail.paymentTerms = Adbag_NavMicroservice.YRCBillofladen.Terms.P;
            boLDetail.originAddressSameAsShipper = true;

            request.bolDetail = boLDetail;

                Adbag_NavMicroservice.YRCBillofladen.Customer Shipper = new YRCBillofladen.Customer();//From
                //Shipper.companyName = "American AD Bag";
                //Shipper.address = "4953 W Missouri Ave";
                //Shipper.city = "Glendale";
                //Shipper.state = "AZ";
                //Shipper.zip = "85301";
                //Shipper.country = Adbag_NavMicroservice.YRCBillofladen.countryCodeList.USA;

                Shipper.companyName = Convert.ToString(objFreightParameter_List[0].Shipper_Name);
                Shipper.address = Convert.ToString(objFreightParameter_List[0].Shipper_Address_1);
                Shipper.city = Convert.ToString(objFreightParameter_List[0].ShipperCity);
                Shipper.state = Convert.ToString(objFreightParameter_List[0].ShipperState);
                Shipper.zip = Convert.ToString(objFreightParameter_List[0].ShipperZipCode);
                Shipper.country = Adbag_NavMicroservice.YRCBillofladen.countryCodeList.USA;

           

                request.shipper = Shipper;


            Adbag_NavMicroservice.YRCBillofladen.Customer consignee = new YRCBillofladen.Customer();


                //consignee.companyName = "Powerweave llc";
                //consignee.address = "Suite 410, 8815 Center Park Drive";
                //consignee.city = "Columbia";
                //consignee.state = "MD";
                //consignee.zip = "21045";
                //consignee.country = Adbag_NavMicroservice.YRCBillofladen.countryCodeList.USA;
                consignee.companyName = Convert.ToString(objFreightParameter_List[0].Recipient_Name);
                consignee.address = Convert.ToString(objFreightParameter_List[0].Recipient_Address_1);
                consignee.city = Convert.ToString(objFreightParameter_List[0].RecipientCity);
                consignee.state = Convert.ToString(objFreightParameter_List[0].RecipientState);
                consignee.zip = Convert.ToString(objFreightParameter_List[0].RecipientZipCode);
                consignee.country = Adbag_NavMicroservice.YRCBillofladen.countryCodeList.USA;



                request.consignee = consignee;

            Adbag_NavMicroservice.YRCBillofladen.CommodityInformation commodityInformation = new YRCBillofladen.CommodityInformation();
            commodityInformation.weightTypeIdentifier = Adbag_NavMicroservice.YRCBillofladen.WeightTypes.LB;
            //commodityInformation.palletWeight = "";
            //commodityInformation.shipmentCube = "";

            request.commodityInformation = commodityInformation;

            // Adbag_NavMicroservice.YRCBillofladen.CommodityItem commodityItem = new YRCBillofladen.CommodityItem();

            List<Adbag_NavMicroservice.YRCBillofladen.CommodityItem> commodityItem = new List<Adbag_NavMicroservice.YRCBillofladen.CommodityItem>();
            Adbag_NavMicroservice.YRCBillofladen.DeliveryServiceOptions objDeliveryOptions = new Adbag_NavMicroservice.YRCBillofladen.DeliveryServiceOptions();
            Adbag_NavMicroservice.YRCBillofladen.PickupRequest objPickupRequest = new Adbag_NavMicroservice.YRCBillofladen.PickupRequest();
            Adbag_NavMicroservice.YRCBillofladen.BolLabelPDF objBolLabelPDF = new Adbag_NavMicroservice.YRCBillofladen.BolLabelPDF();


            commodityItem.Add(new Adbag_NavMicroservice.YRCBillofladen.CommodityItem()
            {

                totalWeight = "12",
                handlingUnitQuantity = "1",
                handlingUnitType = Adbag_NavMicroservice.YRCBillofladen.HandlingUnitTypes.BAG,
                //packageQuantity="1",
                //packageUnitType= Adbag_NavMicroservice.YRCBillofladen.HandlingUnitTypes.BOX,
                productDesc = "Books",
                nmfc= "020530",
                freightClass = Adbag_NavMicroservice.YRCBillofladen.FreightClassTypes.Item60,
                //isHazardous = false,
                handlingUnitTypeSpecified = true,
                //packageUnitTypeSpecified=false,
                // length="11",
                // width="12",
                // height="20",
                //density="12",
                //cube="1",
                //freightClassSpecified=true,

            });
            //YRCBillofladen
            request.commodityItem = commodityItem.ToArray();



            objDeliveryOptions.deliveryServiceOption = Adbag_NavMicroservice.YRCBillofladen.DeliveryServiceTypes.ACEL;

            request.deliveryServiceOptions = objDeliveryOptions;

            //objPickupRequest.pickupLocationContactName = "Ginny Semrow";
            //objPickupRequest.pickupLocationPhone = "+1 623-931-1386";
            //objPickupRequest.shipmentReadyTime = "10:00";
            //objPickupRequest.dockCloseTime = "16:00";
            //objPickupRequest.isCertifiedPickup = true;
            //objPickupRequest.certifiedPickupEmailAddress = "kalpana.gaonkar@powerweave.com";
            //objPickupRequest.sendConfirmationEmail = true;
            //objPickupRequest.confirmationEmailAddress = "kalpana.gaonkar@powerweave.com";

            //request.pickupRequest = objPickupRequest;

            objBolLabelPDF.generateBolPDF = true;
            objBolLabelPDF.bolDocumentType = Adbag_NavMicroservice.YRCBillofladen.BolDocumentType.STD;
            objBolLabelPDF.generateShippingLabelsPDF = true;
            objBolLabelPDF.numberOfLabelsPerShipment = "1";
            objBolLabelPDF.labelStartingPosition = "1";
            objBolLabelPDF.labelFormat = Adbag_NavMicroservice.YRCBillofladen.LabelFormat.AVERY_5164;
            objBolLabelPDF.generateProLabelsPDF = true;
            objBolLabelPDF.proLabelBorders = false;
            objBolLabelPDF.proLabelsPerPage = "5";

            request.bolLabelPDF = objBolLabelPDF;

            Adbag_NavMicroservice.YRCBillofladen.Credentials UsernameToken = new YRCBillofladen.Credentials();
            UsernameToken.Username = ConfigurationManager.AppSettings["yrcuser"];
            UsernameToken.Password = ConfigurationManager.AppSettings["yrcpass"];

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            var response1 = objyrc.submitBOL(UsernameToken, request);
            if (response1.statusCode == "0")
            {
                Bolreply obj = new Bolreply();

                    //Bill of laden

                    if (response1.encodedBolPdf != null)
                    {
                        string Pronumber = response1.proNumber;

                        obj.bollabel = response1.encodedBolPdf;
                        string base64String = Convert.ToBase64String(obj.bollabel, 0, obj.bollabel.Length);

                        byte[] pdf = Convert.FromBase64String(base64String);
                        filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + Pronumber + ".pdf"));
                        System.IO.File.WriteAllBytes(filepath, pdf);
                        GetReport(filepath);
                        getPdf(filepath);
                        objbyteimage = new BOL();
                        objbyteimage.BOLimag = obj.bollabel;
                        empdf(obj.bollabel);
                        objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/" + Pronumber + ".pdf"));



                        return View("BillofLaden", objbyteimage);
                    }

                    //shipping label

                    //if (response1.encodedShippingLabelsPdf != null)
                    //{
                    //    string Pronumber = response1.proNumber;

                    //    obj.bollabel = response1.encodedShippingLabelsPdf;
                    //    string base64String = Convert.ToBase64String(obj.bollabel, 0, obj.bollabel.Length);

                    //    byte[] pdf = Convert.FromBase64String(base64String);
                    //    filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/SH_"+ Pronumber + ".pdf"));
                    //    System.IO.File.WriteAllBytes(filepath, pdf);
                    //    GetReport(filepath);
                    //    getPdf(filepath);
                    //    objbyteimage = new BOL();
                    //    objbyteimage.BOLimag = obj.bollabel;
                    //    empdf(obj.bollabel);
                    //    objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/SH_" + Pronumber + ".pdf"));



                    //    return View("BillofLaden", objbyteimage);
                    //}

                    //Prolabelpdf

                //    if (response1.encodedProLabelsPdf != null)
                //{
                //    string Pronumber = response1.proNumber;

                //    obj.bollabel = response1.encodedProLabelsPdf;
                //    string base64String = Convert.ToBase64String(obj.bollabel, 0, obj.bollabel.Length);

                //    byte[] pdf = Convert.FromBase64String(base64String);
                //    filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/PRO_" + Pronumber + ".pdf"));
                //    System.IO.File.WriteAllBytes(filepath, pdf);
                //    GetReport(filepath);
                //    getPdf(filepath);
                //    objbyteimage = new BOL();
                //    objbyteimage.BOLimag = obj.bollabel;
                //    empdf(obj.bollabel);
                //    objbyteimage.filepath = (Server.MapPath("~/FTPImages/ArtworkApproval/PRO_" + Pronumber + ".pdf"));



                //    return View("BillofLaden", objbyteimage);
                //}

                //printpdf(objbyteimage.filepath);
            }
            return null;
        }

        catch (Exception ex)
        {

            Exceptions.WriteExceptionLog(ex);

        }
        return null;
        }

        public class Bolreply
        {
            public string BolID { get; set; }

            public string WebProNumber { get; set; }

            public string Pronumber { get; set; }

            public byte[] shiplabel { get; set; }

            public byte[] bollabel { get; set; }
        }

    }
}
