using Adbag_NavMicroservice.FreightParameterList;
using Adbag_NavMicroservice.UPSSalesLineCarton;
using Adbag_NavMicroservice.UPSShippingInfo;
using Adbag_NavMicroservice.UPSWebTransaction;
using Adbag_NAVMicroservice.Models;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Adbag_NavMicroservice.WAH;
using Adbag_NavMicroservice.WAL;
using Adbag_NavMicroservice.Models;
using Type = Adbag_NavMicroservice.WAH.Type;
using Source_Document = Adbag_NavMicroservice.WAH.Source_Document;

namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("BarCodeScanner")]
    public class BarCodeScannerController : Controller
    {
        private string _navUrl, _host;

        WAH_PortClient objWAH_PortClient = null;
        WAH_Filter objWAH_Filter = null;

        WAL_PortClient ObjWAL_PortClient = null;
        WAL_Filter objWAL_Filter = null;

        CustomerDetails objCustomerDetails = null;

        [Route("Get")]
        [HttpGet()]
        public string Get()
        {
            return "Success";
        }

        [Route("OrderList")]
        [HttpGet()]
        public ActionResult OrderList()
        {
            var lstOrder = new List<WAHViewModel>();
            try
            {
                ViewBag.HostURL = ConfigurationManager.AppSettings["HostURL"]; ;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("~/Views/BarCodeScanner/BarCodeOrderIndex.cshtml", lstOrder);
        }

        [Route("PartialGetListOrder/{ScanWorksheetNo?}")]
        [HttpGet()]
        public ActionResult PartialGetListOrder(string ScanWorksheetNo = "")
        {
            var lstOrder = new List<WAHViewModel>();
            try
            {
                ViewBag.HostURL = ConfigurationManager.AppSettings["HostURL"] + "/BarCodeScanner/GetOrderItem/";
                _navUrl = ConfigurationManager.AppSettings["NavUrlV2"];

                BasicHttpBinding objNavWSBinding = null;
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                //objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                #region FreightParameter_List
                objCustomerDetails = new CustomerDetails();
                objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/WAH";

                objWAH_PortClient = new WAH_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                objWAH_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                objWAH_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objWAH_Filter = new WAH_Filter();
                objWAH_Filter.Field = WAH_Fields.Type;
                objWAH_Filter.Criteria = "=" + Type.Pick;
                WAH_Filter objWAH_Filter2 = new WAH_Filter();
                if (!string.IsNullOrEmpty(ScanWorksheetNo))
                {
                    objWAH_Filter2.Field = WAH_Fields.Scan_Worksheet_No;
                    objWAH_Filter2.Criteria = "=" + ScanWorksheetNo;
                }
                WAH_Filter objWAH_Filter3 = new WAH_Filter();
                if (!string.IsNullOrEmpty(ScanWorksheetNo))
                {
                    objWAH_Filter3.Field = WAH_Fields.No;
                    objWAH_Filter3.Criteria = "=" + ScanWorksheetNo;
                }
                WAH_Filter[] arryWAH_Filter = { objWAH_Filter, objWAH_Filter2 };
                WAH.WAH[] arrResponse = objWAH_PortClient.ReadMultipleAsync(arryWAH_Filter, null, 0).Result.ReadMultiple_Result1;
                #endregion
                if (arrResponse != null && arrResponse.Count() > 0)
                {
                    lstOrder = arrResponse.ToList().Select(m => new WAHViewModel()
                    {
                        Location_Code = m.Location_Code,
                        No = m.No,
                        Source_Document = Convert.ToString(Enum.GetName(typeof(Source_Document), m.Source_Document)),
                        Source_No = m.Source_No,
                        Scan_Worksheet_No = m.Scan_Worksheet_No
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("~/Views/BarCodeScanner/PartialBarCodeOrderIndex.cshtml", lstOrder);
        }

        [Route("GetOrderItem/{OrderNumber}")]
        [HttpGet()]
        public ActionResult GetOrderItem(string OrderNumber)
        {
            List<WAL.WAL> lstOrderItem = new List<WAL.WAL>();
            ViewBag.OrderList = ConfigurationManager.AppSettings["HostURL"] + "/BarCodeScanner/OrderList";
            ViewBag.OrderListDetails = ConfigurationManager.AppSettings["HostURL"] + "/BarCodeScanner/ItemDetails/";
            try
            {
                if (!string.IsNullOrEmpty(OrderNumber))
                {
                    ViewBag.URL = _host;
                    _navUrl = ConfigurationManager.AppSettings["NavUrlV2"];

                    BasicHttpBinding objNavWSBinding = null;
                    objNavWSBinding = new BasicHttpBinding();
                    objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                    objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                    //objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                    objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                    #region FreightParameter_List
                    objCustomerDetails = new CustomerDetails();
                    objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/WAL";

                    ObjWAL_PortClient = new WAL_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                    ObjWAL_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    ObjWAL_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                    objWAL_Filter = new WAL_Filter();
                    objWAL_Filter.Field = WAL_Fields.No;
                    objWAL_Filter.Criteria = "=" + OrderNumber;

                    WAL_Filter objWAL_Filter3 = new WAL_Filter();
                    objWAL_Filter3.Field = WAL_Fields.Action_Type;
                    objWAL_Filter3.Criteria = "=" + Action_Type.Take;

                    WAL_Filter[] arryWAL_Filter = { objWAL_Filter, objWAL_Filter3 };


                    WAL.WAL[] arrResponse = ObjWAL_PortClient.ReadMultipleAsync(arryWAL_Filter, null, 0).Result.ReadMultiple_Result1;
                    #endregion
                    if(arrResponse != null)
                    {
                        lstOrderItem = arrResponse.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("~/Views/BarCodeScanner/BarCodeOrderItem.cshtml", lstOrderItem);
        }

        [Route("PartialGetOrderItem/{OrderNumber}/{OrderLineNumber}")]
        [HttpGet()]
        public ActionResult PartialGetOrderItem(string OrderNumber, string OrderLineNumber)
        {
            List<WAL.WAL> lstOrderItem = new List<WAL.WAL>();
            ViewBag.OrderList = ConfigurationManager.AppSettings["HostURL"] + "/BarCodeScanner/OrderList";
            ViewBag.OrderListDetails = ConfigurationManager.AppSettings["HostURL"] + "/BarCodeScanner/ItemDetails/";
            try
            {
                if (!string.IsNullOrEmpty(OrderLineNumber))
                {
                    ViewBag.URL = _host;
                    _navUrl = ConfigurationManager.AppSettings["NavUrlV2"];

                    BasicHttpBinding objNavWSBinding = null;
                    objNavWSBinding = new BasicHttpBinding();
                    objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                    objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                    objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                    #region FreightParameter_List
                    objCustomerDetails = new CustomerDetails();
                    objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/WAL";

                    ObjWAL_PortClient = new WAL_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                    ObjWAL_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    ObjWAL_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                    WAL_Filter  objWAL_Filter2 = new WAL_Filter();
                    objWAL_Filter2.Field = WAL_Fields.No;
                    objWAL_Filter2.Criteria = "=" + OrderNumber;

                    objWAL_Filter = new WAL_Filter();
                    objWAL_Filter.Field = WAL_Fields.Item_No;
                    objWAL_Filter.Criteria = "=" + OrderLineNumber;

                    WAL_Filter objWAL_Filter3 = new WAL_Filter();
                    objWAL_Filter3.Field = WAL_Fields.Action_Type;
                    objWAL_Filter3.Criteria = "=" + Action_Type.Take;

                    WAL_Filter[] arryWAL_Filter = { objWAL_Filter, objWAL_Filter2, objWAL_Filter3 };
                    WAL.WAL[] arrResponse = ObjWAL_PortClient.ReadMultipleAsync(arryWAL_Filter, null, 0).Result.ReadMultiple_Result1;
                    #endregion
                    if (arrResponse != null)
                    {
                        lstOrderItem = arrResponse.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("~/Views/BarCodeScanner/PartialGetOrderItem.cshtml", lstOrderItem);
        }

        [Route("ItemDetails/{OrderNumber}/{OrderLineNumber}")]
        [HttpGet()]
        public ActionResult GetOrderItemDetails(string OrderNumber, string OrderLineNumber)
        {
            WAL.WAL OrderItem = new WAL.WAL();
            ViewBag.OrderItemList = ConfigurationManager.AppSettings["HostURL"] + "/BarCodeScanner/GetOrderItem/"+ OrderNumber;
            try
            {
                if (!string.IsNullOrEmpty(OrderLineNumber))
                {
                    ViewBag.URL = _host;
                    _navUrl = ConfigurationManager.AppSettings["NavUrlV2"];

                    BasicHttpBinding objNavWSBinding = null;
                    objNavWSBinding = new BasicHttpBinding();
                    objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                    objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                    objNavWSBinding.MaxReceivedMessageSize = 2147483647;

                    #region FreightParameter_List
                    objCustomerDetails = new CustomerDetails();
                    objCustomerDetails.strCustomerPageURL = _navUrl + "/Page/WAL";

                    ObjWAL_PortClient = new WAL_PortClient(objNavWSBinding, new EndpointAddress(objCustomerDetails.strCustomerPageURL));
                    ObjWAL_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    ObjWAL_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                    objWAL_Filter = new WAL_Filter();
                    objWAL_Filter.Field = WAL_Fields.Item_No;
                    objWAL_Filter.Criteria = "=" + OrderLineNumber;

                    WAL_Filter objWAL_Filter2 = new WAL_Filter();
                    objWAL_Filter2.Field = WAL_Fields.No;
                    objWAL_Filter2.Criteria = "=" + OrderNumber;

                    WAL_Filter objWAL_Filter3 = new WAL_Filter();
                    objWAL_Filter3.Field = WAL_Fields.Action_Type;
                    objWAL_Filter3.Criteria = "=" + Action_Type.Take;

                    WAL_Filter[] arryWAL_Filter = { objWAL_Filter, objWAL_Filter2, objWAL_Filter3 };
                    WAL.WAL[] arrResponse = ObjWAL_PortClient.ReadMultipleAsync(arryWAL_Filter, null, 0).Result.ReadMultiple_Result1;
                    #endregion
                    if (arrResponse != null && arrResponse[0] != null)
                    {
                        OrderItem = arrResponse[0];
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("~/Views/BarCodeScanner/BarCodeOrderItemDetails.cshtml", OrderItem);
        }

    }
}