﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Adbag_NavMicroservice.UPSWebTransaction {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", ConfigurationName="UPSWebTransaction.Web_Transaction_Port")]
    public interface Web_Transaction_Port {
        
        // CODEGEN: Generating message contract since the wrapper name (UpdateUPSCharges_Result) of message UpdateUPSCharges_Result does not match the default value (UpdateUPSCharges)
        [System.ServiceModel.OperationContractAttribute(Action="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction:UpdateUPSCharges", ReplyAction="*")]
        Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges_Result UpdateUPSCharges(Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges request);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction:UpdateUPSCharges", ReplyAction="*")]
        System.Threading.Tasks.Task<Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges_Result> UpdateUPSChargesAsync(Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UpdateUPSCharges", WrapperNamespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", IsWrapped=true)]
    public partial class UpdateUPSCharges {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", Order=0)]
        public int documentType;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", Order=1)]
        public string documentNo;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", Order=2)]
        public int lineNo;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", Order=3)]
        public string chargeDescription;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", Order=4)]
        public string chargeCost;
        
        public UpdateUPSCharges() {
        }
        
        public UpdateUPSCharges(int documentType, string documentNo, int lineNo, string chargeDescription, string chargeCost) {
            this.documentType = documentType;
            this.documentNo = documentNo;
            this.lineNo = lineNo;
            this.chargeDescription = chargeDescription;
            this.chargeCost = chargeCost;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UpdateUPSCharges_Result", WrapperNamespace="urn:microsoft-dynamics-schemas/codeunit/Web_Transaction", IsWrapped=true)]
    public partial class UpdateUPSCharges_Result {
        
        public UpdateUPSCharges_Result() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface Web_Transaction_PortChannel : Adbag_NavMicroservice.UPSWebTransaction.Web_Transaction_Port, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class Web_Transaction_PortClient : System.ServiceModel.ClientBase<Adbag_NavMicroservice.UPSWebTransaction.Web_Transaction_Port>, Adbag_NavMicroservice.UPSWebTransaction.Web_Transaction_Port {
        
        public Web_Transaction_PortClient() {
        }
        
        public Web_Transaction_PortClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public Web_Transaction_PortClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Web_Transaction_PortClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Web_Transaction_PortClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges_Result Adbag_NavMicroservice.UPSWebTransaction.Web_Transaction_Port.UpdateUPSCharges(Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges request) {
            return base.Channel.UpdateUPSCharges(request);
        }
        
        public void UpdateUPSCharges(int documentType, string documentNo, int lineNo, string chargeDescription, string chargeCost) {
            Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges inValue = new Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges();
            inValue.documentType = documentType;
            inValue.documentNo = documentNo;
            inValue.lineNo = lineNo;
            inValue.chargeDescription = chargeDescription;
            inValue.chargeCost = chargeCost;
            Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges_Result retVal = ((Adbag_NavMicroservice.UPSWebTransaction.Web_Transaction_Port)(this)).UpdateUPSCharges(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges_Result> Adbag_NavMicroservice.UPSWebTransaction.Web_Transaction_Port.UpdateUPSChargesAsync(Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges request) {
            return base.Channel.UpdateUPSChargesAsync(request);
        }
        
        public System.Threading.Tasks.Task<Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges_Result> UpdateUPSChargesAsync(int documentType, string documentNo, int lineNo, string chargeDescription, string chargeCost) {
            Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges inValue = new Adbag_NavMicroservice.UPSWebTransaction.UpdateUPSCharges();
            inValue.documentType = documentType;
            inValue.documentNo = documentNo;
            inValue.lineNo = lineNo;
            inValue.chargeDescription = chargeDescription;
            inValue.chargeCost = chargeCost;
            return ((Adbag_NavMicroservice.UPSWebTransaction.Web_Transaction_Port)(this)).UpdateUPSChargesAsync(inValue);
        }
    }
}
