using Adbag_NavMicroservice.ArtworkApproval;
using Adbag_NavMicroservice.SalesLineWeb;
using Adbag_NAVMicroservice.Models;
using Adbag_NAVMicroservice.Models.GlobalUtilities;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Adbag_NavMicroservice.Controllers
{
    [RoutePrefix("Artwork")]
    public class ArtworkApprovalController : Controller
    {

        private string _host, _navUrl;

        BasicHttpBinding objNavWSBinding = null;
        //SystemService_PortClient systemService = null;
        WS_Artwork_Approval_Listing_PortClient artworkService = null;
        WS_Artwork_Approval_Listing objArtworkListingRef = null;
        WS_Artwork_Approval_Listing_Filter[] objArtworkFilter;

        Sales_Line_Web_PortClient objSalesLineWeb_PortClient = null;
        Sales_Line_Web objSalesLineWeb = null;
        Sales_Line_Web_Filter[] objSalesLineWeb_Filter = null;
        ArtworkApprovalDetails objArtworkApprovalDetails = null;

        [Route("GetArtworkDetails/{PrimaryKey}/{IsSO}")]
        [HttpGet()]
        public async Task<ActionResult> GetArtworkDetails(string PrimaryKey, string IsSO)
        {
            try
            {
                string OrderNo = string.Empty;
                string[] arrykeys = new string[2];
                _navUrl = ConfigurationManager.AppSettings["NavUrl"].ToString();
                _host = ConfigurationManager.AppSettings["HostURL"].ToString();
                //Added By Swapnil for Assorted
                string NewPrimaryKey = string.Empty;
                string SO = IsSO;
                if (SO == "Yes")
                {

                    if (PrimaryKey.Contains("{") && PrimaryKey.Contains("}"))
                    {
                        arrykeys = ConvertGUID(PrimaryKey, IsSO);
                    }
                    else
                    {
                        //OrderNo = PrimaryKey;
                        arrykeys = ConvertGUID(PrimaryKey, IsSO);
                    }

                    BasicHttpBinding objNavWSBindingNew = null;
                    WS_Artwork_Approval_Listing_PortClient artworkServiceNew = null;
                    WS_Artwork_Approval_Listing objArtworkListingRefNew = null;
                    WS_Artwork_Approval_Listing_Filter[] objArtworkFilterNew;

                    objNavWSBindingNew = new BasicHttpBinding();
                    objNavWSBindingNew.MaxReceivedMessageSize = Int32.MaxValue;
                    objNavWSBindingNew.MaxBufferSize = Int32.MaxValue;
                    objNavWSBindingNew.Security.Mode = BasicHttpSecurityMode.Transport;
                    objNavWSBindingNew.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                    string strCustomerPageURLNew = _navUrl + "/Page/WS_Artwork_Approval_Listing";

                    artworkServiceNew = new WS_Artwork_Approval_Listing_PortClient(objNavWSBindingNew, new EndpointAddress(strCustomerPageURLNew));
                    //artworkServiceNew.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                    artworkServiceNew.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                    artworkServiceNew.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                    objArtworkListingRefNew = new WS_Artwork_Approval_Listing();

                    objArtworkFilterNew = new WS_Artwork_Approval_Listing_Filter[1];
                    objArtworkFilterNew[0] = new WS_Artwork_Approval_Listing_Filter();
                    objArtworkFilterNew[0].Field = WS_Artwork_Approval_Listing_Fields.Order_No;
                    objArtworkFilterNew[0].Criteria = "=" + OrderNo;

                    WS_Artwork_Approval_Listing[] obj2_New = artworkServiceNew.ReadMultipleAsync(objArtworkFilterNew, null, 0).Result.ReadMultiple_Result1;

                    if (obj2_New != null)
                    {
                        if (obj2_New.Length > 0)
                        {
                            for (int i = 0; i < obj2_New.Count(); i++)
                            {
                                NewPrimaryKey = arrykeys[0] + "|" + arrykeys[1];
                            }
                            NewPrimaryKey = NewPrimaryKey.TrimStart('|');
                        }
                    }
                }
                else
                {

                    if (PrimaryKey.Contains("{") && PrimaryKey.Contains("}"))
                    {
                        arrykeys = ConvertGUID(PrimaryKey, IsSO);
                    }
                    else
                    {
                        //OrderNo = PrimaryKey;
                        arrykeys = ConvertGUID(PrimaryKey, IsSO);
                    }

                    //if (PrimaryKey.Contains("{") && PrimaryKey.Contains("}"))
                    //{
                    //    NewPrimaryKey = arrykeys[0] + "|" + arrykeys[1];
                    //}
                    //else
                    //{
                    //    NewPrimaryKey = PrimaryKey;
                    //}
                }
                //End By Swapnil

                ViewBag.URL = _host;
                ViewBag.PrimaryKey = PrimaryKey;
                ViewBag.NewPrimaryKey = NewPrimaryKey;
                ViewBag.IsSO = IsSO;

                objArtworkApprovalDetails = new ArtworkApprovalDetails();
                // Create a NAV compatible binding
                objNavWSBinding = new BasicHttpBinding();
                objNavWSBinding.MaxReceivedMessageSize = Int32.MaxValue;
                objNavWSBinding.MaxBufferSize = Int32.MaxValue;
                objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                string strCustomerPageURL = _navUrl + "/Page/WS_Artwork_Approval_Listing";
                artworkService = new WS_Artwork_Approval_Listing_PortClient(objNavWSBinding, new EndpointAddress(strCustomerPageURL));
                //artworkService.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                artworkService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                artworkService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objArtworkListingRef = new WS_Artwork_Approval_Listing();

                objArtworkFilter = new WS_Artwork_Approval_Listing_Filter[NewPrimaryKey.Split('|').Length];
                for (int i = 0; i < NewPrimaryKey.Split('|').Length; i++)
                {
                    objArtworkFilter[i] = new WS_Artwork_Approval_Listing_Filter();
                    objArtworkFilter[i].Field = WS_Artwork_Approval_Listing_Fields.Order_No;
                  objArtworkFilter[i].Criteria = "=" + arrykeys[0];

                    objArtworkFilter[i] = new WS_Artwork_Approval_Listing_Filter();
                    objArtworkFilter[i].Field = WS_Artwork_Approval_Listing_Fields.Line_No;
                    objArtworkFilter[i].Criteria = "=" + arrykeys[1];
                }

                WS_Artwork_Approval_Listing[] obj2 = artworkService.ReadMultipleAsync(objArtworkFilter, null, 0).Result.ReadMultiple_Result1;


                //objArtworkApprovalDetails.SalesOrder = objArtworkListingRef.Order_No.Trim();
                if (obj2 != null)
                {
                    if (obj2.Length > 0)
                    {
                        string lsColors = string.Empty;
                        string ItemNo = string.Empty;
                        int PrevItemNo = 0;
                        bool isPreProduction = false;

                        //Added By Swapnil
                        string strItemNumber = string.Empty;
                        string strQuantity = string.Empty;
                        string strItemColor = string.Empty;
                        //End

                        for (int i = 0; i < obj2.Length; i++)
                        {
                            if (obj2[i].Is_Pre_Production_Item)
                                isPreProduction = true;
                            objArtworkApprovalDetails.CustomerPO = Convert.ToString(obj2[i].External_Document_No);
                        }
                        if (isPreProduction)
                        {
                            for (int i = 0; i < obj2.Length; i++)
                            {
                                if (obj2[i].Is_Pre_Production_Item)
                                {
                                    lsColors = string.Empty;
                                    if (obj2[i].Line_No == PrevItemNo)
                                    {
                                        objArtworkApprovalDetails.ItemDescription = string.Empty;
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(obj2[i].Item_No))
                                        {
                                            strItemNumber = strItemNumber + "," + Convert.ToString(obj2[i].Item_No);
                                        }

                                        objArtworkApprovalDetails.ItemDescription = Convert.ToString(obj2[i].recItem_Description);

                                        var ObjQtyVal = await artworkService.GetOrderQuantityAsync(obj2[i].Key);
                                        if (obj2[i].Full_Back_Order)
                                        {
                                            strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value);
                                            //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value);
                                        }
                                        else
                                        {
                                            strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].Back_Order);
                                            //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].BackOrder);
                                        }

                                        if (obj2[i].Is_Pre_Production_Item)
                                        {
                                            var ObjActualQty = await artworkService.GetActualQuantityAsync(obj2[i].Key);
                                            objArtworkApprovalDetails.ActualQuantity = Convert.ToString(ObjActualQty.return_value);
                                        }

                                        if (!string.IsNullOrEmpty(obj2[i].Web_Color))
                                        {
                                            strItemColor = strItemColor + "," + Convert.ToString(obj2[i].Web_Color);
                                        }
                                    }
                                    if (obj2[i].Method != null)
                                    {
                                        var ObjQtyImprint = await artworkService.GetImprintMethodNameAsync(obj2[i].Key, obj2[i].Method.Trim());
                                        objArtworkApprovalDetails.ImprintMethod = Convert.ToString(ObjQtyImprint.return_value);

                                    }
                                    else
                                        objArtworkApprovalDetails.ImprintMethod = string.Empty;

                                    objArtworkApprovalDetails.ImprintLocation = Convert.ToString(obj2[i].Location);

                                    if (obj2[i].Imprint_Location_Size != null)
                                    {
                                        objArtworkApprovalDetails.ImprintSize = Convert.ToString(obj2[i].Imprint_Location_Size) + " Inches";
                                    }

                                    GetImprintColors Objcolor = new GetImprintColors()
                                    {
                                        imprint = obj2[i].Key,
                                        imprintColors = "",
                                    };
                                    var objimpcolor = await artworkService.GetImprintColorsAsync(Objcolor);
                                    lsColors = objimpcolor.imprintColors.Replace("|", ", ").Trim().TrimEnd(',');
                                    if (lsColors.Contains(','))
                                    {
                                        string strImprintColors = lsColors.Replace(",", "\r\n");
                                        objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strImprintColors.ToLower());
                                    }
                                    else
                                    {
                                        objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(lsColors.ToLower());
                                    }
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < obj2.Length; i++)
                            {
                                lsColors = string.Empty;
                                if (obj2[i].Line_No == PrevItemNo)
                                {
                                    //objArtworkApprovalDetails.ItemNumber = string.Empty;
                                    objArtworkApprovalDetails.ItemDescription = string.Empty;
                                    //objArtworkApprovalDetails.Quantity = string.Empty;
                                    //objArtworkApprovalDetails.ItemColor = string.Empty;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(obj2[i].Item_No))
                                    {
                                        strItemNumber = strItemNumber + "," + Convert.ToString(obj2[i].Item_No);
                                    }

                                    objArtworkApprovalDetails.ItemDescription = Convert.ToString(obj2[i].recItem_Description);

                                    var ObjQtyVal = await artworkService.GetOrderQuantityAsync(obj2[i].Key);
                                    if (obj2[i].Full_Back_Order)
                                    {
                                        strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value);
                                        //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value);
                                    }
                                    else
                                    {
                                        strQuantity = strQuantity + "," + Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].Back_Order);
                                        //objArtworkApprovalDetails.Quantity = Convert.ToString(ObjQtyVal.return_value) + Convert.ToString(obj2[i].BackOrder);
                                    }

                                    if (obj2[i].Is_Pre_Production_Item)
                                    {
                                        var ObjActualQty = await artworkService.GetActualQuantityAsync(obj2[i].Key);
                                        objArtworkApprovalDetails.ActualQuantity = Convert.ToString(ObjActualQty.return_value);
                                    }

                                    if (!string.IsNullOrEmpty(obj2[i].Web_Color))
                                    {
                                        strItemColor = strItemColor + "," + Convert.ToString(obj2[i].Web_Color);
                                    }
                                }
                                if (obj2[i].Method != null)
                                {
                                    var ObjQtyImprint = await artworkService.GetImprintMethodNameAsync(obj2[i].Key, obj2[i].Method.Trim());
                                    objArtworkApprovalDetails.ImprintMethod = Convert.ToString(ObjQtyImprint.return_value);

                                }
                                else
                                    objArtworkApprovalDetails.ImprintMethod = string.Empty;

                                objArtworkApprovalDetails.ImprintLocation = Convert.ToString(obj2[i].Location);

                                if (obj2[i].Imprint_Location_Size != null)
                                {
                                    objArtworkApprovalDetails.ImprintSize = Convert.ToString(obj2[i].Imprint_Location_Size) + " Inches";
                                }

                                GetImprintColors Objcolor = new GetImprintColors()
                                {
                                    imprint = obj2[i].Key,
                                    imprintColors = "",
                                };
                                var objimpcolor = await artworkService.GetImprintColorsAsync(Objcolor);
                                lsColors = objimpcolor.imprintColors.Replace("|", ", ").Trim().TrimEnd(',');
                                if (lsColors.Contains(','))
                                {
                                    string strImprintColors = lsColors.Replace(",", "\r\n");
                                    objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(strImprintColors.ToLower());
                                }
                                else
                                {
                                    objArtworkApprovalDetails.ImprintColors = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(lsColors.ToLower());
                                }
                            }

                        }

                        //Added By Swapnil
                        strItemNumber = strItemNumber.TrimStart(',');
                        strQuantity = strQuantity.TrimStart(',');
                        strItemColor = strItemColor.TrimStart(',');

                        //objArtworkApprovalDetails.ItemNumber = strItemNumber;
                        objArtworkApprovalDetails.Quantity = strQuantity;
                        objArtworkApprovalDetails.ItemColor = strItemColor;
                        //End

                        objArtworkListingRef = obj2[0];
                        objArtworkApprovalDetails.ItemNumber = Convert.ToString(objArtworkListingRef.Item_No);
                        objArtworkApprovalDetails.SalesOrder = Convert.ToString(objArtworkListingRef.Order_No);
                        objArtworkApprovalDetails.ArtFile = Convert.ToString(objArtworkListingRef.Art_ID);
                        objArtworkApprovalDetails.SalesPerson = Convert.ToString(objArtworkListingRef.recsh_Salesperson_Code);
                        objArtworkApprovalDetails.NoOfRework = Convert.ToString(objArtworkListingRef.No_of_Rework);
                        if (!string.IsNullOrEmpty(objArtworkListingRef.Artwork_DateTime.ToString()))
                        {
                            string[] _splitDate = Convert.ToString(objArtworkListingRef.Artwork_DateTime).Split(' ');
                            if (_splitDate.Count() > 0)
                            {
                                objArtworkApprovalDetails.ArtworkDate = !string.IsNullOrEmpty(_splitDate[0].ToString()) ? _splitDate[0].ToString() : string.Empty;
                                objArtworkApprovalDetails.ArtworkTime = (!string.IsNullOrEmpty(_splitDate[1].ToString()) ? _splitDate[1].ToString() : string.Empty) + (!string.IsNullOrEmpty(_splitDate[2].ToString()) ? _splitDate[2].ToString() : string.Empty);
                            }
                        }
                        objArtworkApprovalDetails.IsBlindPage = objArtworkListingRef.Blind_Page;
                        objArtworkApprovalDetails.CommentsForCustomer = Convert.ToString(objArtworkListingRef.Comments_for_customer);

                        if (!string.IsNullOrEmpty(objArtworkListingRef.Order_No.ToString()) || objArtworkListingRef.Order_No != null)
                        {
                            try
                            {
                                #region SalesLine Web
                                string strCustomerPageURLSales = _navUrl + "/Page/Sales_Line_Web";
                                objSalesLineWeb_PortClient = new Sales_Line_Web_PortClient(objNavWSBinding, new EndpointAddress(strCustomerPageURLSales));
                                //objSalesLineWeb_PortClient.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                                objSalesLineWeb_PortClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                                objSalesLineWeb_PortClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                                objSalesLineWeb = new Sales_Line_Web();

                                objSalesLineWeb_Filter = new Sales_Line_Web_Filter[1];
                                objSalesLineWeb_Filter[0] = new Sales_Line_Web_Filter();
                                objSalesLineWeb_Filter[0].Field = Sales_Line_Web_Fields.Document_No;
                                objSalesLineWeb_Filter[0].Criteria = "=" + Convert.ToString(objArtworkListingRef.Order_No);

                                Sales_Line_Web[] objSLB = objSalesLineWeb_PortClient.ReadMultipleAsync(objSalesLineWeb_Filter, null, 0).Result.ReadMultiple_Result1;
                                if (objSLB != null)
                                {
                                    if (objSLB.Length > 0)
                                    {
                                        if (SO == "Yes")
                                        {
                                            objArtworkApprovalDetails.PackagingOption = Convert.ToString(objSLB[0].Packaging_Option);
                                        }
                                        else
                                        {
                                            string strPackagingOption = string.Empty;
                                            for (int i = 0; i < objSLB.Count(); i++)
                                            {
                                                strPackagingOption = strPackagingOption + "," + Convert.ToString(objSLB[i].Packaging_Option);
                                            }
                                            strPackagingOption = strPackagingOption.TrimStart(',');
                                            objArtworkApprovalDetails.PackagingOption = strPackagingOption;
                                        }
                                    }
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Exceptions.WriteExceptionLog(ex);
                            }
                        }
                    }
                }

                //string webRootPath = _hostingEnvironment.WebRootPath;
                Array.ForEach(Directory.GetFiles(Server.MapPath("~/FTPImages/ArtworkApproval/")), System.IO.File.Delete);
                //string ftpFileSourcePath = "ftp://pwpdc.powerweave.com/clothpromotions-Images/Artwork/Proof/" + Convert.ToString(objArtworkListingRef.Order_No) + "/ArtWork/";

                //if (!string.IsNullOrEmpty(objArtworkListingRef.Proof_Image))
                //{
                //    DownloadFile("clothpromotions", "Cl0ThProM0T!on$%543", ftpFileSourcePath + objArtworkListingRef.Proof_Image.Trim(), webRootPath + "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Proof_Image.Trim());
                //    objArtworkApprovalDetails.ImagePath1 = "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Proof_Image.Trim();
                //}
                //if (!string.IsNullOrEmpty(objArtworkListingRef.Ruler_Image))
                //{
                //    DownloadFile("clothpromotions", "Cl0ThProM0T!on$%543", ftpFileSourcePath + objArtworkListingRef.Ruler_Image.Trim(), webRootPath + "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Ruler_Image.Trim());
                //    objArtworkApprovalDetails.ImagePath2 = "/FTPImages/ArtworkApproval/" + objArtworkListingRef.Ruler_Image.Trim();
                //}
                if (!string.IsNullOrEmpty(objArtworkListingRef.Proof_Image))
                {
                    ViewBag.Image1 = objArtworkListingRef.Proof_Image;
                }
                if (!string.IsNullOrEmpty(objArtworkListingRef.Ruler_Image))
                {
                    ViewBag.Image2 = objArtworkListingRef.Ruler_Image;
                }
                ViewBag.OrderNo = Convert.ToString(objArtworkListingRef.Order_No);

                // Check artwork has been evaluated by customer or not
                if (objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Rejected_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof_with_Suggestions)
                {
                    if (objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof_with_Suggestions)
                    {
                        objArtworkApprovalDetails.Status = "This proof is already approved.";
                        if (objArtworkListingRef.Proof_Approved_DateSpecified)
                        {
                            if (objArtworkListingRef.Proof_Approved_Date != null)
                            {
                                if (objArtworkListingRef.Proof_Approved_Date.ToShortDateString().Trim() != "1/1/0001")
                                {
                                    objArtworkApprovalDetails.Status = "This proof is already approved on " + objArtworkListingRef.Proof_Approved_Date.ToShortDateString();
                                }
                            }
                        }
                    }
                    else if (objArtworkListingRef.Status == Status.Customer_Rejected_Paper_Proof)
                    {
                        objArtworkApprovalDetails.Status = "This proof is already rejected.";
                        if (objArtworkListingRef.Proof_Approved_DateSpecified)
                        {
                            if (objArtworkListingRef.Proof_Approved_Date != null)
                            {
                                if (objArtworkListingRef.Proof_Approved_Date.ToShortDateString().Trim() != "1/1/0001")
                                    objArtworkApprovalDetails.Status = "This proof is already rejected on " + objArtworkListingRef.Proof_Approved_Date.ToShortDateString();
                            }
                        }
                    }
                }
                else
                {
                    objArtworkApprovalDetails.Status = "Ok";
                }
                //End

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("GetArtworkDetails", objArtworkApprovalDetails);
        }

        private string[] ConvertGUID(string GUIDKey, string IsSO)
        {
            string[] key = new string[2];
            //string key1 = string.Empty;
            try
            {
                BasicHttpBinding objNavWSBindingGUID = null;
                WS_Artwork_Approval_Listing_PortClient artworkServiceGUID = null;
                WS_Artwork_Approval_Listing objArtworkListingRefGUID = null;
                WS_Artwork_Approval_Listing_Filter[] objArtworkFilterGUID;

                objNavWSBindingGUID = new BasicHttpBinding();
                objNavWSBindingGUID.MaxReceivedMessageSize = Int32.MaxValue;
                objNavWSBindingGUID.MaxBufferSize = Int32.MaxValue;
                objNavWSBindingGUID.Security.Mode = BasicHttpSecurityMode.Transport;
                objNavWSBindingGUID.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                string strCustomerPageURLNew = _navUrl + "/Page/WS_Artwork_Approval_Listing";

                artworkServiceGUID = new WS_Artwork_Approval_Listing_PortClient(objNavWSBindingGUID, new EndpointAddress(strCustomerPageURLNew));
                //artworkServiceGUID.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                artworkServiceGUID.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                artworkServiceGUID.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                objArtworkListingRefGUID = new WS_Artwork_Approval_Listing();

                objArtworkFilterGUID = new WS_Artwork_Approval_Listing_Filter[1];
                objArtworkFilterGUID[0] = new WS_Artwork_Approval_Listing_Filter();
                objArtworkFilterGUID[0].Field = WS_Artwork_Approval_Listing_Fields.GUID;
                objArtworkFilterGUID[0].Criteria = "=" + GUIDKey;

                WS_Artwork_Approval_Listing[] obj2_GUID = artworkServiceGUID.ReadMultipleAsync(objArtworkFilterGUID, null, 0).Result.ReadMultiple_Result1;

                if (IsSO == "Yes")
                {
                    if (obj2_GUID != null)
                    {
                        if (obj2_GUID.Length > 0)
                        {
                            key[0] = obj2_GUID[0].Order_No;
                            key[1] = Convert.ToString(obj2_GUID[0].Line_No); 
                        }
                    }
                }
                else
                {
                    if (obj2_GUID != null)
                    {
                        if (obj2_GUID.Length > 0)
                        {
                            key[0] = Convert.ToString(obj2_GUID[0].Order_No);
                            key[1] = Convert.ToString(obj2_GUID[0].Line_No);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return key;
        }

        private void DownloadFile(string userName, string password, string ftpSourceFilePath, string localDestinationFilePath)
        {
            try
            {
                WebClient client = new WebClient();
                client.Credentials = new NetworkCredential(userName, password);
                client.DownloadFile(ftpSourceFilePath, localDestinationFilePath);
            }
            catch (Exception ex)
            {

                Exceptions.WriteExceptionLog(ex);
            }
        }

        [Route("DownloadFile/{filename}")]
        [HttpGet()]
        public FileResult DownloadFile(string filename)
        {
            MemoryStream ms = null;
            string fullPath = string.Empty;
            try
            {
                //string webRootPath = _hostingEnvironment.WebRootPath;
                fullPath = Path.Combine(Server.MapPath("~/FTPImages/ArtworkApproval/"), filename);

                WebClient myWebClient = new WebClient();
                byte[] imageBytes = myWebClient.DownloadData(fullPath);
                ms = new MemoryStream(imageBytes);
                //var image = System.Drawing.Image.FromStream(ms);
                //image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return File(ms.ToArray(), "image/jpeg", filename);
            //new File(myDataBuffer, "image/jpeg", "ImageName");
        }

        [Route("LoadImages")]
        [HttpPost]
        public string LoadImages(ImageDetails objImageDetails)
        {
            ImageDetails imgDetails = new ImageDetails();

            try
            {
                //objArtworkApprovalDetails = new ArtworkApprovalDetails();              
                //string webRootPath = _hostingEnvironment.WebRootPath;
                Array.ForEach(Directory.GetFiles(Server.MapPath("~/FTPImages/ArtworkApproval/")), System.IO.File.Delete);
                string ftpFileSourcePath = "ftp://cs.artworkservicesusa.com/Adbag_ERP/Artwork/Proof/" + objImageDetails.OrderNo.Trim() + "/ArtWork/";
                if (!string.IsNullOrEmpty(objImageDetails.ImgPath1))
                {
                    DownloadFile("Adbag", "@DbAg&%7891", ftpFileSourcePath + objImageDetails.ImgPath1.Trim(), Server.MapPath("~/FTPImages/ArtworkApproval/" + objImageDetails.ImgPath1.Trim()));
                    imgDetails.ImgPath1 = "/FTPImages/ArtworkApproval/" + objImageDetails.ImgPath1.Trim();
                }
                if (!string.IsNullOrEmpty(objImageDetails.ImgPath2))
                {
                    DownloadFile("Adbag", "@DbAg&%7891", ftpFileSourcePath + objImageDetails.ImgPath2.Trim(), Server.MapPath("~/FTPImages/ArtworkApproval/" + objImageDetails.ImgPath2.Trim()));
                    imgDetails.ImgPath2 = "/FTPImages/ArtworkApproval/" + objImageDetails.ImgPath2.Trim();
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return JsonConvert.SerializeObject(imgDetails);
        }

        [HttpPost]
        [Route("SubmitArtwork")]
        public ActionResult SubmitArtwork(ArtworkApprovalDetails objArtworkApprovalDetails)
        {
            try
            {
                //To get IP Address for Remote User  
                string ipaddress = string.Empty;
                //ipString = HttpContext.Connection.RemoteIpAddress.ToString();
                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                //End
                Boolean lbIsStatusUpdated = false;
                string ArtworkStatus = objArtworkApprovalDetails.ArtworkApprovalStatus;
                string Comments = objArtworkApprovalDetails.Comments;
                string Signature = objArtworkApprovalDetails.Signature;
                string PrimaryKey = objArtworkApprovalDetails.NewPrimaryKey;
                //if (!string.IsNullOrEmpty(Signature))
                //{
                if (!string.IsNullOrEmpty(PrimaryKey))
                {
                    BasicHttpBinding objNavWSBinding = null;
                    //SystemService_PortClient systemService = null;
                    WS_Artwork_Approval_Listing_PortClient artworkService = null;
                    WS_Artwork_Approval_Listing objArtworkListingRef = null;
                    WS_Artwork_Approval_Listing_Filter objArtworkFilter = null;

                    try
                    {
                        _navUrl = ConfigurationManager.AppSettings["NavUrl"].ToString();

                        objNavWSBinding = new BasicHttpBinding();
                        objNavWSBinding.MaxReceivedMessageSize = Int32.MaxValue;
                        objNavWSBinding.MaxBufferSize = Int32.MaxValue;
                        objNavWSBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                        objNavWSBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                        string strCustomerPageURL = _navUrl + "/Page/WS_Artwork_Approval_Listing";
                        artworkService = new WS_Artwork_Approval_Listing_PortClient(objNavWSBinding, new EndpointAddress(strCustomerPageURL));
                        //artworkService.ClientCredentials.Windows.ClientCredential = new NetworkCredential(_iconfiguration["WebServiceUserName"], _iconfiguration["WebServiceUserPassword"]);
                        artworkService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["WebServiceUserName"].ToString();
                        artworkService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["WebServiceUserPassword"].ToString();

                        objArtworkListingRef = new WS_Artwork_Approval_Listing();
                        objArtworkFilter = new WS_Artwork_Approval_Listing_Filter();
                        objArtworkFilter.Field = WS_Artwork_Approval_Listing_Fields.Primary_Key;
                        objArtworkFilter.Criteria = "=" + PrimaryKey;

                        WS_Artwork_Approval_Listing_Filter[] FilterArr = { objArtworkFilter };
                        WS_Artwork_Approval_Listing[] obj2 = artworkService.ReadMultipleAsync(FilterArr, null, 0).Result.ReadMultiple_Result1;

                        if (obj2 != null)
                        {
                            if (obj2.Length > 0)
                            {
                                objArtworkListingRef = obj2[0];

                                if (objArtworkListingRef.Status == Status.Customer_Approved_Paper_Proof || objArtworkListingRef.Status == Status.Customer_Rejected_Paper_Proof)
                                {
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "AlreadyApprovedOrRejected", "javascript:alert('The artwork has already been approved or rejected.');window.location='ArtworkApproval.aspx?pid=" + Convert.ToString(Request.QueryString["pid"]).Trim() + "'", true);
                                    //return;
                                }
                                objArtworkListingRef.Customer_Approved = Customer_Approved.Option1;
                                //|| ArtworkStatus == "1"
                                if (ArtworkStatus == "0")
                                {
                                    //objArtworkListingRef.Customer_ApprovedSpecified = true;
                                    //if (ArtworkStatus == "1")
                                    //{
                                    //    objArtworkListingRef.Status = ArtworkApproval.Status.Customer_Approved_Paper_Proof_with_Suggestions;
                                    //    objArtworkListingRef.Customer_Comment = Comments.Trim();
                                    //}
                                    //if (ArtworkStatus == "0")
                                    //{
                                    objArtworkListingRef.Status = Status.Customer_Approved_Paper_Proof;
                                    objArtworkListingRef.Customer_Comment = "";
                                    //}
                                    objArtworkListingRef.StatusSpecified = true;
                                    objArtworkListingRef.Proof_Approved_Date = System.DateTime.Today.Date;
                                    objArtworkListingRef.Proof_Approved_DateSpecified = true;
                                }
                                else if (ArtworkStatus == "1")
                                {
                                    objArtworkListingRef.Status = Status.Customer_Rejected_Paper_Proof;
                                    objArtworkListingRef.StatusSpecified = true;
                                    objArtworkListingRef.Customer_Comment = Comments.Trim();
                                    objArtworkListingRef.Proof_Approved_Date = System.DateTime.Today.Date;
                                    objArtworkListingRef.Proof_Approved_DateSpecified = true;
                                }
                                objArtworkListingRef.Approved_by_Signature = Signature.ToString().Trim() == "" ? "" : Signature.ToString().Trim();
                                objArtworkListingRef.IP_Address = ipaddress;
                                //artworkService.UpdateAsync(ref objArtworkListingRef);
                                Adbag_NavMicroservice.ArtworkApproval.Update update = new Adbag_NavMicroservice.ArtworkApproval.Update();
                                update.WS_Artwork_Approval_Listing = objArtworkListingRef;
                                artworkService.UpdateAsync(update);
                                lbIsStatusUpdated = true;
                            }
                        }
                    }
                    catch (Exception foException)
                    {
                        lbIsStatusUpdated = false;
                    }
                    finally
                    {
                        objNavWSBinding = null;
                        artworkService = null;
                        objArtworkListingRef = null;
                        objArtworkFilter = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return View("GetArtworkDetails", objArtworkApprovalDetails);
        }
    }
}